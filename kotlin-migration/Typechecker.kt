fun main() {
    val result =
            (Success(curry2(::StringPair))
                    applicateCollect Success("foo")
                    applicateCollect Success("bar"))
    println(result)
}

data class StringPair(val a: String, val b: String)

fun <A, B, R> curry2(f: (A, B) -> R): (A) -> (B) -> R {
    return { a -> { b -> f(a, b) }}
}

sealed class Typecheck<T>
data class Success<T>(val value: T) : Typecheck<T>()
data class Errors<T>(val errors: List<String>) : Typecheck<T>()

infix fun <F, A, B> Typecheck<F>.applicateCollect(other: Typecheck<A>): Typecheck<B> where F: (A) -> B {
    return when (this) {
        is Success -> {
            when (other) {
                is Success -> {
                    Success(this.value(other.value))
                }
                is Errors -> {
                    Errors(other.errors)
                }
            }
        }
        is Errors -> {
            when (other) {
                is Success -> {
                    Errors(this.errors)
                }
                is Errors -> {
                    Errors(this.errors + other.errors)
                }
            }
        }
    }
}