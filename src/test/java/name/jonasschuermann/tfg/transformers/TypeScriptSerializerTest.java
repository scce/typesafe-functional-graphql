package name.jonasschuermann.tfg.transformers;

import io.vavr.collection.List;
import name.jonasschuermann.tfg.TestResourceReader;
import name.jonasschuermann.tfg.languages.pretty.PrettyPrinter;
import name.jonasschuermann.tfg.languages.typescript.TypeScriptTestData;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public final class TypeScriptSerializerTest {
    @Test
    public void testGenerate() {
        var generator = new TypeScriptSerializer();
        var prettyPrinter = new PrettyPrinter();
        var input = TypeScriptTestData.ast();
        var expected =
            List.of("AllUsers", "User", "Group")
                .map(name -> "typescript/example/" + name + ".ts")
                .map(TestResourceReader::read);
        List<String> actual =
            input.map(
                file -> prettyPrinter.prettyPrint(generator.serialize(file))
            );
        assertEquals(expected, actual);
    }
}
