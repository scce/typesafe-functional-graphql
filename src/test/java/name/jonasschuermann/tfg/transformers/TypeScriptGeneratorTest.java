package name.jonasschuermann.tfg.transformers;

import io.vavr.collection.List;
import io.vavr.collection.TreeMap;
import name.jonasschuermann.tfg.languages.graphqlquery.GraphQLQueryTestData;
import name.jonasschuermann.tfg.languages.pretty.PrettyPrinter;
import name.jonasschuermann.tfg.languages.typedtfg.TypedTFGTestData;
import name.jonasschuermann.tfg.languages.typedtfg.ast.TypedTFGFile;
import name.jonasschuermann.tfg.languages.typescript.TypeScriptReferenceCollector;
import name.jonasschuermann.tfg.languages.typescript.TypeScriptTestData;
import name.jonasschuermann.tfg.languages.typescript.ast.TypeScriptFile;
import name.jonasschuermann.tfg.transformers.typescriptgenerator.TypeScriptGenerator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public final class TypeScriptGeneratorTest {
    @Test
    public void test() {
        GraphQLQuerySerializer graphQLQuerySerializer =
            new GraphQLQuerySerializer();
        PrettyPrinter prettyPrinter = new PrettyPrinter();
        TypedTFGFile input = TypedTFGTestData.ast();
        List<TypeScriptFile> expected = TypeScriptTestData.ast();
        List<TypeScriptFile> actual =
            new TypeScriptGenerator(
                new TypeScriptReferenceCollector()
            ).generate(
                TreeMap.of(
                    "allUsers",
                    prettyPrinter.prettyPrint(
                        graphQLQuerySerializer.serialize(
                            GraphQLQueryTestData.ast()
                        )
                    )
                ),
                input
            );
        assertEquals(expected, actual);
    }
}
