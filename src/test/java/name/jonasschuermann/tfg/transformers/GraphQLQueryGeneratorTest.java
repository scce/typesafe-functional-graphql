package name.jonasschuermann.tfg.transformers;

import io.vavr.collection.Map;
import io.vavr.collection.TreeMap;
import name.jonasschuermann.tfg.languages.graphqlquery.GraphQLQueryTestData;
import name.jonasschuermann.tfg.languages.graphql.ast.GraphQLFile;
import name.jonasschuermann.tfg.languages.typedtfg.TypedTFGTestData;
import name.jonasschuermann.tfg.languages.typedtfg.ast.TypedTFGFile;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public final class GraphQLQueryGeneratorTest {
    @Test
    public void test() {
        TypedTFGFile input = TypedTFGTestData.ast();
        Map<String, GraphQLFile> expected =
            TreeMap.of("allUsers", GraphQLQueryTestData.ast());
        Map<String, GraphQLFile> actual =
            new GraphQLQueryGenerator().generate(input);
        assertEquals(expected, actual);
    }
}
