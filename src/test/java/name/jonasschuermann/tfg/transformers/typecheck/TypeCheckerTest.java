package name.jonasschuermann.tfg.transformers.typecheck;

import graphql.language.ObjectTypeDefinition;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import io.vavr.collection.List;
import io.vavr.control.Either;
import name.jonasschuermann.tfg.TestResourceReader;
import name.jonasschuermann.tfg.languages.tfg.TFGTestData;
import name.jonasschuermann.tfg.languages.tfg.Weeder;
import name.jonasschuermann.tfg.languages.tfg.ast.TFGFile;
import name.jonasschuermann.tfg.languages.tfg.ast.TFGIntLiteral;
import name.jonasschuermann.tfg.languages.tfg.ast.TFGNullLiteral;
import name.jonasschuermann.tfg.languages.tfg.ast.TFGVariable;
import name.jonasschuermann.tfg.languages.typedtfg.TypedTFGTestData;
import name.jonasschuermann.tfg.languages.typedtfg.ast.TypedTFGFile;
import name.jonasschuermann.tfg.transformers.typecheck.typeerrors.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public final class TypeCheckerTest {

    @Test
    public void testSuccess() {
        TFGFile input = TFGTestData.ast();
        TypeDefinitionRegistry typeDefinitionRegistry =
            new SchemaParser().parse(
                TestResourceReader.read(
                    "graphqlschema/example.graphqlschema"
                )
            );
        Either<List<TypeError>, TypedTFGFile> expected =
            Either.right(TypedTFGTestData.ast());
        Either<List<TypeError>, TypedTFGFile> actual =
            new Typechecker()
                .typecheck(typeDefinitionRegistry, input);
        assertEquals(expected, actual);
    }

    @Test
    public void testDuplicateArgumentDeclaration() {
        TFGFile input =
            TFGTestData.duplicateArgumentDefinition();
        Either<List<TypeError>, TypedTFGFile> expected =
            Either.left(
                List.of(
                    new DuplicateArgumentDeclarationError(
                        "foo",
                        "bar"
                    )
                )
            );
        TypeDefinitionRegistry typeDefinitionRegistry =
            new TypeDefinitionRegistry();
        typeDefinitionRegistry.add(new ObjectTypeDefinition("Query"));
        Either<List<TypeError>, TypedTFGFile> actual =
            new Typechecker()
                .typecheck(typeDefinitionRegistry, input);
        assertEquals(expected, actual);
    }

    @Test
    public void testTypeErrors() {
        Weeder weeder = new Weeder();
        TFGFile input =
            weeder.process(
                TestResourceReader.read(
                    "tfg/typeErrors.tfg"
                )
            );
        TypeDefinitionRegistry typeDefinitionRegistry =
            new SchemaParser().parse(
                TestResourceReader.read(
                    "graphqlschema/typeErrors.graphqlschema"
                )
            );
        Either<List<TypeError>, TypedTFGFile> expected =
            Either.left(
                List.of(
                    new DuplicateQueryError(
                        "duplicateQuery"
                    ),
                    new DuplicateArgumentDeclarationError(
                        "firstQuery",
                        "duplicateArgument"
                    ),
                    new WrongArgumentTypeError(
                        "firstQuery",
                        "foo",
                        "blip",
                        "String",
                        new TFGIntLiteral("7")
                    ),
                    new SubselectionTypeMismatchError(
                        "firstQuery",
                        "foo",
                        "ObjectX",
                        "secondQuery",
                        "Object1"
                    ),
                    new WrongArgumentTypeError(
                        "firstQuery",
                        "foo",
                        "string",
                        "String",
                        new TFGVariable("bool")
                    ),
                    new UndefinedArgumentError(
                        "firstQuery",
                        "foo",
                        "undefined"
                    ),
                    new WrongArgumentTypeError(
                        "firstQuery",
                        "foo",
                        "nonNull",
                        "Int!",
                        new TFGNullLiteral()
                    ),
                    new WrongArgumentTypeError(
                        "firstQuery",
                        "bar",
                        "blop",
                        "Int!",
                        new TFGNullLiteral()
                    ),
                    new UndefinedArgumentError(
                        "firstQuery",
                        "bar",
                        "undefined"
                    ),
                    new UndefinedQueryError(
                        "firstQuery",
                        "bar",
                        "nonexistent"
                    ),
                    new UnrestrictedSubselectionError(
                        "firstQuery",
                        "baz"
                    ),
                    new UndefinedSchemaPropertyError(
                        "firstQuery",
                        "undefined",
                        "Query"
                    ),
                    new UndefinedSchemaTypeError(
                        "undefinedType",
                        "Undefined"
                    )
                )
            );
        Either<List<TypeError>, TypedTFGFile> actual =
            new Typechecker()
                .typecheck(typeDefinitionRegistry, input);
        assertEquals(expected, actual);
    }
}
