package name.jonasschuermann.tfg.transformers;

import name.jonasschuermann.tfg.TestResourceReader;
import name.jonasschuermann.tfg.languages.graphqlquery.GraphQLQueryTestData;
import name.jonasschuermann.tfg.languages.pretty.PrettyPrinter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public final class GraphQLQuerySerializerTest {
    @Test
    public void testGenerate() {
        var generator = new GraphQLQuerySerializer();
        var prettyPrinter = new PrettyPrinter();
        var input = GraphQLQueryTestData.ast();
        var actual = prettyPrinter.prettyPrint(generator.serialize(input));
        var expected = TestResourceReader.read("graphqlquery/example.graphql");
        assertEquals(expected, actual);
    }
}
