package name.jonasschuermann.tfg;

import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.TreeMap;
import io.vavr.control.Either;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

public final class CompilerTest {
    @Test
    public void testGitHub() {
        final var compiler = Compiler.make();

        assertEquals(
            Either.right(
                TreeMap.ofEntries(
                    List.of(
                        "RepositoryListing.ts",
                        "RepositoryForListing.ts",
                        "LanguageWithSize.ts"
                    )
                        .map(name ->
                            new Tuple2<>(
                                name,
                                TestResourceReader.read(
                                    "typescript/github/" + name
                                )
                            )
                        )
                )
            ),
            compiler.compile(
                TestResourceReader.read("graphqlschema/github.graphqlschema"),
                TestResourceReader.read("tfg/github.tfg")
            )
        );
    }
}
