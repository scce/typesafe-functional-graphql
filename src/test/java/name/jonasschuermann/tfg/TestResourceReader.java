package name.jonasschuermann.tfg;

import io.vavr.control.Option;

import java.io.IOException;

public final class TestResourceReader {
    public static String read(String filename) {
        try {
            return new String(
                Option.of(
                    TestResourceReader.class
                        .getClassLoader()
                        .getResourceAsStream(filename)
                )
                    .getOrElseThrow(() ->
                        new RuntimeException(
                            "Empty resource stream for '" + filename + "'"
                        )
                    )
                    .readAllBytes()
            );
        } catch (IOException e) {
            throw new RuntimeException(
                "IO error when reading resource file '" + filename + "'",
                e
            );
        }
    }
}
