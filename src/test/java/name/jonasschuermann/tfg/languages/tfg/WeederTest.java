package name.jonasschuermann.tfg.languages.tfg;

import name.jonasschuermann.tfg.TestResourceReader;
import name.jonasschuermann.tfg.languages.tfg.ast.TFGFile;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public final class WeederTest {
    @Test
    public void testProcess() {
        Weeder weeder = new Weeder();
        String input = TestResourceReader.read("tfg/example.tfg");
        TFGFile expected = TFGTestData.ast();
        TFGFile actual = weeder.process(input);
        assertEquals(expected, actual);
    }
}
