package name.jonasschuermann.tfg.languages.tfg;

import io.vavr.collection.List;
import io.vavr.control.Option;
import name.jonasschuermann.tfg.languages.common.ast.CommonListType;
import name.jonasschuermann.tfg.languages.common.ast.CommonPrimitiveType;
import name.jonasschuermann.tfg.languages.common.ast.CommonPrimitiveTypeEnum;
import name.jonasschuermann.tfg.languages.common.ast.CommonSimpleType;
import name.jonasschuermann.tfg.languages.tfg.ast.*;

public final class TFGTestData {
    public static TFGFile ast() {
        return new TFGFile(
            List.of(
                new TFGQuery(
                    TFGQueryIntend.Query,
                    "allUsers",
                    List.of(
                        new TFGArgumentDeclaration(
                            "foo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                false
                            )
                        ),
                        new TFGArgumentDeclaration(
                            "bayoo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.Boolean
                                ),
                                true
                            )
                        ),
                        new TFGArgumentDeclaration(
                            "list",
                            new CommonListType(
                                new CommonSimpleType(
                                    new CommonPrimitiveType(
                                        CommonPrimitiveTypeEnum.Int
                                    ),
                                    false
                                ),
                                false
                            )
                        )
                    ),
                    "Query",
                    List.of(
                        new TFGSelection(
                            new TFGPropertyAnchor(
                                new TFGProperty(
                                    "users",
                                    Option.none(),
                                    List.empty()
                                )
                            ),
                            Option.of(
                                new TFGSubselection(
                                    "user",
                                    List.of(
                                        new TFGArgumentApplication(
                                            "foo",
                                            new TFGVariable("foo")
                                        ),
                                        new TFGArgumentApplication(
                                            "bayoo",
                                            new TFGVariable("bayoo")
                                        )
                                    )
                                )
                            )
                        )
                    )
                ),
                new TFGQuery(
                    TFGQueryIntend.Query,
                    "user",
                    List.of(
                        new TFGArgumentDeclaration(
                            "foo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                false
                            )
                        ),
                        new TFGArgumentDeclaration(
                            "bayoo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.Boolean
                                ),
                                true
                            )
                        )
                    ),
                    "User",
                    List.of(
                        new TFGSelection(
                            new TFGPropertyAnchor(
                                new TFGProperty(
                                    "email",
                                    Option.none(),
                                    List.empty()
                                )
                            ),
                            Option.none()
                        ),
                        new TFGSelection(
                            new TFGPropertyAnchor(
                                new TFGProperty(
                                    "userGroup",
                                    Option.of("group"),
                                    List.empty()
                                )
                            ),
                            Option.of(
                                new TFGSubselection(
                                    "group",
                                    List.of(
                                        new TFGArgumentApplication(
                                            "foofoo",
                                            new TFGVariable("foo")
                                        ),
                                        new TFGArgumentApplication(
                                            "bazoo",
                                            new TFGIntLiteral("0")
                                        ),
                                        new TFGArgumentApplication(
                                            "baxoo",
                                            new TFGNullLiteral()
                                        )
                                    )
                                )
                            )
                        ),
                        new TFGSelection(
                            new TFGPropertyAnchor(
                                new TFGProperty(
                                    "foobar",
                                    Option.none(),
                                    List.empty()
                                )
                            ),
                            Option.none()
                        )
                    )
                ),
                new TFGQuery(
                    TFGQueryIntend.Query,
                    "group",
                    List.of(
                        new TFGArgumentDeclaration(
                            "foofoo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                false
                            )
                        ),
                        new TFGArgumentDeclaration(
                            "bazoo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.Int
                                ),
                                false
                            )
                        ),
                        new TFGArgumentDeclaration(
                            "baxoo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                true
                            )
                        )
                    ),
                    "Group",
                    List.of(
                        new TFGSelection(
                            new TFGPropertyAnchor(
                                new TFGProperty(
                                    "name",
                                    Option.none(),
                                    List.of(
                                        new TFGArgumentApplication(
                                            "foo",
                                            new TFGVariable("foofoo")
                                        ),
                                        new TFGArgumentApplication(
                                            "bar",
                                            new TFGVariable("bazoo")
                                        )
                                    )
                                )
                            ),
                            Option.none()
                        ),
                        new TFGSelection(
                            new TFGPropertyAnchor(
                                new TFGProperty(
                                    "literals",
                                    Option.none(),
                                    List.of(
                                        new TFGArgumentApplication(
                                            "int",
                                            new TFGIntLiteral("-7")
                                        ),
                                        new TFGArgumentApplication(
                                            "float",
                                            new TFGFloatLiteral("-0.25e-7")
                                        ),
                                        new TFGArgumentApplication(
                                            "string",
                                            new TFGStringLiteral(
                                                "\"foo\\\"bar\\n\""
                                            )
                                        ),
                                        new TFGArgumentApplication(
                                            "block_string",
                                            new TFGStringLiteral(
                                                "\"\"\"a\"bc\\\"\"\"\"\"\""
                                            )
                                        ),
                                        new TFGArgumentApplication(
                                            "boolean",
                                            new TFGBooleanLiteral(true)
                                        ),
                                        new TFGArgumentApplication(
                                            "enum",
                                            new TFGEnumLiteral("Green")
                                        ),
                                        new TFGArgumentApplication(
                                            "list",
                                            new TFGListLiteral(
                                                List.of(
                                                    new TFGIntLiteral(
                                                        "1"
                                                    ),
                                                    new TFGIntLiteral(
                                                        "2"
                                                    ),
                                                    new TFGIntLiteral(
                                                        "3"
                                                    )
                                                )
                                            )
                                        ),
                                        new TFGArgumentApplication(
                                            "record",
                                            new TFGRecordLiteral(
                                                List.of(
                                                    new TFGRecordLiteralProperty(
                                                        "foo",
                                                        new TFGStringLiteral(
                                                            "\"a\""
                                                        )
                                                    ),
                                                    new TFGRecordLiteralProperty(
                                                        "bar",
                                                        new TFGIntLiteral("1")
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            ),
                            Option.none()
                        )
                    )
                )
            )
        );
    }

    public static TFGFile duplicateArgumentDefinition() {
        return new TFGFile(
            List.of(
                new TFGQuery(
                    TFGQueryIntend.Query,
                    "foo",
                    List.of(
                        new TFGArgumentDeclaration(
                            "bar",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                false
                            )
                        ),
                        new TFGArgumentDeclaration(
                            "bar",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.Int
                                ),
                                false
                            )
                        )
                    ),
                    "Query",
                    List.empty()
                )
            )
        );
    }
}
