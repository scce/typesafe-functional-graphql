package name.jonasschuermann.tfg.languages.typedtfg;

import io.vavr.collection.List;
import io.vavr.control.Option;
import name.jonasschuermann.tfg.languages.common.ast.*;
import name.jonasschuermann.tfg.languages.tfg.ast.*;
import name.jonasschuermann.tfg.languages.typedtfg.ast.*;

public final class TypedTFGTestData {
    public static TypedTFGFile ast() {
        return new TypedTFGFile(
            List.of(
                new TypedTFGQuery(
                    TypedTFGQueryIntend.Query,
                    "allUsers",
                    List.of(
                        new TypedTFGArgumentDeclaration(
                            "foo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                false
                            )
                        ),
                        new TypedTFGArgumentDeclaration(
                            "bayoo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.Boolean
                                ),
                                true
                            )
                        ),
                        new TypedTFGArgumentDeclaration(
                            "list",
                            new CommonListType(
                                new CommonSimpleType(
                                    new CommonPrimitiveType(
                                        CommonPrimitiveTypeEnum.Int
                                    ),
                                    false
                                ),
                                false
                            )
                        )
                    ),
                    "Query",
                    List.of(
                        new TypedTFGSelection(
                            new TypedTFGPropertyAnchor(
                                new TypedTFGProperty(
                                    "users",
                                    Option.none(),
                                    List.empty(),
                                    new CommonListType(
                                        new CommonSimpleType(
                                            new CommonCompoundType("User"),
                                            false
                                        ),
                                        false
                                    )
                                )
                            ),
                            Option.of(
                                new TypedTFGSubselection(
                                    "user",
                                    List.of(
                                        new TypedTFGArgumentApplication(
                                            "foo",
                                            new TypedTFGVariable(
                                                "foo"
                                            )
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "bayoo",
                                            new TypedTFGVariable(
                                                "bayoo"
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    List.of(
                        new TypedTFGField(
                            "users",
                            new CommonListType(
                                new CommonSimpleType(
                                    new CommonCompoundType("User"),
                                    false
                                ),
                                false
                            ),
                            List.of(
                                new TypedTFGPathElement(
                                    "users",
                                    false,
                                    true,
                                    false
                                )
                            )
                        )
                    )
                ),
                new TypedTFGQuery(
                    TypedTFGQueryIntend.Query,
                    "user",
                    List.of(
                        new TypedTFGArgumentDeclaration(
                            "foo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                false
                            )
                        ),
                        new TypedTFGArgumentDeclaration(
                            "bayoo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.Boolean
                                ),
                                true
                            )
                        )
                    ),
                    "User",
                    List.of(
                        new TypedTFGSelection(
                            new TypedTFGPropertyAnchor(
                                new TypedTFGProperty(
                                    "email",
                                    Option.none(),
                                    List.empty(),
                                    new CommonSimpleType(
                                        new CommonPrimitiveType(
                                            CommonPrimitiveTypeEnum.String
                                        ),
                                        false
                                    )
                                )
                            ),
                            Option.none()
                        ),
                        new TypedTFGSelection(
                            new TypedTFGPropertyAnchor(
                                new TypedTFGProperty(
                                    "userGroup",
                                    Option.of("group"),
                                    List.empty(),
                                    new CommonSimpleType(
                                        new CommonCompoundType("Group"),
                                        false
                                    )
                                )
                            ),
                            Option.of(
                                new TypedTFGSubselection(
                                    "group",
                                    List.of(
                                        new TypedTFGArgumentApplication(
                                            "foofoo",
                                            new TypedTFGVariable("foo")
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "bazoo",
                                            new TypedTFGIntLiteral("0")
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "baxoo",
                                            new TypedTFGNullLiteral()
                                        )
                                    )
                                )
                            )
                        ),
                        new TypedTFGSelection(
                            new TypedTFGPropertyAnchor(
                                new TypedTFGProperty(
                                    "foobar",
                                    Option.none(),
                                    List.empty(),
                                    new CommonListType(
                                        new CommonSimpleType(
                                            new CommonPrimitiveType(
                                                CommonPrimitiveTypeEnum.String
                                            ),
                                            true
                                        ),
                                        true
                                    )
                                )
                            ),
                            Option.none()
                        )
                    ),
                    List.of(
                        new TypedTFGField(
                            "email",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                false
                            ),
                            List.of(
                                new TypedTFGPathElement(
                                    "email",
                                    false,
                                    false,
                                    false
                                )
                            )
                        ),
                        new TypedTFGField(
                            "group",
                            new CommonSimpleType(
                                new CommonCompoundType("Group"),
                                false
                            ),
                            List.of(
                                new TypedTFGPathElement(
                                    "group",
                                    false,
                                    false,
                                    false
                                )
                            )
                        ),
                        new TypedTFGField(
                            "foobar",
                            new CommonListType(
                                new CommonSimpleType(
                                    new CommonPrimitiveType(
                                        CommonPrimitiveTypeEnum.String
                                    ),
                                    true
                                ),
                                true
                            ),
                            List.of(
                                new TypedTFGPathElement(
                                    "foobar",
                                    true,
                                    true,
                                    true
                                )
                            )
                        )
                    )
                ),
                new TypedTFGQuery(
                    TypedTFGQueryIntend.Query,
                    "group",
                    List.of(
                        new TypedTFGArgumentDeclaration(
                            "foofoo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                false
                            )
                        ),
                        new TypedTFGArgumentDeclaration(
                            "bazoo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.Int
                                ),
                                false
                            )
                        ),
                        new TypedTFGArgumentDeclaration(
                            "baxoo",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                true
                            )
                        )
                    ),
                    "Group",
                    List.of(
                        new TypedTFGSelection(
                            new TypedTFGPropertyAnchor(
                                new TypedTFGProperty(
                                    "name",
                                    Option.none(),
                                    List.of(
                                        new TypedTFGArgumentApplication(
                                            "foo",
                                            new TypedTFGVariable("foofoo")
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "bar",
                                            new TypedTFGVariable("bazoo")
                                        )
                                    ),
                                    new CommonSimpleType(
                                        new CommonPrimitiveType(
                                            CommonPrimitiveTypeEnum.String
                                        ),
                                        false
                                    )
                                )
                            ),
                            Option.none()
                        ),
                        new TypedTFGSelection(
                            new TypedTFGPropertyAnchor(
                                new TypedTFGProperty(
                                    "literals",
                                    Option.none(),
                                    List.of(
                                        new TypedTFGArgumentApplication(
                                            "int",
                                            new TypedTFGIntLiteral("-7")
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "float",
                                            new TypedTFGFloatLiteral("-0.25e-7")
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "string",
                                            new TypedTFGStringLiteral(
                                                "\"foo\\\"bar\\n\""
                                            )
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "block_string",
                                            new TypedTFGStringLiteral(
                                                "\"\"\"a\"bc\\\"\"\"\"\"\""
                                            )
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "boolean",
                                            new TypedTFGBooleanLiteral(true)
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "enum",
                                            new TypedTFGEnumLiteral("Green")
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "list",
                                            new TypedTFGListLiteral(
                                                List.of(
                                                    new TypedTFGIntLiteral(
                                                        "1"
                                                    ),
                                                    new TypedTFGIntLiteral(
                                                        "2"
                                                    ),
                                                    new TypedTFGIntLiteral(
                                                        "3"
                                                    )
                                                )
                                            )
                                        ),
                                        new TypedTFGArgumentApplication(
                                            "record",
                                            new TypedTFGRecordLiteral(
                                                List.of(
                                                    new TypedTFGRecordLiteralProperty(
                                                        "foo",
                                                        new TypedTFGStringLiteral(
                                                            "\"a\""
                                                        )
                                                    ),
                                                    new TypedTFGRecordLiteralProperty(
                                                        "bar",
                                                        new TypedTFGIntLiteral(
                                                            "1"
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    ),
                                    new CommonSimpleType(
                                        new CommonPrimitiveType(
                                            CommonPrimitiveTypeEnum.String
                                        ),
                                        false
                                    )
                                )
                            ),
                            Option.none()
                        )
                    ),
                    List.of(
                        new TypedTFGField(
                            "name",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                false
                            ),
                            List.of(
                                new TypedTFGPathElement(
                                    "name",
                                    false,
                                    false,
                                    false
                                )
                            )
                        ),
                        new TypedTFGField(
                            "literals",
                            new CommonSimpleType(
                                new CommonPrimitiveType(
                                    CommonPrimitiveTypeEnum.String
                                ),
                                false
                            ),
                            List.of(
                                new TypedTFGPathElement(
                                    "literals",
                                    false,
                                    false,
                                    false
                                )
                            )
                        )
                    )
                )
            )
        );
    }
}
