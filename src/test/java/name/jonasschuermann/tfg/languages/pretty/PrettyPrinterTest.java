package name.jonasschuermann.tfg.languages.pretty;

import name.jonasschuermann.tfg.SequenceMonoid;
import name.jonasschuermann.tfg.languages.pretty.ast.IndentedBlock;
import name.jonasschuermann.tfg.languages.pretty.ast.Line;
import name.jonasschuermann.tfg.languages.pretty.ast.Prefix;
import name.jonasschuermann.tfg.languages.pretty.ast.Suffix;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrettyPrinterTest {
    @Test
    public void test() {
        var prettyPrinter = new PrettyPrinter();
        assertEquals("" +
                "function foo() {\n" +
                "    return bar\n" +
                "}\n" +
                "\n" +
                "function bar(\n" +
                "    a,\n" +
                "    b,\n" +
                ") {\n" +
                "    baz\n" +
                "        bam,\n" +
                "}\n",
            prettyPrinter.prettyPrint(
                SequenceMonoid.of(
                    Line.of("function ", "foo", "(", ") {"),
                    new IndentedBlock(
                        SequenceMonoid.of(
                            new Prefix(
                                SequenceMonoid.of("return "),
                                SequenceMonoid.of(Line.of("bar"))
                            )
                        )
                    ),
                    Line.of("}"),
                    Line.of(""),
                    Line.of("function ", "bar", "("),
                    IndentedBlock.of(
                        Line.of("a", ","),
                        Line.of("b", ",")
                    ),
                    Line.of(") {"),
                    IndentedBlock.of(
                        new Suffix(
                            SequenceMonoid.of(","),
                            SequenceMonoid.of(
                                Line.of("baz"),
                                IndentedBlock.of(
                                    Line.of("bam")
                                )
                            )
                        )
                    ),
                    Line.of("}")
                )
            )
        );
    }
}
