package name.jonasschuermann.tfg.languages.typescript;

import io.vavr.collection.List;
import name.jonasschuermann.tfg.languages.graphqlquery.GraphQLQueryTestData;
import name.jonasschuermann.tfg.languages.pretty.PrettyPrinter;
import name.jonasschuermann.tfg.languages.typescript.ast.*;
import name.jonasschuermann.tfg.transformers.GraphQLQuerySerializer;

public final class TypeScriptTestData {
    public static List<TypeScriptFile> ast() {
        GraphQLQuerySerializer graphQLQuerySerializer =
            new GraphQLQuerySerializer();
        PrettyPrinter prettyPrinter = new PrettyPrinter();
        return List.of(
            new TypeScriptFile(
                "AllUsers.ts",
                List.of(
                    new TypeScriptImport(
                        "json-bouncer",
                        List.of(
                            "Decoder",
                            "array",
                            "field",
                            "object"
                        )
                    ),
                    new TypeScriptImport(
                        "./User",
                        List.of(
                            "User",
                            "userDecoder"
                        )
                    )
                ),
                List.of(
                    new TypeScriptFunctionDeclaration(
                        "requestAllUsers",
                        List.of(
                            new TypeScriptArgumentDeclaration(
                                "foo",
                                new TypeScriptNominalType(
                                    new TypeScriptLocalReference("string")
                                )
                            ),
                            new TypeScriptArgumentDeclaration(
                                "bayoo",
                                new TypeScriptUnionType(
                                    List.of(
                                        new TypeScriptNominalType(
                                            new TypeScriptLocalReference(
                                                "boolean"
                                            )
                                        ),
                                        new TypeScriptNominalType(
                                            new TypeScriptLocalReference(
                                                "null"
                                            )
                                        )
                                    )
                                )
                            ),
                            new TypeScriptArgumentDeclaration(
                                "list",
                                new TypeScriptTypeApplication(
                                    new TypeScriptLocalReference(
                                        "ReadonlyArray"
                                    ),
                                    List.of(
                                        new TypeScriptNominalType(
                                            new TypeScriptLocalReference(
                                                "number"
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        new TypeScriptNominalType(
                            new TypeScriptLocalReference("AllUsersRequest")
                        ),
                        List.of(
                            new TypeScriptReturnStatement(
                                new TypeScriptObjectLiteral(
                                    List.of(
                                        new TypeScriptObjectLiteralVariableProperty(
                                            new TypeScriptLocalReference(
                                                "query"
                                            )
                                        ),
                                        new TypeScriptObjectLiteralProperty(
                                            "arguments",
                                            new TypeScriptObjectLiteral(
                                                List.of(
                                                    new TypeScriptObjectLiteralVariableProperty(
                                                        new TypeScriptLocalReference(
                                                            "foo"
                                                        )
                                                    ),
                                                    new TypeScriptObjectLiteralVariableProperty(
                                                        new TypeScriptLocalReference(
                                                            "bayoo"
                                                        )
                                                    ),
                                                    new TypeScriptObjectLiteralVariableProperty(
                                                        new TypeScriptLocalReference(
                                                            "list"
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    new TypeScriptEmbeddedTextFile(
                        "query",
                        prettyPrinter.prettyPrint(
                            graphQLQuerySerializer.serialize(
                                GraphQLQueryTestData.ast()
                            )
                        )
                    ),
                    new TypeScriptInterfaceDeclaration(
                        "AllUsersRequest",
                        List.of(
                            new TypeScriptPropertyDeclaration(
                                "query",
                                new TypeScriptNominalType(
                                    new TypeScriptLocalReference("string")
                                )
                            ),
                            new TypeScriptPropertyDeclaration(
                                "arguments",
                                new TypeScriptNominalType(
                                    new TypeScriptLocalReference(
                                        "AllUsersArguments"
                                    )
                                )
                            )
                        )
                    ),
                    new TypeScriptInterfaceDeclaration(
                        "AllUsersArguments",
                        List.of(
                            new TypeScriptPropertyDeclaration(
                                "foo",
                                new TypeScriptNominalType(
                                    new TypeScriptLocalReference("string")
                                )
                            ),
                            new TypeScriptPropertyDeclaration(
                                "bayoo",
                                new TypeScriptUnionType(
                                    List.of(
                                        new TypeScriptNominalType(
                                            new TypeScriptLocalReference(
                                                "boolean"
                                            )
                                        ),
                                        new TypeScriptNominalType(
                                            new TypeScriptLocalReference(
                                                "null"
                                            )
                                        )
                                    )
                                )
                            ),
                            new TypeScriptPropertyDeclaration(
                                "list",
                                new TypeScriptTypeApplication(
                                    new TypeScriptLocalReference(
                                        "ReadonlyArray"
                                    ),
                                    List.of(
                                        new TypeScriptNominalType(
                                            new TypeScriptLocalReference(
                                                "number"
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    new TypeScriptInterfaceDeclaration(
                        "AllUsers",
                        List.of(
                            new TypeScriptPropertyDeclaration(
                                "users",
                                new TypeScriptTypeApplication(
                                    new TypeScriptLocalReference(
                                        "ReadonlyArray"
                                    ),
                                    List.of(
                                        new TypeScriptNominalType(
                                            new TypeScriptImportedReference(
                                                "./User",
                                                "User"
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    new TypeScriptFunctionDeclaration(
                        "allUsersDecoder",
                        List.empty(),
                        new TypeScriptTypeApplication(
                            new TypeScriptImportedReference(
                                "json-bouncer",
                                "Decoder"
                            ),
                            List.of(
                                new TypeScriptNominalType(
                                    new TypeScriptLocalReference("AllUsers")
                                )
                            )
                        ),
                        List.of(
                            new TypeScriptReturnStatement(
                                new TypeScriptFunctionApplication(
                                    new TypeScriptImportedReference(
                                        "json-bouncer",
                                        "object"
                                    ),
                                    List.of(
                                        new TypeScriptObjectLiteral(
                                            List.of(
                                                new TypeScriptObjectLiteralProperty(
                                                    "users",
                                                    new TypeScriptFunctionApplication(
                                                        new TypeScriptImportedReference(
                                                            "json-bouncer",
                                                            "field"
                                                        ),
                                                        List.of(
                                                            new TypeScriptStringLiteral(
                                                                "users"
                                                            ),
                                                            new TypeScriptFunctionApplication(
                                                                new TypeScriptImportedReference(
                                                                    "json-bouncer",
                                                                    "array"
                                                                ),
                                                                List.of(
                                                                    new TypeScriptFunctionApplication(
                                                                        new TypeScriptImportedReference(
                                                                            "./User",
                                                                            "userDecoder"
                                                                        ),
                                                                        List.empty()
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            ),
            new TypeScriptFile(
                "User.ts",
                List.of(
                    new TypeScriptImport(
                        "json-bouncer",
                        List.of(
                            "Decoder",
                            "array",
                            "field",
                            "nullable",
                            "object",
                            "string"
                        )
                    ),
                    new TypeScriptImport(
                        "./Group",
                        List.of(
                            "Group",
                            "groupDecoder"
                        )
                    )
                ),
                List.of(
                    new TypeScriptInterfaceDeclaration(
                        "User",
                        List.of(
                            new TypeScriptPropertyDeclaration(
                                "email",
                                new TypeScriptNominalType(
                                    new TypeScriptLocalReference("string")
                                )
                            ),
                            new TypeScriptPropertyDeclaration(
                                "group",
                                new TypeScriptNominalType(
                                    new TypeScriptImportedReference(
                                        "./Group",
                                        "Group"
                                    )
                                )
                            ),
                            new TypeScriptPropertyDeclaration(
                                "foobar",
                                new TypeScriptUnionType(
                                    List.of(
                                        new TypeScriptTypeApplication(
                                            new TypeScriptLocalReference(
                                                "ReadonlyArray"
                                            ),
                                            List.of(
                                                new TypeScriptUnionType(
                                                    List.of(
                                                        new TypeScriptNominalType(
                                                            new TypeScriptLocalReference(
                                                                "string"
                                                            )
                                                        ),
                                                        new TypeScriptNominalType(
                                                            new TypeScriptLocalReference(
                                                                "null"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        new TypeScriptNominalType(
                                            new TypeScriptLocalReference("null")
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    new TypeScriptFunctionDeclaration(
                        "userDecoder",
                        List.empty(),
                        new TypeScriptTypeApplication(
                            new TypeScriptImportedReference(
                                "json-bouncer",
                                "Decoder"
                            ),
                            List.of(
                                new TypeScriptNominalType(
                                    new TypeScriptLocalReference("User")
                                )
                            )
                        ),
                        List.of(
                            new TypeScriptReturnStatement(
                                new TypeScriptFunctionApplication(
                                    new TypeScriptImportedReference(
                                        "json-bouncer",
                                        "object"
                                    ),
                                    List.of(
                                        new TypeScriptObjectLiteral(
                                            List.of(
                                                new TypeScriptObjectLiteralProperty(
                                                    "email",
                                                    new TypeScriptFunctionApplication(
                                                        new TypeScriptImportedReference(
                                                            "json-bouncer",
                                                            "field"
                                                        ),
                                                        List.of(
                                                            new TypeScriptStringLiteral(
                                                                "email"
                                                            ),
                                                            new TypeScriptFunctionApplication(
                                                                new TypeScriptImportedReference(
                                                                    "json-bouncer",
                                                                    "string"
                                                                ),
                                                                List.empty()
                                                            )
                                                        )
                                                    )
                                                ),
                                                new TypeScriptObjectLiteralProperty(
                                                    "group",
                                                    new TypeScriptFunctionApplication(
                                                        new TypeScriptImportedReference(
                                                            "json-bouncer",
                                                            "field"
                                                        ),
                                                        List.of(
                                                            new TypeScriptStringLiteral(
                                                                "group"
                                                            ),
                                                            new TypeScriptFunctionApplication(
                                                                new TypeScriptImportedReference(
                                                                    "./Group",
                                                                    "groupDecoder"
                                                                ),
                                                                List.empty()
                                                            )
                                                        )
                                                    )
                                                ),
                                                new TypeScriptObjectLiteralProperty(
                                                    "foobar",
                                                    new TypeScriptFunctionApplication(
                                                        new TypeScriptImportedReference(
                                                            "json-bouncer",
                                                            "field"
                                                        ),
                                                        List.of(
                                                            new TypeScriptStringLiteral(
                                                                "foobar"
                                                            ),
                                                            new TypeScriptFunctionApplication(
                                                                new TypeScriptImportedReference(
                                                                    "json-bouncer",
                                                                    "nullable"
                                                                ),
                                                                List.of(
                                                                    new TypeScriptFunctionApplication(
                                                                        new TypeScriptImportedReference(
                                                                            "json-bouncer",
                                                                            "array"
                                                                        ),
                                                                        List.of(
                                                                            new TypeScriptFunctionApplication(
                                                                                new TypeScriptImportedReference(
                                                                                    "json-bouncer",
                                                                                    "nullable"
                                                                                ),
                                                                                List.of(
                                                                                    new TypeScriptFunctionApplication(
                                                                                        new TypeScriptImportedReference(
                                                                                            "json-bouncer",
                                                                                            "string"
                                                                                        ),
                                                                                        List.empty()
                                                                                    )
                                                                                )
                                                                            )
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            ),
            new TypeScriptFile(
                "Group.ts",
                List.of(
                    new TypeScriptImport(
                        "json-bouncer",
                        List.of(
                            "Decoder",
                            "field",
                            "object",
                            "string"
                        )
                    )
                ),
                List.of(
                    new TypeScriptInterfaceDeclaration(
                        "Group",
                        List.of(
                            new TypeScriptPropertyDeclaration(
                                "name",
                                new TypeScriptNominalType(
                                    new TypeScriptLocalReference(
                                        "string"
                                    )
                                )
                            ),
                            new TypeScriptPropertyDeclaration(
                                "literals",
                                new TypeScriptNominalType(
                                    new TypeScriptLocalReference(
                                        "string"
                                    )
                                )
                            )
                        )
                    ),
                    new TypeScriptFunctionDeclaration(
                        "groupDecoder",
                        List.empty(),
                        new TypeScriptTypeApplication(
                            new TypeScriptImportedReference(
                                "json-bouncer",
                                "Decoder"
                            ),
                            List.of(
                                new TypeScriptNominalType(
                                    new TypeScriptLocalReference(
                                        "Group"
                                    )
                                )
                            )
                        ),
                        List.of(
                            new TypeScriptReturnStatement(
                                new TypeScriptFunctionApplication(
                                    new TypeScriptImportedReference(
                                        "json-bouncer",
                                        "object"
                                    ),
                                    List.of(
                                        new TypeScriptObjectLiteral(
                                            List.of(
                                                new TypeScriptObjectLiteralProperty(
                                                    "name",
                                                    new TypeScriptFunctionApplication(
                                                        new TypeScriptImportedReference(
                                                            "json-bouncer",
                                                            "field"
                                                        ),
                                                        List.of(
                                                            new TypeScriptStringLiteral(
                                                                "name"
                                                            ),
                                                            new TypeScriptFunctionApplication(
                                                                new TypeScriptImportedReference(
                                                                    "json-bouncer",
                                                                    "string"
                                                                ),
                                                                List.empty()
                                                            )
                                                        )
                                                    )
                                                ),
                                                new TypeScriptObjectLiteralProperty(
                                                    "literals",
                                                    new TypeScriptFunctionApplication(
                                                        new TypeScriptImportedReference(
                                                            "json-bouncer",
                                                            "field"
                                                        ),
                                                        List.of(
                                                            new TypeScriptStringLiteral(
                                                                "literals"
                                                            ),
                                                            new TypeScriptFunctionApplication(
                                                                new TypeScriptImportedReference(
                                                                    "json-bouncer",
                                                                    "string"
                                                                ),
                                                                List.empty()
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );
    }
}
