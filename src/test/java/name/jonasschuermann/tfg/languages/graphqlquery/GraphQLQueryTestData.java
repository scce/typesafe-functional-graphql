package name.jonasschuermann.tfg.languages.graphqlquery;

import io.vavr.collection.List;
import io.vavr.control.Option;
import name.jonasschuermann.tfg.languages.common.ast.CommonListType;
import name.jonasschuermann.tfg.languages.common.ast.CommonPrimitiveType;
import name.jonasschuermann.tfg.languages.common.ast.CommonPrimitiveTypeEnum;
import name.jonasschuermann.tfg.languages.common.ast.CommonSimpleType;
import name.jonasschuermann.tfg.languages.graphql.ast.*;
import name.jonasschuermann.tfg.languages.tfg.ast.*;

public final class GraphQLQueryTestData {
    public static GraphQLFile ast() {
        GraphQLInterface groupQuery =
            new GraphQLInterface(
                List.of(
                    new GraphQLProperty(
                        "name",
                        Option.none(),
                        List.of(
                            new GraphQLArgumentApplication(
                                "foo",
                                new GraphQLVariable("foo")
                            ),
                            new GraphQLArgumentApplication(
                                "bar",
                                new GraphQLIntLiteral("0")
                            )
                        ),
                        Option.none()
                    ),
                    new GraphQLProperty(
                        "literals",
                        Option.none(),
                        List.of(
                            new GraphQLArgumentApplication(
                                "int",
                                new GraphQLIntLiteral("-7")
                            ),
                            new GraphQLArgumentApplication(
                                "float",
                                new GraphQLFloatLiteral("-0.25e-7")
                            ),
                            new GraphQLArgumentApplication(
                                "string",
                                new GraphQLStringLiteral("\"foo\\\"bar\\n\"")
                            ),
                            new GraphQLArgumentApplication(
                                "block_string",
                                new GraphQLStringLiteral(
                                    "\"\"\"a\"bc\\\"\"\"\"\"\""
                                )
                            ),
                            new GraphQLArgumentApplication(
                                "boolean",
                                new GraphQLBooleanLiteral(true)
                            ),
                            new GraphQLArgumentApplication(
                                "enum",
                                new GraphQLEnumLiteral("Green")
                            ),
                            new GraphQLArgumentApplication(
                                "list",
                                new GraphQLListLiteral(
                                    List.of(
                                        new GraphQLIntLiteral(
                                            "1"
                                        ),
                                        new GraphQLIntLiteral(
                                            "2"
                                        ),
                                        new GraphQLIntLiteral(
                                            "3"
                                        )
                                    )
                                )
                            ),
                            new GraphQLArgumentApplication(
                                "record",
                                new GraphQLRecordLiteral(
                                    List.of(
                                        new GraphQLRecordLiteralProperty(
                                            "foo",
                                            new GraphQLStringLiteral(
                                                "\"a\""
                                            )
                                        ),
                                        new GraphQLRecordLiteralProperty(
                                            "bar",
                                            new GraphQLIntLiteral("1")
                                        )
                                    )
                                )
                            )
                        ),
                        Option.none()
                    )
                )
            );
        GraphQLInterface userQuery =
            new GraphQLInterface(
                List.of(
                    new GraphQLProperty(
                        "email",
                        Option.none(),
                        List.empty(),
                        Option.none()
                    ),
                    new GraphQLProperty(
                        "userGroup",
                        Option.of("group"),
                        List.empty(),
                        Option.of(groupQuery)
                    ),
                    new GraphQLProperty(
                        "foobar",
                        Option.none(),
                        List.empty(),
                        Option.none()
                    )
                )
            );
        return new GraphQLFile(
            GraphQLQueryIntend.Query,
            "allUsers",
            List.of(
                new GraphQLArgumentDeclaration(
                    "foo",
                    new CommonSimpleType(
                        new CommonPrimitiveType(CommonPrimitiveTypeEnum.String),
                        false
                    )
                ),
                new GraphQLArgumentDeclaration(
                    "bayoo",
                    new CommonSimpleType(
                        new CommonPrimitiveType(
                            CommonPrimitiveTypeEnum.Boolean
                        ),
                        true
                    )
                ),
                new GraphQLArgumentDeclaration(
                    "list",
                    new CommonListType(
                        new CommonSimpleType(
                            new CommonPrimitiveType(
                                CommonPrimitiveTypeEnum.Int
                            ),
                            false
                        ),
                        false
                    )
                )
            ),
            new GraphQLInterface(
                List.of(
                    new GraphQLProperty(
                        "users",
                        Option.none(),
                        List.of(),
                        Option.of(userQuery)
                    )
                )
            )
        );
    }
}
