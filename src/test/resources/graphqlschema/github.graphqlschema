type Query {
    organization(login: String!): Organization
}

type Organization {
    repositories(first: Int, orderBy: RepositoryOrder): RepositoryConnection!
}

input RepositoryOrder {
    field: RepositoryOrderField!
    direction: OrderDirection!
}

enum RepositoryOrderField {
    CREATED_AT
    UPDATED_AT
    PUSHED_AT
    NAME
    STARGAZERS
}

enum OrderDirection {
    ASC
    DESC
}

type RepositoryConnection {
    nodes: [Repository]
}

type Repository {
    name: String!
    languages(first: Int, orderBy: LanguageOrder): LanguageConnection
}

input LanguageOrder {
    field: LanguageOrderField!
    direction: OrderDirection!
}

enum LanguageOrderField {
    SIZE
}

type LanguageConnection {
    edges: [LanguageEdge]
}

type LanguageEdge {
    size: Int!
    node: Language!
}

type Language {
    name: String!
}