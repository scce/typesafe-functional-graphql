import {
    Decoder,
    array,
    field,
    nullable,
    object,
} from "json-bouncer"

import {
    RepositoryForListing,
    repositoryForListingDecoder,
} from "./RepositoryForListing"

export function requestRepositoryListing(
    organization: string,
    repositoryLimit: number,
    languageLimit: number,
): RepositoryListingRequest {
    return {
        query,
        arguments: {
            organization,
            repositoryLimit,
            languageLimit,
        },
    };
}

export const query =
    "query repositoryListing(\n" +
    "    $organization: String!,\n" +
    "    $repositoryLimit: Int!,\n" +
    "    $languageLimit: Int!\n" +
    ") {\n" +
    "    organization(\n" +
    "        login: $organization\n" +
    "    ) {\n" +
    "        repositories(\n" +
    "            first: $repositoryLimit,\n" +
    "            orderBy: { field: STARGAZERS, direction: DESC }\n" +
    "        ) {\n" +
    "            nodes {\n" +
    "                name\n" +
    "                languages(\n" +
    "                    first: $languageLimit,\n" +
    "                    orderBy: { field: SIZE, direction: DESC }\n" +
    "                ) {\n" +
    "                    edges {\n" +
    "                        node {\n" +
    "                            name\n" +
    "                        }\n" +
    "                        size\n" +
    "                    }\n" +
    "                }\n" +
    "            }\n" +
    "        }\n" +
    "    }\n" +
    "}\n"

export interface RepositoryListingRequest {
    readonly query: string
    readonly arguments: RepositoryListingArguments
}

export interface RepositoryListingArguments {
    readonly organization: string
    readonly repositoryLimit: number
    readonly languageLimit: number
}

export interface RepositoryListing {
    readonly repositories: ReadonlyArray<RepositoryForListing | null> | null
}

export function repositoryListingDecoder(): Decoder<RepositoryListing> {
    return object(
        {
            repositories: field(
                "organization",
                nullable(
                    field(
                        "repositories",
                        field(
                            "nodes",
                            nullable(
                                array(
                                    nullable(
                                        repositoryForListingDecoder(),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        },
    );
}
