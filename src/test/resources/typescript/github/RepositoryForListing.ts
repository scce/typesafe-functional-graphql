import {
    Decoder,
    array,
    field,
    nullable,
    object,
    string,
} from "json-bouncer"

import {
    LanguageWithSize,
    languageWithSizeDecoder,
} from "./LanguageWithSize"

export interface RepositoryForListing {
    readonly name: string
    readonly languages: ReadonlyArray<LanguageWithSize | null> | null
}

export function repositoryForListingDecoder(): Decoder<RepositoryForListing> {
    return object(
        {
            name: field(
                "name",
                string(),
            ),
            languages: field(
                "languages",
                nullable(
                    field(
                        "edges",
                        nullable(
                            array(
                                nullable(
                                    languageWithSizeDecoder(),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        },
    );
}
