import {
    Decoder,
    field,
    number,
    object,
    string,
} from "json-bouncer"

export interface LanguageWithSize {
    readonly name: string
    readonly size: number
}

export function languageWithSizeDecoder(): Decoder<LanguageWithSize> {
    return object(
        {
            name: field(
                "node",
                field(
                    "name",
                    string(),
                ),
            ),
            size: field(
                "size",
                number(),
            ),
        },
    );
}
