import {
    Decoder,
    field,
    object,
    string,
} from "json-bouncer"

export interface Group {
    readonly name: string
    readonly literals: string
}

export function groupDecoder(): Decoder<Group> {
    return object(
        {
            name: field(
                "name",
                string(),
            ),
            literals: field(
                "literals",
                string(),
            ),
        },
    );
}
