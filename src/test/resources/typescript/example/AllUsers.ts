import {
    Decoder,
    array,
    field,
    object,
} from "json-bouncer"

import {
    User,
    userDecoder,
} from "./User"

export function requestAllUsers(
    foo: string,
    bayoo: boolean | null,
    list: ReadonlyArray<number>,
): AllUsersRequest {
    return {
        query,
        arguments: {
            foo,
            bayoo,
            list,
        },
    };
}

export const query =
    "query allUsers(\n" +
    "    $foo: String!,\n" +
    "    $bayoo: Boolean,\n" +
    "    $list: [Int!]!\n" +
    ") {\n" +
    "    users {\n" +
    "        email\n" +
    "        group: userGroup {\n" +
    "            name(\n" +
    "                foo: $foo,\n" +
    "                bar: 0\n" +
    "            )\n" +
    "            literals(\n" +
    "                int: -7,\n" +
    "                float: -0.25e-7,\n" +
    "                string: \"foo\\\"bar\\n\",\n" +
    "                block_string: \"\"\"a\"bc\\\"\"\"\"\"\",\n" +
    "                boolean: true,\n" +
    "                enum: Green,\n" +
    "                list: [ 1, 2, 3 ],\n" +
    "                record: { foo: \"a\", bar: 1 }\n" +
    "            )\n" +
    "        }\n" +
    "        foobar\n" +
    "    }\n" +
    "}\n"

export interface AllUsersRequest {
    readonly query: string
    readonly arguments: AllUsersArguments
}

export interface AllUsersArguments {
    readonly foo: string
    readonly bayoo: boolean | null
    readonly list: ReadonlyArray<number>
}

export interface AllUsers {
    readonly users: ReadonlyArray<User>
}

export function allUsersDecoder(): Decoder<AllUsers> {
    return object(
        {
            users: field(
                "users",
                array(
                    userDecoder(),
                ),
            ),
        },
    );
}
