import {
    Decoder,
    array,
    field,
    nullable,
    object,
    string,
} from "json-bouncer"

import {
    Group,
    groupDecoder,
} from "./Group"

export interface User {
    readonly email: string
    readonly group: Group
    readonly foobar: ReadonlyArray<string | null> | null
}

export function userDecoder(): Decoder<User> {
    return object(
        {
            email: field(
                "email",
                string(),
            ),
            group: field(
                "group",
                groupDecoder(),
            ),
            foobar: field(
                "foobar",
                nullable(
                    array(
                        nullable(
                            string(),
                        ),
                    ),
                ),
            ),
        },
    );
}
