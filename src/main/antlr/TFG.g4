grammar TFG;

@header{
package name.jonasschuermann.tfg.languages.tfg.parser;
}

file
    : queries+=query* EOF
    ;

query
    : intend=('query' | 'mutation') name=LowerIdentifier ('('
          arguments+=argumentDeclaration (',' arguments+=argumentDeclaration)*
      ')')? 'on' type=UpperIdentifier '{'
          selections+=selection*
      '}'
    ;

argumentDeclaration
    : name=VariableIdentifier ':' type=typeDeclaration
    ;

typeDeclaration
    : nominalTypeDeclaration
    | listTypeDeclaration
    ;

nominalTypeDeclaration
    : name=UpperIdentifier (nonNullable='!')?
    ;

listTypeDeclaration
    : '[' appliedType=nominalTypeDeclaration ']' (nonNullable='!')?
    ;

selection
    : anchor subselection?
    ;

anchor
    : propertyAnchor
    | parentDeepSelection
    | childrenDeepSelection
    ;

propertyAnchor
    : property
    ;

parentDeepSelection
    : parent=property '.' child=anchor
    ;

childrenDeepSelection
    : parent=property '.' '{' children+=selection+ '}'
    ;

property
    : (alias=LowerIdentifier ':')?
      name=LowerIdentifier ('('
          arguments+=argumentApplication (',' arguments+=argumentApplication)*
      ')')?
    ;

subselection
    : '<-' name=LowerIdentifier ('('
          arguments+=argumentApplication (',' arguments+=argumentApplication)*
      ')')?
    ;

argumentApplication
    : name=LowerIdentifier ':' value=expression
    ;

expression
    : variable
    | literal
    ;

literal
    : intLiteral
    | floatLiteral
    | stringLiteral
    | booleanLiteral
    | nullLiteral
    | enumLiteral
    | listLiteral
    | recordLiteral
    ;

variable
    : name=VariableIdentifier
    ;

intLiteral
    : value=IntLiteral
    ;

floatLiteral
    : value=FloatLiteral
    ;

stringLiteral
    : value=StringLiteral
    ;

booleanLiteral
    : value=BooleanLiteral
    ;

nullLiteral
    : value=NullLiteral
    ;

enumLiteral
    : value=UpperIdentifier
    ;

listLiteral
    : '[' (items+=expression (',' items+=expression)*)? ']'
    ;

recordLiteral
    : '{' (
          properties+=recordLiteralProperty
          (',' properties+=recordLiteralProperty)*
      )? '}'
    ;

recordLiteralProperty
    : name=LowerIdentifier ':' value=expression
    ;

IntLiteral
    : IntPart
    ;

FloatLiteral
    : IntPart FractionalPart
    | IntPart ExponentPart
    | IntPart FractionalPart ExponentPart
    ;

fragment
IntPart
    : '-'?('0'|[1-9][0-9]*)
    ;

fragment
FractionalPart
    : '.'[0-9]+
    ;

fragment
ExponentPart
    : [eE][+-]?[0-9]+
    ;

StringLiteral
    : '"'StringCharacter*'"'
    | '"""'BlockStringCharacter*'"""'
    ;

fragment
StringCharacter
    : ~["\\]
    | '\\u'[0-9A-Fa-f]'\\u'[0-9A-Fa-f]'\\u'[0-9A-Fa-f]'\\u'[0-9A-Fa-f]
    | '\\'["\\/bfnrt]
    ;

fragment
BlockStringCharacter
    : ~'"'
    | '"'BlockStringCharacterAfterOneQuote
    | '\\"""'
    ;

fragment
BlockStringCharacterAfterOneQuote
    : ~'"'
    | '"'BlockStringCharacterAfterTwoQuotes
    ;

fragment
BlockStringCharacterAfterTwoQuotes
    : ~'"'
    ;

BooleanLiteral
    : 'false'
    | 'true'
    ;

NullLiteral
    : 'null'
    ;

UpperIdentifier
    : [A-Z][a-zA-Z0-9_]*
    ;

LowerIdentifier
    : [a-z][a-zA-Z0-9_]*
    ;

VariableIdentifier
    : '$'[a-z][a-zA-Z0-9_]*
    ;

WS
    : [ \t\r\n]+ -> skip
    ;
