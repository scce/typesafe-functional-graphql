package name.jonasschuermann.tfg.transformers;

import io.vavr.collection.List;
import name.jonasschuermann.tfg.SerializerHelper;
import name.jonasschuermann.tfg.SequenceMonoid;
import name.jonasschuermann.tfg.UnhandledCaseException;
import name.jonasschuermann.tfg.languages.pretty.ast.*;
import name.jonasschuermann.tfg.languages.typescript.ast.*;

import java.util.Arrays;

public final class TypeScriptSerializer {
    public SequenceMonoid<Part> serialize(TypeScriptFile file) {
        return SequenceMonoid
            .sum(
                file.getImports()
                    .map(this::serializeImport)
                    .intersperse(SequenceMonoid.of(Line.of()))
            )
            .append(Line.of())
            .plus(
                SequenceMonoid.sum(
                    file.getTopLevelStatements()
                        .map(this::serializeTopLevelStatement)
                        .intersperse(SequenceMonoid.of(Line.of()))
                )
            );
    }

    private SequenceMonoid<Part> serializeImport(TypeScriptImport import_) {
        return SequenceMonoid.of(
            Line.of("import {"),
            IndentedBlock.over(
                definition -> SequenceMonoid.of(Line.of(definition, ",")),
                import_.getDefinitions()
            ),
            Line.of("} from \"", import_.getFrom(), "\"")
        );
    }

    private SequenceMonoid<Part> serializeTopLevelStatement(
        TypeScriptTopLevelStatement topLevelStatement
    ) {
        if (topLevelStatement instanceof TypeScriptInterfaceDeclaration) {
            final var interfaceGeneration =
                (TypeScriptInterfaceDeclaration) topLevelStatement;
            return serializeInterfaceDeclaration(interfaceGeneration);
        } else if (topLevelStatement instanceof TypeScriptFunctionDeclaration) {
            final var functionDeclaration =
                (TypeScriptFunctionDeclaration) topLevelStatement;
            return serializeFunctionDeclaration(functionDeclaration);
        } else if (topLevelStatement instanceof TypeScriptEmbeddedTextFile) {
            final var embeddedFile =
                (TypeScriptEmbeddedTextFile) topLevelStatement;
            return serializeEmbeddedTextFile(embeddedFile);
        } else {
            throw new UnhandledCaseException(topLevelStatement);
        }
    }

    private SequenceMonoid<Part> serializeInterfaceDeclaration(
        TypeScriptInterfaceDeclaration interfaceDeclaration
    ) {
        return SequenceMonoid.of(
            Line.of(
                "export interface ",
                interfaceDeclaration.getName(),
                " {"
            ),
            IndentedBlock.over(
                this::serializePropertyDeclaration,
                interfaceDeclaration.getPropertyDeclarations()
            ),
            Line.of("}")
        );
    }

    private SequenceMonoid<Part> serializePropertyDeclaration(
        TypeScriptPropertyDeclaration propertyDeclaration
    ) {
        return SequenceMonoid.of(
            new Line(
                SequenceMonoid
                    .of("readonly ", propertyDeclaration.getName(), ": ")
                    .plus(serializeType(propertyDeclaration.getType()))
            )
        );
    }

    private SequenceMonoid<Part> serializeFunctionDeclaration(
        TypeScriptFunctionDeclaration functionDeclaration
    ) {
        final var name = functionDeclaration.getName();
        return serializeParentheses(
            SequenceMonoid.of("export function ", name, "("),
            SequenceMonoid.sum(
                functionDeclaration
                    .getArgumentDeclarations()
                    .map(this::serializeArgumentDeclaration)
            ),
            SequenceMonoid
                .of("): ")
                .plus(serializeType(functionDeclaration.getReturnType()))
                .append(" {")
        )
            .append(
                IndentedBlock.over(
                    this::serializeStatement,
                    functionDeclaration.getStatements()
                )
            )
            .append(Line.of("}"));
    }

    private SequenceMonoid<Part> serializeArgumentDeclaration(
        TypeScriptArgumentDeclaration argumentDeclaration
    ) {
        return SequenceMonoid.of(
            new Line(
                SequenceMonoid
                    .of(argumentDeclaration.getName(), ": ")
                    .plus(serializeType(argumentDeclaration.getType()))
                    .append(",")
            )
        );
    }

    private SequenceMonoid<Part> serializeStatement(
        TypeScriptStatement statement
    ) {
        SequenceMonoid<Part> serializedStatement;
        if (statement instanceof TypeScriptReturnStatement) {
            final var returnStatement = (TypeScriptReturnStatement) statement;
            serializedStatement = serializeReturnStatement(returnStatement);
        } else {
            throw new UnhandledCaseException(statement);
        }
        return SequenceMonoid.of(
            new Suffix(SequenceMonoid.of(";"), serializedStatement)
        );
    }

    private SequenceMonoid<Part> serializeReturnStatement(
        TypeScriptReturnStatement returnStatement
    ) {
        return SequenceMonoid.of(
            new Prefix(
                SequenceMonoid.of("return "),
                serializeExpression(returnStatement.getValue())
            )
        );
    }

    private SequenceMonoid<Part> serializeExpression(
        TypeScriptExpression expression
    ) {
        if (expression instanceof TypeScriptStringLiteral) {
            final var stringLiteral = (TypeScriptStringLiteral) expression;
            return SequenceMonoid.of(
                new Line(serializeStringLiteral(stringLiteral))
            );
        } else if (expression instanceof TypeScriptObjectLiteral) {
            return serializeObjectLiteral((TypeScriptObjectLiteral) expression);
        } else if (expression instanceof TypeScriptFunctionApplication) {
            final var functionApplication =
                (TypeScriptFunctionApplication) expression;
            return serializeFunctionApplication(functionApplication);
        } else {
            throw new UnhandledCaseException(expression);
        }
    }

    private SequenceMonoid<String> serializeStringLiteral(
        TypeScriptStringLiteral stringLiteral
    ) {
        return SequenceMonoid.of(
            "\"",
            stringLiteral
                .getValue()
                .replace("\\", "\\\\")
                .replace("\"", "\\\"")
                .replace("\n", "\\n"),
            "\""
        );
    }

    private SequenceMonoid<Part> serializeObjectLiteral(
        TypeScriptObjectLiteral objectLiteral
    ) {
        return SequenceMonoid.of(
            Line.of("{"),
            IndentedBlock.over(
                this::serializeObjectLiteralPart,
                objectLiteral.getParts()
            ),
            Line.of("}")
        );
    }

    private SequenceMonoid<Part> serializeObjectLiteralPart(
        TypeScriptObjectLiteralPart part
    ) {
        if (part instanceof TypeScriptObjectLiteralProperty) {
            TypeScriptObjectLiteralProperty property =
                (TypeScriptObjectLiteralProperty) part;
            return serializeObjectLiteralProperty(property);
        } else if (part instanceof TypeScriptObjectLiteralVariableProperty) {
            TypeScriptObjectLiteralVariableProperty variableProperty =
                (TypeScriptObjectLiteralVariableProperty) part;
            return serializeObjectLiteralVariableProperty(variableProperty);
        } else {
            throw new UnhandledCaseException(part);
        }
    }

    private SequenceMonoid<Part> serializeObjectLiteralProperty(
        TypeScriptObjectLiteralProperty property
    ) {
        return SequenceMonoid.of(
            new Prefix(
                SequenceMonoid.of(property.getName(), ": "),
                SequenceMonoid.of(
                    new Suffix(
                        SequenceMonoid.of(","),
                        serializeExpression(property.getValue())
                    )
                )
            )
        );
    }

    private SequenceMonoid<Part> serializeObjectLiteralVariableProperty(
        TypeScriptObjectLiteralVariableProperty variableProperty
    ) {
        return SequenceMonoid.of(
            new Line(
                SequenceMonoid.of(
                    variableProperty.getName().getLocalName(),
                    ","
                )
            )
        );
    }

    private SequenceMonoid<Part> serializeFunctionApplication(
        TypeScriptFunctionApplication functionApplication
    ) {
        final var localName = functionApplication.getName().getLocalName();
        return serializeParentheses(
            SequenceMonoid.of(localName, "("),
            SequenceMonoid.ofAll(
                functionApplication
                    .getExpressions()
                    .map(
                        expression ->
                            new Suffix(
                                SequenceMonoid.of(","),
                                serializeExpression(expression)
                            )
                    )
            ),
            SequenceMonoid.of(")")
        );
    }

    private SequenceMonoid<String> serializeType(
        TypeScriptType type
    ) {
        if (type instanceof TypeScriptNominalType) {
            return serializeNominalType((TypeScriptNominalType) type);
        } else if (type instanceof TypeScriptUnionType) {
            return serializeUnionType((TypeScriptUnionType) type);
        } else if (type instanceof TypeScriptTypeApplication) {
            return serializeTypeApplication((TypeScriptTypeApplication) type);
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private SequenceMonoid<String> serializeNominalType(
        TypeScriptNominalType basicType
    ) {
        return SequenceMonoid.of(basicType.getName().getLocalName());
    }

    private SequenceMonoid<String> serializeUnionType(
        TypeScriptUnionType unionType
    ) {
        return SequenceMonoid.sum(
            unionType
                .getTypes()
                .map(this::serializeType)
                .intersperse(SequenceMonoid.of(" | "))
        );
    }

    private SequenceMonoid<String> serializeTypeApplication(
        TypeScriptTypeApplication typeApplication
    ) {
        return SequenceMonoid
            .of(typeApplication.getConstructor().getLocalName(), "<")
            .plus(
                SequenceMonoid.sum(
                    typeApplication
                        .getArguments()
                        .map(this::serializeType)
                        .intersperse(SequenceMonoid.of(", "))
                )
            )
            .append(">");
    }

    private SequenceMonoid<Part> serializeEmbeddedTextFile(
        TypeScriptEmbeddedTextFile embeddedTextFile
    ) {
        return SequenceMonoid.of(
            new Line(
                SequenceMonoid.of(
                    "export const ",
                    embeddedTextFile.getVariableName(),
                    " ="
                )
            ),
            SerializerHelper.serializeMultilineList(
                line ->
                    serializeStringLiteral(
                        new TypeScriptStringLiteral(line + "\n")
                    ),
                " +",
                List.ofAll(
                    Arrays.asList(
                        embeddedTextFile
                            .getContent()
                            .split("\\n")

                    )
                )
            )
        );
    }

    private SequenceMonoid<Part> serializeParentheses(
        SequenceMonoid<String> prefix,
        SequenceMonoid<Part> within,
        SequenceMonoid<String> suffix
    ) {
        return within.length() == 0 ?
            SequenceMonoid.of(new Line(prefix.plus(suffix))) :
            SequenceMonoid
                .of(
                    new Line(prefix),
                    new IndentedBlock(within),
                    new Line(suffix)
                );
    }
}
