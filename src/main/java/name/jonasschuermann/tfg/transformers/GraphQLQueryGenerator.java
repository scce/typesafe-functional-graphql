package name.jonasschuermann.tfg.transformers;

import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import name.jonasschuermann.tfg.UnhandledCaseException;
import name.jonasschuermann.tfg.languages.graphql.ast.*;
import name.jonasschuermann.tfg.languages.typedtfg.ast.*;

import java.util.function.Function;

public final class GraphQLQueryGenerator {
    public Map<String, GraphQLFile> generate(
        TypedTFGFile file
    ) {
        return file.getQueries()
            .filter(selection -> selection.getType().equals("Query"))
            .toMap(selection -> {
                Map<String, GraphQLExpression> scope =
                    generateInitialScope(
                        selection.getArgumentDeclarations()
                    );
                return new Tuple2<>(
                    selection.getName(),
                    generateFile(
                        generateSelectionStore(file.getQueries()),
                        scope,
                        selection
                    )
                );
            });
    }

    private Map<String, TypedTFGQuery> generateSelectionStore(
        List<TypedTFGQuery> queries
    ) {
        return queries.toMap(
            TypedTFGQuery::getName,
            Function.identity()
        );
    }

    private Map<String, GraphQLExpression> generateInitialScope(
        List<TypedTFGArgumentDeclaration> argumentDeclarations
    ) {
        return argumentDeclarations.toMap(
            TypedTFGArgumentDeclaration::getName,
            argument -> new GraphQLVariable(argument.getName())
        );
    }


    private GraphQLFile generateFile(
        Map<String, TypedTFGQuery> queryStore,
        Map<String, GraphQLExpression> scope,
        TypedTFGQuery query
    ) {
        return new GraphQLFile(
            generateQueryIntend(query.getIntend()),
            query.getName(),
            query.getArgumentDeclarations()
                .map(this::generateArgumentDeclaration),
            generateInterface(queryStore, scope, query)
        );
    }

    private GraphQLQueryIntend generateQueryIntend(TypedTFGQueryIntend intend) {
        switch (intend) {
            case Query:
                return GraphQLQueryIntend.Query;
            case Mutation:
                return GraphQLQueryIntend.Mutation;
            default:
                throw new UnhandledCaseException(intend);
        }
    }

    private GraphQLInterface generateInterface(
        Map<String, TypedTFGQuery> selectionStore,
        Map<String, GraphQLExpression> scope,
        TypedTFGQuery query
    ) {
        return new GraphQLInterface(
            query
                .getSelections()
                .map(selection ->
                    generateSelectionProperty(
                        selectionStore,
                        scope,
                        selection
                    )
                )
        );
    }

    private GraphQLArgumentDeclaration generateArgumentDeclaration(
        TypedTFGArgumentDeclaration argumentDeclaration
    ) {
        return new GraphQLArgumentDeclaration(
            argumentDeclaration.getName(),
            argumentDeclaration.getType()
        );
    }

    private GraphQLProperty generateSelectionProperty(
        Map<String, TypedTFGQuery> selectionStore,
        Map<String, GraphQLExpression> scope,
        TypedTFGSelection selection
    ) {
        return generateAnchorProperty(
            selectionStore,
            scope,
            selection.getSubselection()
                .map(subselection ->
                    generateInterface(
                        selectionStore,
                        generateScope(
                            subselection.getArgumentApplications(),
                            scope
                        ),
                        mapGetForce(
                            selectionStore,
                            subselection.getName()
                        )
                    )
                ),
            selection.getAnchor()
        );
    }

    private GraphQLProperty generateAnchorProperty(
        Map<String, TypedTFGQuery> selectionStore,
        Map<String, GraphQLExpression> scope,
        Option<GraphQLInterface> subselectionInterface,
        TypedTFGAnchor anchor
    ) {
        final var property = anchor.getProperty();

        final Function<Option<GraphQLInterface>, GraphQLProperty> generateQueryProperty =
            queryInterface -> new GraphQLProperty(
                property.getName(),
                property.getAlias(),
                property
                    .getArguments()
                    .map(argument -> generateArgumentApplication(scope, argument)),
                queryInterface
            );

        if (anchor instanceof TypedTFGParentDeepSelection) {
            final var parentDeepSelection =
                (TypedTFGParentDeepSelection) anchor;

            return generateQueryProperty.apply(
                Option.some(
                    new GraphQLInterface(
                        List.of(
                            generateAnchorProperty(
                                selectionStore,
                                scope,
                                subselectionInterface,
                                parentDeepSelection.getChild()
                            )
                        )
                    )
                )
            );
        } else if (anchor instanceof TypedTFGChildrenDeepSelection) {
            final var childrenDeepSelection =
                (TypedTFGChildrenDeepSelection) anchor;

            return generateQueryProperty.apply(
                Option.of(
                    new GraphQLInterface(
                        childrenDeepSelection
                            .getChildren()
                            .map(selection ->
                                generateSelectionProperty(
                                    selectionStore,
                                    scope,
                                    selection
                                )
                            )
                    )
                )
            );
        } else if (anchor instanceof TypedTFGPropertyAnchor) {
            return generateQueryProperty.apply(subselectionInterface);
        } else {
            throw new UnhandledCaseException(anchor);
        }
    }

    private Map<String, GraphQLExpression> generateScope(
        List<TypedTFGArgumentApplication> arguments,
        Map<String, GraphQLExpression> previousScope
    ) {
        return arguments.toMap(
            TypedTFGArgumentApplication::getName,
            argument -> generateExpression(
                previousScope,
                argument.getExpression()
            )
        );
    }

    private GraphQLArgumentApplication generateArgumentApplication(
        Map<String, GraphQLExpression> scope,
        TypedTFGArgumentApplication argumentApplication
    ) {
        return new GraphQLArgumentApplication(
            argumentApplication.getName(),
            generateExpression(scope, argumentApplication.getExpression())
        );
    }

    private GraphQLExpression generateExpression(
        Map<String, GraphQLExpression> scope,
        TypedTFGExpression expression
    ) {
        if (expression instanceof TypedTFGVariable) {
            return mapGetForce(
                scope,
                ((TypedTFGVariable) expression).getName()
            );
        } else if (expression instanceof TypedTFGLiteral) {
            return generateLiteral(scope, (TypedTFGLiteral) expression);
        } else {
            throw new UnhandledCaseException(expression);
        }
    }

    private GraphQLLiteral generateLiteral(
        Map<String, GraphQLExpression> scope,
        TypedTFGLiteral literal
    ) {
        if (literal instanceof TypedTFGIntLiteral) {
            return new GraphQLIntLiteral(
                ((TypedTFGIntLiteral) literal).getRawValue()
            );
        } else if (literal instanceof TypedTFGFloatLiteral) {
            return new GraphQLFloatLiteral(
                ((TypedTFGFloatLiteral) literal).getRawValue()
            );
        } else if (literal instanceof TypedTFGStringLiteral) {
            return new GraphQLStringLiteral(
                ((TypedTFGStringLiteral) literal).getRawValue()
            );
        } else if (literal instanceof TypedTFGBooleanLiteral) {
            return new GraphQLBooleanLiteral(
                ((TypedTFGBooleanLiteral) literal).getValue()
            );
        } else if (literal instanceof TypedTFGNullLiteral) {
            return new GraphQLNullLiteral();
        } else if (literal instanceof TypedTFGEnumLiteral) {
            return new GraphQLEnumLiteral(
                ((TypedTFGEnumLiteral) literal).getRawValue()
            );
        } else if (literal instanceof TypedTFGListLiteral) {
            return new GraphQLListLiteral(
                ((TypedTFGListLiteral) literal)
                    .getItems()
                    .map(item -> generateExpression(scope, item))
            );
        } else if (literal instanceof TypedTFGRecordLiteral) {
            final var recordLiteral = (TypedTFGRecordLiteral) literal;
            return new GraphQLRecordLiteral(
                recordLiteral.getProperties().map(
                    property ->
                        new GraphQLRecordLiteralProperty(
                            property.getName(),
                            generateExpression(scope, property.getValue())
                        )
                )
            );
        } else {
            throw new UnhandledCaseException(literal);
        }
    }

    private <V> V mapGetForce(Map<String, V> map, String key) {
        return map.get(key).getOrElseThrow(() -> {
            String keys = String.join(", ", map.keySet());
            throw new RuntimeException(
                "Missing key: " + key + ", available: " + keys
            );
        });
    }
}
