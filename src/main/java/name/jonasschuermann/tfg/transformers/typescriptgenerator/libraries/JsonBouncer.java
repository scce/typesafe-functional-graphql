package name.jonasschuermann.tfg.transformers.typescriptgenerator.libraries;

import name.jonasschuermann.tfg.languages.typescript.ast.TypeScriptImportedReference;
import name.jonasschuermann.tfg.languages.typescript.ast.TypeScriptReference;

public class JsonBouncer {
    private static final String moduleName = "json-bouncer";

    private static TypeScriptReference reference(String name) {
        return new TypeScriptImportedReference(moduleName, name);
    }

    public static final TypeScriptReference nullable = reference("nullable");

    public static final TypeScriptReference array = reference("array");

    public static final TypeScriptReference field = reference("field");

    public static final TypeScriptReference null_ = reference("null");

    public static final TypeScriptReference object = reference("object");

    public static final TypeScriptReference _Decoder = reference("Decoder");
}
