package name.jonasschuermann.tfg.transformers.typescriptgenerator;

import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import io.vavr.collection.TreeSet;
import name.jonasschuermann.tfg.UnhandledCaseException;
import name.jonasschuermann.tfg.languages.common.ast.*;
import name.jonasschuermann.tfg.languages.typedtfg.ast.*;
import name.jonasschuermann.tfg.languages.typescript.TypeScriptReferenceCollector;
import name.jonasschuermann.tfg.languages.typescript.ast.*;
import name.jonasschuermann.tfg.transformers.typescriptgenerator.libraries.JsonBouncer;

public final class TypeScriptGenerator {
    private static final String jsonBouncerModule = "json-bouncer";

    private final TypeScriptReferenceCollector referenceCollector;

    public TypeScriptGenerator(
        TypeScriptReferenceCollector referenceCollector
    ) {
        this.referenceCollector = referenceCollector;
    }

    public List<TypeScriptFile> generate(
        Map<String, String> graphQLQueries,
        TypedTFGFile file
    ) {
        return file.getQueries().map(
            query -> generateTypeScriptFile(graphQLQueries, query)
        );
    }

    private TypeScriptFile generateTypeScriptFile(
        Map<String, String> graphQLQueries,
        TypedTFGQuery query
    ) {
        final var topLevelStatements =
            graphQLQueries
                .get(query.getName())
                .map(graphQLQuery ->
                    List.of(
                        generateQueryFunction(query),
                        generateEmbeddedGraphQLQuery(graphQLQuery),
                        generateRequestInterface(query),
                        generateArgumentsInterface(query),
                        generateInterface(query),
                        generateDecoder(query)
                    )
                )
                .getOrElse(
                    List.of(
                        generateInterface(query),
                        generateDecoder(query)
                    )
                );
        final var imports = generateImports(topLevelStatements);
        final var filename = upperCaseName(query.getName()) + ".ts";
        return new TypeScriptFile(filename, imports, topLevelStatements);
    }

    private List<TypeScriptImport> generateImports(
        List<TypeScriptTopLevelStatement> topLevelStatements
    ) {
        var references =
            referenceCollector.collectReferences(topLevelStatements);

        return references
            .foldLeft(
                TreeSet.empty(),
                (
                    Set<TypeScriptImportedReference> importReferences,
                    TypeScriptReference reference
                ) ->
                    reference instanceof TypeScriptImportedReference ?
                        importReferences.add(
                            (TypeScriptImportedReference) reference
                        ) :
                        importReferences
            )
            .groupBy(TypeScriptImportedReference::getModule)
            .mapValues(importReferences ->
                importReferences
                    .map(TypeScriptImportedReference::getName)
                    .toList()
                    .sorted()
            )
            .foldLeft(
                List.empty(),
                (
                    List<TypeScriptImport> imports,
                    Tuple2<String, List<String>> moduleReferences
                ) ->
                    imports.append(
                        new TypeScriptImport(
                            moduleReferences._1(),
                            moduleReferences._2()
                        )
                    )
            )
            .sorted();
    }

    private TypeScriptTopLevelStatement generateQueryFunction(
        TypedTFGQuery query
    ) {
        return new TypeScriptFunctionDeclaration(
            "request" + upperCaseName(query.getName()),
            query
                .getArgumentDeclarations()
                .map(argumentDeclaration ->
                    new TypeScriptArgumentDeclaration(
                        argumentDeclaration.getName(),
                        generateType(argumentDeclaration.getType())
                    )
                ),
            new TypeScriptNominalType(
                new TypeScriptLocalReference(
                    upperCaseName(query.getName()) + "Request"
                )
            ),
            List.of(
                new TypeScriptReturnStatement(
                    new TypeScriptObjectLiteral(
                        List.of(
                            new TypeScriptObjectLiteralVariableProperty(
                                new TypeScriptLocalReference("query")
                            ),
                            new TypeScriptObjectLiteralProperty(
                                "arguments",
                                new TypeScriptObjectLiteral(
                                    query
                                        .getArgumentDeclarations()
                                        .map(TypedTFGArgumentDeclaration::getName)
                                        .map(TypeScriptLocalReference::new)
                                        .map(TypeScriptObjectLiteralVariableProperty::new)
                                )
                            )
                        )
                    )
                )
            )
        );
    }

    private TypeScriptTopLevelStatement generateEmbeddedGraphQLQuery(
        String graphQLQuery
    ) {
        return new TypeScriptEmbeddedTextFile("query", graphQLQuery);
    }

    private TypeScriptTopLevelStatement generateRequestInterface(
        TypedTFGQuery query
    ) {
        return new TypeScriptInterfaceDeclaration(
            upperCaseName(query.getName()) + "Request",
            List.of(
                new TypeScriptPropertyDeclaration(
                    "query",
                    new TypeScriptNominalType(
                        new TypeScriptLocalReference("string")
                    )
                ),
                new TypeScriptPropertyDeclaration(
                    "arguments",
                    new TypeScriptNominalType(
                        new TypeScriptLocalReference(
                            upperCaseName(query.getName()) + "Arguments"
                        )
                    )
                )
            )
        );
    }

    private TypeScriptTopLevelStatement generateArgumentsInterface(
        TypedTFGQuery query
    ) {
        return new TypeScriptInterfaceDeclaration(
            upperCaseName(query.getName()) + "Arguments",
            query.getArgumentDeclarations().map(this::generateArgumentsProperty)
        );
    }

    private TypeScriptPropertyDeclaration generateArgumentsProperty(
        TypedTFGArgumentDeclaration argumentDeclaration
    ) {
        return new TypeScriptPropertyDeclaration(
            argumentDeclaration.getName(),
            generateType(argumentDeclaration.getType())
        );
    }

    private TypeScriptTopLevelStatement generateInterface(
        TypedTFGQuery query
    ) {
        return new TypeScriptInterfaceDeclaration(
            upperCaseName(query.getName()),
            query
                .getFields()
                .map(this::generateProperty)
        );
    }

    private TypeScriptPropertyDeclaration generateProperty(
        TypedTFGField field
    ) {
        return new TypeScriptPropertyDeclaration(
            field.getName(),
            generateType(field.getType())
        );
    }

    private TypeScriptType generateType(CommonType returnType) {
        if (returnType instanceof CommonListType) {
            final var listType = (CommonListType) returnType;
            return generateNullableType(
                listType.getNullable(),
                new TypeScriptTypeApplication(
                    new TypeScriptLocalReference("ReadonlyArray"),
                    List.of(generateSimpleType(listType.getAppliedType()))
                )
            );
        } else if (returnType instanceof CommonSimpleType) {
            return generateSimpleType((CommonSimpleType) returnType);
        } else if (returnType instanceof CommonNullType) {
            return new TypeScriptNominalType(
                new TypeScriptLocalReference("null")
            );
        } else {
            throw new UnhandledCaseException(returnType);
        }
    }

    private TypeScriptType generateSimpleType(CommonSimpleType simpleType) {
        final var nominalType = simpleType.getNominalType();
        TypeScriptReference nominalReference;
        if (nominalType instanceof CommonPrimitiveType) {
            final var primitiveType = (CommonPrimitiveType) nominalType;
            nominalReference =
                new TypeScriptLocalReference(
                    generatePrimitiveType(primitiveType.getName())
                );
        } else if (nominalType instanceof CommonCompoundType) {
            final var compoundType = (CommonCompoundType) nominalType;
            nominalReference =
                new TypeScriptImportedReference(
                    generateEntityModule(compoundType.getName()),
                    compoundType.getName()
                );
        } else {
            throw new UnhandledCaseException(nominalType);
        }
        return generateNullableType(
            simpleType.getNullable(),
            new TypeScriptNominalType(nominalReference)
        );
    }

    private String generateEntityModule(String name) {
        return "./" + name;
    }

    private TypeScriptType generateNullableType(
        boolean nullable,
        TypeScriptType type
    ) {
        return nullable ?
            new TypeScriptUnionType(
                List.of(
                    type,
                    new TypeScriptNominalType(
                        new TypeScriptLocalReference("null")
                    )
                )
            ) :
            type;
    }

    private String lowerCaseName(String name) {
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }

    private String upperCaseName(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private TypeScriptTopLevelStatement generateDecoder(
        TypedTFGQuery query
    ) {
        final var upperCaseName = upperCaseName(query.getName());
        return new TypeScriptFunctionDeclaration(
            query.getName() + "Decoder",
            List.empty(),
            new TypeScriptTypeApplication(
                JsonBouncer._Decoder,
                List.of(
                    new TypeScriptNominalType(
                        new TypeScriptLocalReference(upperCaseName)
                    )
                )
            ),
            List.of(
                new TypeScriptReturnStatement(
                    new TypeScriptFunctionApplication(
                        JsonBouncer.object,
                        List.of(
                            new TypeScriptObjectLiteral(
                                query
                                    .getFields()
                                    .map(this::generatePropertyDecoder)
                            )
                        )
                    )
                )
            )
        );
    }

    private TypeScriptObjectLiteralProperty generatePropertyDecoder(
        TypedTFGField field
    ) {

        return new TypeScriptObjectLiteralProperty(
            field.getName(),
            field
                .getPath()
                .foldRight(
                    generateNominalTypeDecoder(getNominalType(field.getType())),
                    this::generatePathElementDecoder
                )
        );
    }

    private TypeScriptExpression generatePathElementDecoder(
        TypedTFGPathElement pathElement,
        TypeScriptExpression innerDecoder
    ) {
            final var listItemsNullableDecoder =
                pathElement.getListItemsNullable() ?
                    new TypeScriptFunctionApplication(
                        JsonBouncer.nullable,
                        List.of(innerDecoder)
                    ) :
                    innerDecoder;

            final var listDecoder =
                pathElement.getList() ?
                    new TypeScriptFunctionApplication(
                        JsonBouncer.array,
                        List.of(listItemsNullableDecoder)
                    ) :
                    listItemsNullableDecoder;

            final var decoder =
                pathElement.getNullable() ?
                    new TypeScriptFunctionApplication(
                        JsonBouncer.nullable,
                        List.of(listDecoder)
                    ) :
                    listDecoder;

            return new TypeScriptFunctionApplication(
                JsonBouncer.field,
                List.of(
                    new TypeScriptStringLiteral(pathElement.getName()),
                    decoder
                )
            );
    }

    private CommonNominalType getNominalType(CommonType type) {
        if (type instanceof CommonListType) {
            return ((CommonListType) type).getAppliedType().getNominalType();
        } else if (type instanceof CommonSimpleType) {
            return ((CommonSimpleType) type).getNominalType();
        } else if (type instanceof CommonNullType) {
            throw new RuntimeException("Null type not possible here");
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private TypeScriptExpression generateNominalTypeDecoder(
        CommonNominalType nominalType
    ) {
        TypeScriptReference decoderReference;
        if (nominalType instanceof CommonPrimitiveType) {
            final var primitiveType = (CommonPrimitiveType) nominalType;
            decoderReference =
                new TypeScriptImportedReference(
                    jsonBouncerModule,
                    generatePrimitiveType(primitiveType.getName())
                );
        } else if (nominalType instanceof CommonCompoundType) {
            final var compoundType = (CommonCompoundType) nominalType;
            decoderReference =
                new TypeScriptImportedReference(
                    generateEntityModule(compoundType.getName()),
                    lowerCaseName(compoundType.getName()) + "Decoder"
                );
        } else {
            throw new UnhandledCaseException(nominalType);
        }
        return new TypeScriptFunctionApplication(decoderReference, List.empty());
    }

    private String generatePrimitiveType(CommonPrimitiveTypeEnum type) {
        switch (type) {
            case Boolean:
                return "boolean";
            case Int:
            case Float:
                return "number";
            case String:
            case ID:
                return "string";
            default:
                throw new UnhandledCaseException(type);
        }
    }
}
