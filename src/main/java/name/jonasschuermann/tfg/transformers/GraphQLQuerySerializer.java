package name.jonasschuermann.tfg.transformers;

import name.jonasschuermann.tfg.SerializerHelper;
import name.jonasschuermann.tfg.SequenceMonoid;
import name.jonasschuermann.tfg.UnhandledCaseException;
import name.jonasschuermann.tfg.languages.common.CommonTypeRenderer;
import name.jonasschuermann.tfg.languages.common.ast.CommonType;
import name.jonasschuermann.tfg.languages.graphql.ast.*;
import name.jonasschuermann.tfg.languages.pretty.ast.IndentedBlock;
import name.jonasschuermann.tfg.languages.pretty.ast.Line;
import name.jonasschuermann.tfg.languages.pretty.ast.Part;

public final class GraphQLQuerySerializer {
    public SequenceMonoid<Part> serialize(GraphQLFile file) {
        var queryName = SequenceMonoid.of(
            serializeQueryIntend(file.getIntend()),
            " ",
            file.getName()
        );

        SequenceMonoid<Part> argumentDeclaration =
            file.getArguments().length() > 0 ?
                SequenceMonoid.of(
                    new Line(queryName.append("(")),
                    SerializerHelper.serializeMultilineList(
                        this::serializeArgumentDeclaration,
                        ",",
                        file.getArguments()
                    ),
                    new Line(SequenceMonoid.of(") {"))
                ) :
                SequenceMonoid.of(new Line(queryName.append(" {")));

        return
            argumentDeclaration
                .append(
                    new IndentedBlock(
                        SequenceMonoid.sum(
                            file.getRootInterface()
                                .getProperties()
                                .map(this::serializeQueryProperty)
                        )
                    )
                )
                .append(
                    new Line(SequenceMonoid.of("}"))
                );
    }

    private String serializeQueryIntend(GraphQLQueryIntend intend) {
        switch (intend) {
            case Query:
                return "query";
            case Mutation:
                return "mutation";
            default:
                throw new UnhandledCaseException(intend);
        }
    }

    private SequenceMonoid<String> serializeArgumentDeclaration(
        GraphQLArgumentDeclaration argumentDeclaration
    ) {
        final var name = argumentDeclaration.getName();
        final var type = argumentDeclaration.getType();
        return SequenceMonoid
            .of("$", name, ": ")
            .plus(serializeCommonType(type));
    }

    private SequenceMonoid<String> serializeCommonType(
        CommonType type
    ) {
        return SequenceMonoid.of(CommonTypeRenderer.render(type));
    }

    private SequenceMonoid<Part> serializeQueryProperty(
        GraphQLProperty graphQLProperty
    ) {
        var aliasedName =
            graphQLProperty
                .getAlias()
                .map(alias -> SequenceMonoid.of(alias, ": "))
                .getOrElse(SequenceMonoid.zero())
                .append(graphQLProperty.getName());

        SequenceMonoid<String> openingCurlyBrace =
            graphQLProperty.getSubquery().isDefined() ?
                SequenceMonoid.of(" {") :
                SequenceMonoid.zero();

        SequenceMonoid<Part> propertyApplication =
            graphQLProperty.getArguments().length() > 0 ?
                SequenceMonoid.of(
                    new Line(aliasedName.append("(")),
                    SerializerHelper.serializeMultilineList(
                        this::serializeArgumentApplication,
                        ",",
                        graphQLProperty.getArguments()
                    ),
                    new Line(SequenceMonoid.of(")").plus(openingCurlyBrace))
                ) :
                SequenceMonoid.of(
                    new Line(aliasedName.plus(openingCurlyBrace))
                );

        return propertyApplication
            .plus(
                graphQLProperty
                    .getSubquery()
                    .map(
                        subquery -> SequenceMonoid.<Part>of(
                            new IndentedBlock(
                                SequenceMonoid.sum(
                                    subquery
                                        .getProperties()
                                        .map(this::serializeQueryProperty)
                                )
                            )
                        )
                    )
                    .getOrElse(SequenceMonoid.zero())
            )
            .plus(
                graphQLProperty.getSubquery().isDefined() ?
                    SequenceMonoid.of(Line.of("}")) :
                    SequenceMonoid.zero()
            );
    }

    private SequenceMonoid<String> serializeArgumentApplication(
        GraphQLArgumentApplication argument
    ) {
        return SequenceMonoid
            .of(argument.getName(), ": ")
            .plus(serializeExpression(argument.getExpression()));
    }

    private SequenceMonoid<String> serializeExpression(
        GraphQLExpression expression
    ) {
        if (expression instanceof GraphQLVariable) {
            return serializeVariable((GraphQLVariable) expression);
        } else if (expression instanceof GraphQLLiteral) {
            return serializeLiteral((GraphQLLiteral) expression);
        } else {
            throw new UnhandledCaseException(expression);
        }
    }

    private SequenceMonoid<String> serializeLiteral(
        GraphQLLiteral literal
    ) {
        if (literal instanceof GraphQLIntLiteral) {
            return serializeIntLiteral((GraphQLIntLiteral) literal);
        } else if (literal instanceof GraphQLFloatLiteral) {
            return serializeFloatLiteral((GraphQLFloatLiteral) literal);
        } else if (literal instanceof GraphQLStringLiteral) {
            return serializeStringLiteral((GraphQLStringLiteral) literal);
        } else if (literal instanceof GraphQLBooleanLiteral) {
            return serializeBooleanLiteral((GraphQLBooleanLiteral) literal);
        } else if (literal instanceof GraphQLEnumLiteral) {
            return serializeEnumLiteral((GraphQLEnumLiteral) literal);
        } else if (literal instanceof GraphQLListLiteral) {
            return serializeListLiteral((GraphQLListLiteral) literal);
        } else if (literal instanceof GraphQLRecordLiteral) {
            return serializeRecordLiteral((GraphQLRecordLiteral) literal);
        } else {
            throw new UnhandledCaseException(literal);
        }
    }

    private SequenceMonoid<String> serializeVariable(
        GraphQLVariable variable
    ) {
        return SequenceMonoid.of("$", variable.getName());
    }

    private SequenceMonoid<String> serializeIntLiteral(
        GraphQLIntLiteral intLiteral
    ) {
        return SequenceMonoid.of(intLiteral.getRawValue());
    }

    private SequenceMonoid<String> serializeFloatLiteral(
        GraphQLFloatLiteral floatLiteral
    ) {
        return SequenceMonoid.of(floatLiteral.getRawValue());
    }

    private SequenceMonoid<String> serializeStringLiteral(
        GraphQLStringLiteral stringLiteral
    ) {
        return SequenceMonoid.of(stringLiteral.getRawValue());
    }


    private SequenceMonoid<String> serializeBooleanLiteral(
        GraphQLBooleanLiteral booleanLiteral
    ) {
        return SequenceMonoid.of(booleanLiteral.getValue() ? "true" : "false");
    }

    private SequenceMonoid<String> serializeEnumLiteral(
        GraphQLEnumLiteral enumLiteral
    ) {
        return SequenceMonoid.of(enumLiteral.getRawValue());
    }

    private SequenceMonoid<String> serializeListLiteral(
        GraphQLListLiteral listLiteral
    ) {
        return SequenceMonoid.of("[ ")
            .plus(
                SerializerHelper.serializeInlineList(
                    this::serializeExpression,
                    ", ",
                    listLiteral.getItems()
                )
            )
            .append(" ]");
    }

    private SequenceMonoid<String> serializeRecordLiteral(
        GraphQLRecordLiteral recordLiteral
    ) {
        return SequenceMonoid.of("{ ")
            .plus(
                SerializerHelper.serializeInlineList(
                    this::serializeRecordLiteralProperty,
                    ", ",
                    recordLiteral.getProperties()
                )
            )
            .append(" }");
    }

    private SequenceMonoid<String> serializeRecordLiteralProperty(
        GraphQLRecordLiteralProperty recordLiteralProperty
    ) {
        return SequenceMonoid.of(recordLiteralProperty.getName(), ": ")
            .plus(serializeExpression(recordLiteralProperty.getValue()));
    }
}
