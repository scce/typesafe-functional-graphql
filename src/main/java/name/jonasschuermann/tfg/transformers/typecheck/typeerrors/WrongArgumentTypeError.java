package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import name.jonasschuermann.tfg.languages.tfg.ast.TFGExpression;

import java.util.Objects;

public final class WrongArgumentTypeError extends TypeError {
    private String query;
    private String property;
    private String targetArgument;
    private String declaredType;
    private TFGExpression expression;

    public WrongArgumentTypeError(
        String query,
        String property,
        String targetArgument,
        String declaredType,
        TFGExpression expression
    ) {
        this.query = query;
        this.property = property;
        this.targetArgument = targetArgument;
        this.declaredType = declaredType;
        this.expression = expression;
    }

    public String getQuery() {
        return query;
    }

    public String getProperty() {
        return property;
    }

    public String getTargetArgument() {
        return targetArgument;
    }

    public String getDeclaredType() {
        return declaredType;
    }

    public TFGExpression getExpression() {
        return expression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WrongArgumentTypeError that = (WrongArgumentTypeError) o;
        return query.equals(that.query) &&
            property.equals(that.property) &&
            targetArgument.equals(that.targetArgument) &&
            declaredType.equals(that.declaredType) &&
            expression.equals(that.expression);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            query,
            property,
            targetArgument,
            declaredType,
            expression
        );
    }

    @Override
    public String toString() {
        return "WrongArgumentTypeError{" +
            "query='" + query + '\'' +
            ", property='" + property + '\'' +
            ", targetArgument='" + targetArgument + '\'' +
            ", declaredType='" + declaredType + '\'' +
            ", expression=" + expression +
            '}';
    }
}
