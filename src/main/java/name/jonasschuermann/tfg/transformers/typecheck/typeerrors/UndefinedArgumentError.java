package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import java.util.Objects;

public final class UndefinedArgumentError extends TypeError {
    private String query;
    private String property;
    private String targetArgument;

    public UndefinedArgumentError(
        String query,
        String property,
        String targetArgument
    ) {
        this.query = query;
        this.property = property;
        this.targetArgument = targetArgument;
    }

    public String getQuery() {
        return query;
    }

    public String getProperty() {
        return property;
    }

    public String getTargetArgument() {
        return targetArgument;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UndefinedArgumentError that = (UndefinedArgumentError) o;
        return query.equals(that.query) &&
            property.equals(that.property) &&
            targetArgument.equals(that.targetArgument);
    }

    @Override
    public int hashCode() {
        return Objects.hash(query, property, targetArgument);
    }

    @Override
    public String toString() {
        return "UndefinedArgumentError{" +
            "query='" + query + '\'' +
            ", property='" + property + '\'' +
            ", targetArgument='" + targetArgument + '\'' +
            '}';
    }
}
