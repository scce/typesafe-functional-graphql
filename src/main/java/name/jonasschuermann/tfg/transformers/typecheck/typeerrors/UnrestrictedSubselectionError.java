package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import java.util.Objects;

public final class UnrestrictedSubselectionError extends TypeError {
    private String query;
    private String property;

    public UnrestrictedSubselectionError(
        String query,
        String property
    ) {
        this.query = query;
        this.property = property;
    }

    public String getQuery() {
        return query;
    }

    public String getProperty() {
        return property;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnrestrictedSubselectionError that = (UnrestrictedSubselectionError) o;
        return query.equals(that.query) &&
            property.equals(that.property);
    }

    @Override
    public int hashCode() {
        return Objects.hash(query, property);
    }

    @Override
    public String toString() {
        return "UnrestrictedSubselectionError{" +
            "query='" + query + '\'' +
            ", property='" + property + '\'' +
            '}';
    }
}
