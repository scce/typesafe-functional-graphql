package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import java.util.Objects;

public final class UndefinedRecordPropertyError extends TypeError {
    private String query;
    private String property;
    private String targetArgument;
    private String declaredType;
    private String inputPropertyName;

    public UndefinedRecordPropertyError(
        String query,
        String property,
        String targetArgument,
        String declaredType,
        String inputPropertyName
    ) {
        this.query = query;
        this.property = property;
        this.targetArgument = targetArgument;
        this.declaredType = declaredType;
        this.inputPropertyName = inputPropertyName;
    }

    public String getQuery() {
        return query;
    }

    public String getProperty() {
        return property;
    }

    public String getTargetArgument() {
        return targetArgument;
    }

    public String getDeclaredType() {
        return declaredType;
    }

    public String getInputPropertyName() {
        return inputPropertyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UndefinedRecordPropertyError that = (UndefinedRecordPropertyError) o;
        return query.equals(that.query) && property.equals(that.property) && targetArgument.equals(that.targetArgument) && declaredType.equals(that.declaredType) && inputPropertyName.equals(that.inputPropertyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(query, property, targetArgument, declaredType, inputPropertyName);
    }

    @Override
    public String toString() {
        return "UndefinedRecordPropertyError{" +
            "query='" + query + '\'' +
            ", property='" + property + '\'' +
            ", targetArgument='" + targetArgument + '\'' +
            ", declaredType='" + declaredType + '\'' +
            ", propertyName='" + inputPropertyName + '\'' +
            '}';
    }
}
