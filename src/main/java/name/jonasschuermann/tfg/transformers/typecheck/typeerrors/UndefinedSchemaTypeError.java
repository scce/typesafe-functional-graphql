package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import java.util.Objects;

public final class UndefinedSchemaTypeError extends TypeError {
    private String queryName;
    private String targetSchemaType;

    public UndefinedSchemaTypeError(String queryName, String targetSchemaType) {
        this.queryName = queryName;
        this.targetSchemaType = targetSchemaType;
    }

    public String getQueryName() {
        return queryName;
    }

    public String getTargetSchemaType() {
        return targetSchemaType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UndefinedSchemaTypeError that = (UndefinedSchemaTypeError) o;
        return queryName.equals(that.queryName) &&
            targetSchemaType.equals(that.targetSchemaType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryName, targetSchemaType);
    }

    @Override
    public String toString() {
        return "SelectionUndefinedSchemaType{" +
            "queryName='" + queryName + '\'' +
            ", targetSchemaType='" + targetSchemaType + '\'' +
            '}';
    }
}
