package name.jonasschuermann.tfg.transformers.typecheck;

import io.vavr.Function2;
import io.vavr.collection.List;
import io.vavr.control.Either;
import io.vavr.control.Option;
import name.jonasschuermann.tfg.transformers.typecheck.typeerrors.TypeError;

import java.util.function.Function;
import java.util.function.Supplier;

public final class Typecheck<T> {
    private Either<List<TypeError>, T> either;

    public Typecheck(Either<List<TypeError>, T> either) {
        this.either = either;
    }

    public Either<List<TypeError>, T> unpack() {
        return either;
    }

    public static <T> Typecheck<T> right(T right) {
        return new Typecheck<>(Either.right(right));
    }

    public static <T> Typecheck<T> left(List<TypeError> left) {
        return new Typecheck<>(Either.left(left));
    }

    public <U> Typecheck<U> map(Function<T, U> f) {
        return new Typecheck<>(either.map(f));
    }

    public Typecheck<T> mapLeft(Function<List<TypeError>, List<TypeError>> f) {
        return new Typecheck<>(either.mapLeft(f));
    }

    public <U> Typecheck<U> flatMap(Function<T, Typecheck<U>> f) {
        return new Typecheck<>(either.flatMap(x -> f.apply(x).unpack()));
    }

    public <U, R> Typecheck<R> combine(
        Function2<T, U, R> f,
        Typecheck<U> other
    ) {
        return this
            .mapLeft(errors -> errors.appendAll(other.getErrors()))
            .flatMap(right1 ->
                other.flatMap(right2 ->
                    Typecheck.right(f.apply(right1, right2))
                )
            );
    }

    public static <T> Typecheck<T> fromOption(
        Option<T> option,
        List<TypeError> errors
    ) {
        return option.map(Typecheck::right).getOrElse(Typecheck.left(errors));
    }

    public static <T> Typecheck<T> fromOption(
        Option<T> option,
        Supplier<List<TypeError>> errors
    ) {
        return option
            .map(Typecheck::right)
            .getOrElse(() -> Typecheck.left(errors.get()));
    }

    public List<TypeError> getErrors() {
        return either.swap().getOrElse(List.empty());
    }

    public static <A, B> Typecheck<List<B>> combineTraversal(
        Function<A, Typecheck<B>> fn,
        List<A> list
    ) {
        return list
            .map(fn)
            .foldLeft(
                Typecheck.right(List.empty()),
                (combined, typecheck) ->
                    combined.combine(List::append, typecheck)
            );
    }

    public static <A, B> Typecheck<Option<B>> combineTraversal(
        Function<A, Typecheck<B>> fn,
        Option<A> option
    ) {
        return option
            .map(fn)
            .map(typecheck -> typecheck.map(Option::of))
            .getOrElse(() -> Typecheck.right(Option.none()));
    }

    public Typecheck<T> considerAdditionalChecks(
        List<TypeError> errors
    ) {
        return errors.isEmpty() ?
            this :
            Typecheck.left(errors.appendAll(this.getErrors()));
    }
}
