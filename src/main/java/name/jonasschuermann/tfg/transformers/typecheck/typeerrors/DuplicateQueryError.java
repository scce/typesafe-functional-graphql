package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import java.util.Objects;

public final class DuplicateQueryError extends TypeError {
    private String name;

    public DuplicateQueryError(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DuplicateQueryError that = (DuplicateQueryError) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "DuplicateQueryError{" +
            "name='" + name + '\'' +
            '}';
    }
}
