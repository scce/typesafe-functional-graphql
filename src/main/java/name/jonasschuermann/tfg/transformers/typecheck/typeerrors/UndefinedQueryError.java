package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import java.util.Objects;

public final class UndefinedQueryError extends TypeError {
    private String query;
    private String property;
    private String targetQuery;

    public UndefinedQueryError(String query, String property, String targetQuery) {
        this.query = query;
        this.property = property;
        this.targetQuery = targetQuery;
    }

    public String getQuery() {
        return query;
    }

    public String getProperty() {
        return property;
    }

    public String getTargetQuery() {
        return targetQuery;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UndefinedQueryError that = (UndefinedQueryError) o;
        return query.equals(that.query) &&
            property.equals(that.property) &&
            targetQuery.equals(that.targetQuery);
    }

    @Override
    public int hashCode() {
        return Objects.hash(query, property, targetQuery);
    }

    @Override
    public String toString() {
        return "UndefinedQueryError{" +
            "query='" + query + '\'' +
            ", property='" + property + '\'' +
            ", targetQuery='" + targetQuery + '\'' +
            '}';
    }
}
