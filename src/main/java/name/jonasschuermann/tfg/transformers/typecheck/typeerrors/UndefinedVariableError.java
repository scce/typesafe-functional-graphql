package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import java.util.Objects;

public final class UndefinedVariableError extends TypeError {
    private String query;
    private String property;
    private String targetArgument;
    private String referencedVariable;

    public UndefinedVariableError(
        String query,
        String property,
        String targetArgument,
        String referencedVariable
    ) {
        this.query = query;
        this.property = property;
        this.targetArgument = targetArgument;
        this.referencedVariable = referencedVariable;
    }

    public String getQuery() {
        return query;
    }

    public String getProperty() {
        return property;
    }

    public String getTargetArgument() {
        return targetArgument;
    }

    public String getReferencedVariable() {
        return referencedVariable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UndefinedVariableError that = (UndefinedVariableError) o;
        return query.equals(that.query) &&
            property.equals(that.property) &&
            targetArgument.equals(that.targetArgument) &&
            referencedVariable.equals(that.referencedVariable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(query, property, targetArgument, referencedVariable);
    }

    @Override
    public String toString() {
        return "UndefinedVariableError{" +
            "query='" + query + '\'' +
            ", property='" + property + '\'' +
            ", targetArgument='" + targetArgument + '\'' +
            ", referencedVariable='" + referencedVariable + '\'' +
            '}';
    }
}
