package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import java.util.Objects;

public final class SubselectionTypeMismatchError extends TypeError {
    private String query;
    private String property;
    private String propertyType;
    private String targetQuery;
    private String targetQueryType;

    public SubselectionTypeMismatchError(String query, String property, String propertyType, String targetQuery, String targetQueryType) {
        this.query = query;
        this.property = property;
        this.propertyType = propertyType;
        this.targetQuery = targetQuery;
        this.targetQueryType = targetQueryType;
    }

    public String getQuery() {
        return query;
    }

    public String getProperty() {
        return property;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public String getTargetQuery() {
        return targetQuery;
    }

    public String getTargetQueryType() {
        return targetQueryType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubselectionTypeMismatchError that = (SubselectionTypeMismatchError) o;
        return query.equals(that.query) &&
            property.equals(that.property) &&
            propertyType.equals(that.propertyType) &&
            targetQuery.equals(that.targetQuery) &&
            targetQueryType.equals(that.targetQueryType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(query, property, propertyType, targetQuery, targetQueryType);
    }

    @Override
    public String toString() {
        return "SubselectionTypeMismatchError{" +
            "query='" + query + '\'' +
            ", property='" + property + '\'' +
            ", propertyType='" + propertyType + '\'' +
            ", targetQuery='" + targetQuery + '\'' +
            ", targetQueryType='" + targetQueryType + '\'' +
            '}';
    }
}
