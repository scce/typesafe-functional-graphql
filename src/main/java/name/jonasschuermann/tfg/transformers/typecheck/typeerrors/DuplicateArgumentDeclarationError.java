package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import java.util.Objects;

public final class DuplicateArgumentDeclarationError extends TypeError {
    private String queryName;
    private String argumentName;

    public DuplicateArgumentDeclarationError(String queryName, String argumentName) {
        this.queryName = queryName;
        this.argumentName = argumentName;
    }

    public String getQueryName() {
        return queryName;
    }

    public String getArgumentName() {
        return argumentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DuplicateArgumentDeclarationError that = (DuplicateArgumentDeclarationError) o;
        return queryName.equals(that.queryName) &&
            argumentName.equals(that.argumentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryName, argumentName);
    }

    @Override
    public String toString() {
        return "DuplicateArgumentDeclarationError{" +
            "queryName='" + queryName + '\'' +
            ", argumentName='" + argumentName + '\'' +
            '}';
    }
}
