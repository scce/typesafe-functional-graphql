package name.jonasschuermann.tfg.transformers.typecheck;

import graphql.language.*;
import graphql.schema.idl.TypeDefinitionRegistry;
import io.vavr.collection.List;
import io.vavr.collection.TreeSet;
import io.vavr.control.Either;
import io.vavr.control.Option;
import name.jonasschuermann.tfg.Curry;
import name.jonasschuermann.tfg.UnhandledCaseException;
import name.jonasschuermann.tfg.languages.common.CommonTypeRenderer;
import name.jonasschuermann.tfg.languages.common.NominalTypeIdentification;
import name.jonasschuermann.tfg.languages.common.ast.*;
import name.jonasschuermann.tfg.languages.tfg.ast.*;
import name.jonasschuermann.tfg.languages.typedtfg.ast.*;
import name.jonasschuermann.tfg.transformers.typecheck.typeerrors.*;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * About the "Raw use of parameterized class 'Type'" warnings
 * <p>
 * This class contains the warning "Raw use of parameterized class 'Type'"
 * several times. This can't be fixed because the definition of the class `Type`
 * (imported from graphql-java) is broken. To properly instantiate the
 * parameterized type one would have to pass a specific subclass of `Type` which
 * doesn't make sense if the subclass is not known yet. This can be seen in the
 * method FieldDefinition::getType (also from graphql-java), which returns a
 * value of type `Type` without parametrization. That's why the class `Type`
 * also has to be used without parametrization in this class.
 *
 * TODO: Check that all arguments are supplied
 */
public final class Typechecker {
    public Either<List<TypeError>, TypedTFGFile> typecheck(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGFile file
    ) {
        List<TypeError> duplicateQueryErrors =
            findDuplicateNames(
                file.getQueries().map(TFGQuery::getName)
            )
                .map(DuplicateQueryError::new);

        return Typecheck
            .combineTraversal(
                query ->
                    typecheckQuery(
                        typeDefinitionRegistry,
                        file.getQueries(),
                        query
                    ),
                file.getQueries()
            )
            .map(TypedTFGFile::new)
            .considerAdditionalChecks(duplicateQueryErrors)
            .unpack();
    }

    private List<String> findDuplicateNames(List<String> names) {
        return names
            .foldLeft(
                new DuplicateFinderState(TreeSet.empty(), TreeSet.empty()),
                (state, name) ->
                    state.getDefined().contains(name) ?
                        new DuplicateFinderState(
                            state.getDefined(),
                            state.getDuplicates().add(name)
                        ) :
                        new DuplicateFinderState(
                            state.getDefined().add(name),
                            state.getDuplicates()
                        )
            )
            .getDuplicates()
            .toList();
    }

    private static class DuplicateFinderState {
        private final TreeSet<String> defined;
        private final TreeSet<String> duplicates;

        DuplicateFinderState(
            TreeSet<String> defined,
            TreeSet<String> duplicates
        ) {
            this.defined = defined;
            this.duplicates = duplicates;
        }

        TreeSet<String> getDefined() {
            return defined;
        }

        TreeSet<String> getDuplicates() {
            return duplicates;
        }
    }

    private Typecheck<TypedTFGQuery> typecheckQuery(
        TypeDefinitionRegistry typeDefinitionRegistry,
        List<TFGQuery> queries,
        TFGQuery query
    ) {
        return selectObjectType(typeDefinitionRegistry, query, query.getType())
            .flatMap(objectTypeDefinition ->
                Typecheck.combineTraversal(
                    selection -> typecheckSelection(
                        typeDefinitionRegistry,
                        queries,
                        query,
                        objectTypeDefinition,
                        selection
                    ),
                    query.getSelections()
                )
            )
            .flatMap(typedSelections ->
                Typecheck
                    .right(Curry.curry6(TypedTFGQuery::new))
                    .combine(
                        Function::apply,
                        Typecheck.right(typecheckQueryIntend(query.getIntend()))
                    )
                    .combine(
                        Function::apply,
                        Typecheck.right(query.getName())
                    )
                    .combine(
                        Function::apply,
                        Typecheck.right(
                            query
                                .getArgumentDeclarations()
                                .map(this::typecheckArgumentDeclaration)
                        )
                    )
                    .combine(
                        Function::apply,
                        Typecheck.right(query.getType())
                    )
                    .combine(Function::apply, Typecheck.right(typedSelections))
                    .combine(
                        Function::apply,
                        selectObjectType(typeDefinitionRegistry, query, query.getType())
                            .flatMap(objectTypeDefinition ->
                                typecheckFields(
                                    queries,
                                    query,
                                    List.empty(),
                                    false,
                                    false,
                                    false,
                                    typedSelections
                                )
                            )
                    )
            )
            .considerAdditionalChecks(
                findDuplicateNames(
                    query.getArgumentDeclarations()
                        .map(TFGArgumentDeclaration::getName)
                )
                    .map(argumentName ->
                        new DuplicateArgumentDeclarationError(
                            query.getName(),
                            argumentName
                        )
                    )
            );
    }

    private TypedTFGQueryIntend typecheckQueryIntend(TFGQueryIntend intend) {
        switch (intend) {
            case Query:
                return TypedTFGQueryIntend.Query;
            case Mutation:
                return TypedTFGQueryIntend.Mutation;
            default:
                throw new UnhandledCaseException(intend);
        }
    }

    private Typecheck<TypeDefinition> selectTypeDefinition(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        String typeName
    ) {
        return Typecheck
            .fromOption(
                Option.ofOptional(
                    typeDefinitionRegistry.getType(typeName)
                ),
                () -> List.of(
                    new UndefinedSchemaTypeError(
                        query.getName(),
                        typeName
                    )
                )
            );
    }

    private Typecheck<ObjectTypeDefinition> selectObjectType(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        String typeName
    ) {
        return selectTypeDefinition(typeDefinitionRegistry, query, typeName)
            .flatMap(typeDefinition ->
                typeDefinition instanceof ObjectTypeDefinition ?
                    Typecheck.right(
                        (ObjectTypeDefinition) typeDefinition
                    ) :
                    Typecheck.left(
                        List.of(
                            new WrongTypeError(
                                query.getName(),
                                typeName
                            )
                        )
                    )
            );
    }

    private Typecheck<FieldDefinition> selectFieldDefinition(
        TFGQuery query,
        ObjectTypeDefinition objectTypeDefinition,
        String name
    ) {
        return Typecheck
            .fromOption(
                List.ofAll(objectTypeDefinition.getFieldDefinitions())
                    .find(fieldDefinition ->
                        fieldDefinition.getName().equals(name)
                    ),
                () ->
                    List.of(
                        new UndefinedSchemaPropertyError(
                            query.getName(),
                            name,
                            query.getType()
                        )
                    )
            );
    }

    private TypedTFGArgumentDeclaration typecheckArgumentDeclaration(
        TFGArgumentDeclaration argumentDeclaration
    ) {
        return new TypedTFGArgumentDeclaration(
            argumentDeclaration.getName(),
            argumentDeclaration.getType()
        );
    }

    private class AnchorTypecheckResult<T> {
        private final Option<AnchorTarget> anchorTargetOption;
        private final Typecheck<T> valueTypecheck;

        public AnchorTypecheckResult(
            Option<AnchorTarget> anchorTargetOption,
            Typecheck<T> valueTypecheck
        ) {
            this.anchorTargetOption = anchorTargetOption;
            this.valueTypecheck = valueTypecheck;
        }

        public Option<AnchorTarget> getAnchorTargetOption() {
            return anchorTargetOption;
        }

        public Typecheck<T> getValueTypecheck() {
            return valueTypecheck;
        }

        public <U> AnchorTypecheckResult<U> map(Function<T, U> f) {
            return new AnchorTypecheckResult<U>(
                anchorTargetOption,
                valueTypecheck.map(f)
            );
        }
    }

    private class AnchorTarget {
        private final CommonNominalType targetedType;
        private final String lastPropertyName;

        public AnchorTarget(
            CommonNominalType targetedType,
            String lastPropertyName
        ) {
            this.targetedType = targetedType;
            this.lastPropertyName = lastPropertyName;
        }

        public CommonNominalType getTargetedType() {
            return targetedType;
        }

        public String getLastPropertyName() {
            return lastPropertyName;
        }
    }

    private Typecheck<TypedTFGSelection> typecheckSelection(
        TypeDefinitionRegistry typeDefinitionRegistry,
        List<TFGQuery> queries,
        TFGQuery query,
        ObjectTypeDefinition objectTypeDefinition,
        TFGSelection selection
    ) {
        final var anchorTypecheckResult =
            typecheckAnchor(
                typeDefinitionRegistry,
                queries,
                query,
                objectTypeDefinition,
                selection.getAnchor()
            );
        final var anchorTargetOption =
            anchorTypecheckResult.getAnchorTargetOption();
        final var typedAnchorTypecheck =
            anchorTypecheckResult.getValueTypecheck();
        return Typecheck
            .right(Curry.curry2(TypedTFGSelection::new))
            .combine(
                Function::apply,
                typedAnchorTypecheck
            )
            .combine(
                Function::apply,
                Typecheck.combineTraversal(subselection ->
                        typecheckSubselection(
                            typeDefinitionRegistry,
                            queries,
                            query,
                            anchorTargetOption,
                            subselection
                        ),
                    selection.getSubselection()
                )
            )
            .considerAdditionalChecks(
                anchorTargetOption
                    .map(anchorTarget ->
                        anchorTarget.getTargetedType() instanceof CommonCompoundType
                            && selection.getSubselection().isEmpty() ?
                            List.<TypeError>of(
                                new UnrestrictedSubselectionError(
                                    query.getName(),
                                    anchorTarget.getLastPropertyName()
                                )
                            ) :
                            List.<TypeError>empty()
                    )
                    .getOrElse(List::empty)
            );
    }

    private AnchorTypecheckResult<TypedTFGAnchor> typecheckAnchor(
        TypeDefinitionRegistry typeDefinitionRegistry,
        List<TFGQuery> queries,
        TFGQuery query,
        ObjectTypeDefinition objectTypeDefinition,
        TFGAnchor anchor
    ) {
        if (anchor instanceof TFGParentDeepSelection) {
            return typecheckParentDeepSelection(
                typeDefinitionRegistry,
                queries,
                query,
                objectTypeDefinition,
                (TFGParentDeepSelection) anchor
            ).map(x -> x);
        } else if (anchor instanceof TFGChildrenDeepSelection) {
            return typecheckChildrenDeepSelection(
                typeDefinitionRegistry,
                queries,
                query,
                objectTypeDefinition,
                (TFGChildrenDeepSelection) anchor
            ).map(x -> x);
        } else if (anchor instanceof TFGPropertyAnchor) {
            return typecheckProperyAnchor(
                typeDefinitionRegistry,
                query,
                objectTypeDefinition,
                (TFGPropertyAnchor) anchor
            ).map(x -> x);
        } else {
            throw new UnhandledCaseException(anchor);
        }
    }

    private AnchorTypecheckResult<TypedTFGParentDeepSelection> typecheckParentDeepSelection(
        TypeDefinitionRegistry typeDefinitionRegistry,
        List<TFGQuery> queries,
        TFGQuery query,
        ObjectTypeDefinition objectTypeDefinition,
        TFGParentDeepSelection parentDeepSelection
    ) {
        final var name = parentDeepSelection.getProperty().getName();
        final var nestedAnchorResultTypecheck =
            selectFieldDefinition(query, objectTypeDefinition, name)
                .flatMap(fieldDefinition ->
                    selectObjectType(
                        typeDefinitionRegistry,
                        query,
                        getTypeName(fieldDefinition.getType())
                    )
                )
                .map(nextObjectTypeDefinition ->
                    typecheckAnchor(
                        typeDefinitionRegistry,
                        queries,
                        query,
                        nextObjectTypeDefinition,
                        parentDeepSelection.getChild()
                    )
                );
        final var typedLastPropertyOption =
            nestedAnchorResultTypecheck
                .map(AnchorTypecheckResult::getAnchorTargetOption)
                .unpack()
                .getOrElse(Option.none());
        final var nestedTypedAnchorTypecheck =
            nestedAnchorResultTypecheck
                .flatMap(AnchorTypecheckResult::getValueTypecheck);
        return new AnchorTypecheckResult<>(
            typedLastPropertyOption,
            Typecheck.right(
                Curry.curry2(TypedTFGParentDeepSelection::new)
            )
                .combine(
                    Function::apply,
                    typecheckProperty(
                        typeDefinitionRegistry,
                        query,
                        objectTypeDefinition,
                        parentDeepSelection.getProperty()
                    )
                        .getValueTypecheck()
                )
                .combine(
                    Function::apply,
                    nestedTypedAnchorTypecheck
                )
        );
    }

    private AnchorTypecheckResult<TypedTFGChildrenDeepSelection> typecheckChildrenDeepSelection(
        TypeDefinitionRegistry typeDefinitionRegistry,
        List<TFGQuery> queries,
        TFGQuery query,
        ObjectTypeDefinition objectTypeDefinition,
        TFGChildrenDeepSelection childrenDeepSelection
    ) {
        final var property = childrenDeepSelection.getProperty();
        return new AnchorTypecheckResult<>(
            Option.none(),
            Typecheck.right(
                Curry.curry2(TypedTFGChildrenDeepSelection::new)
            )
                .combine(
                    Function::apply,
                    typecheckProperty(
                        typeDefinitionRegistry,
                        query,
                        objectTypeDefinition,
                        property
                    )
                        .getValueTypecheck()
                )
                .combine(
                    Function::apply,
                    selectFieldDefinition(
                        query,
                        objectTypeDefinition,
                        property.getName()
                    )
                        .flatMap(fieldDefinition ->
                            selectObjectType(
                                typeDefinitionRegistry,
                                query,
                                getTypeName(fieldDefinition.getType())
                            )
                        )
                        .flatMap(nextObjectTypeDefinition ->
                            Typecheck.combineTraversal(
                                selection -> typecheckSelection(
                                    typeDefinitionRegistry,
                                    queries,
                                    query,
                                    nextObjectTypeDefinition,
                                    selection
                                ),
                                childrenDeepSelection.getChildren()
                            )
                        )
                )
        );
    }

    private AnchorTypecheckResult<TypedTFGPropertyAnchor> typecheckProperyAnchor(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        ObjectTypeDefinition objectTypeDefinition,
        TFGPropertyAnchor propertyAnchor
    ) {
        final var property = propertyAnchor.getProperty();
        return typecheckProperty(
            typeDefinitionRegistry,
            query,
            objectTypeDefinition,
            property
        )
            .map(TypedTFGPropertyAnchor::new);
    }

    private AnchorTypecheckResult<TypedTFGProperty> typecheckProperty(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        ObjectTypeDefinition objectTypeDefinition,
        TFGProperty property
    ) {
        final var name = property.getName();
        final var fieldDefinitionTypecheck =
            selectFieldDefinition(query, objectTypeDefinition, name);
        return new AnchorTypecheckResult<>(
            fieldDefinitionTypecheck
                .map(fieldDefinition ->
                    buildCommonTypeFromSchema(fieldDefinition.getType())
                )
                .map(type ->
                    Option.of(
                        new AnchorTarget(getNominalOfSchemaType(type), name)
                    )
                )
                .unpack()
                .getOrElse(Option.none()),
            fieldDefinitionTypecheck
                .flatMap(fieldDefinition ->
                    Typecheck.right(Curry.curry4(TypedTFGProperty::new))
                        .combine(
                            Function::apply,
                            Typecheck.right(name)
                        )
                        .combine(
                            Function::apply,
                            Typecheck.right(property.getAlias())
                        )
                        .combine(
                            Function::apply,
                            Typecheck.combineTraversal(
                                (argument ->
                                    typecheckPropertyArgumentApplication(
                                        typeDefinitionRegistry,
                                        query,
                                        property,
                                        fieldDefinition,
                                        argument
                                    )
                                ),
                                property.getArguments()
                            )
                        )
                        .combine(
                            Function::apply,
                            Typecheck.right(
                                buildCommonTypeFromSchema(fieldDefinition.getType())
                            )
                        )
                )
        );
    }

    private Typecheck<List<TypedTFGField>> typecheckFields(
        List<TFGQuery> queries,
        TFGQuery query,
        List<TypedTFGPathElement> path,
        boolean nullable,
        boolean list,
        boolean listItemsNullable,
        List<TypedTFGSelection> typedSelections
    ) {
        return Typecheck.combineTraversal(
            typedSelection ->
                typecheckField(
                    queries,
                    query,
                    path,
                    nullable,
                    list,
                    listItemsNullable,
                    Option.none(),
                    typedSelection
                ),
            typedSelections
        )
            .map(fieldsLists -> fieldsLists.flatMap(Function.identity()));
    }

    private Typecheck<List<TypedTFGField>> typecheckField(
        List<TFGQuery> queries,
        TFGQuery query,
        List<TypedTFGPathElement> path,
        boolean nullable,
        boolean list,
        boolean listItemsNullable,
        Option<String> nameOverride,
        TypedTFGSelection typedSelection
    ) {
        return typecheckFieldAnchor(
            queries,
            query,
            path,
            nullable,
            list,
            listItemsNullable,
            nameOverride,
            typedSelection,
            typedSelection.getAnchor()
        );
    }

    private Typecheck<List<TypedTFGField>> typecheckFieldAnchor(
        List<TFGQuery> queries,
        TFGQuery query,
        List<TypedTFGPathElement> path,
        boolean nullable,
        boolean list,
        boolean listItemsNullable,
        Option<String> nameOverride,
        TypedTFGSelection typedSelection,
        TypedTFGAnchor typedAnchor
    ) {
        final var typedProperty = getTypedAnchorProperty(typedAnchor);

        if (list && isTypeAList(typedProperty.getTargetType())) {
            return Typecheck.left(
                List.of(
                    // TODO selected list in a list
                )
            );
        }

        final var pathElement =
            new TypedTFGPathElement(
                typedProperty.getAlias().getOrElse(typedProperty::getName),
                isTypeNullable(typedProperty.getTargetType()),
                isTypeAList(typedProperty.getTargetType()),
                hasTypeNullableItems(typedProperty.getTargetType())
            );

        final var nextNullable =
            nullable || (!list && pathElement.getNullable());

        final var nextList =
            list || pathElement.getList();

        final var nextListItemsNullable =
            listItemsNullable
                || pathElement.getListItemsNullable()
                || (list && pathElement.getNullable());

        final var nextPath = path.append(pathElement);
        if (typedAnchor instanceof TypedTFGParentDeepSelection) {
            final var typedParentDeepSelection =
                (TypedTFGParentDeepSelection) typedAnchor;

            return typecheckFieldAnchor(
                queries,
                query,
                nextPath,
                nextNullable,
                nextList,
                nextListItemsNullable,
                Option.of(
                    nameOverride.getOrElse(() ->
                        typedProperty.getAlias().getOrElse(typedProperty::getName)
                    )
                ),
                typedSelection,
                typedParentDeepSelection.getChild()
            );
        } else if (typedAnchor instanceof TypedTFGChildrenDeepSelection) {
            final var typedChildrenDeepSelection =
                (TypedTFGChildrenDeepSelection) typedAnchor;

            if (nextList) {
                return Typecheck.left(
                    List.of(
                        // TODO Can't perform children deep selections
                        // inside lists
                    )
                );
            }

            return typecheckFields(
                queries,
                query,
                nextPath,
                nextNullable,
                nextList,
                nextListItemsNullable,
                typedChildrenDeepSelection.getChildren()
            );
        } else if (typedAnchor instanceof TypedTFGPropertyAnchor) {
            final var nominalType =
                typedSelection
                    .getSubselection()
                    .<CommonNominalType>map(typedSubselection ->
                        new CommonCompoundType(
                            upperCaseName(typedSubselection.getName())
                        )
                    )
                    .getOrElse(() ->
                        getNominalOfSchemaType(typedProperty.getTargetType())
                    );
            return Typecheck.right(
                List.of(
                    new TypedTFGField(
                        nameOverride.getOrElse(
                            typedProperty
                                .getAlias()
                                .getOrElse(typedProperty::getName)
                        ),
                        nextList ?
                            new CommonListType(
                                new CommonSimpleType(
                                    nominalType,
                                    nextListItemsNullable
                                ),
                                nextNullable
                            ) :
                            new CommonSimpleType(
                                nominalType,
                                nextNullable
                            ),
                        nextPath
                    )
                )
            );
        } else {
            throw new UnhandledCaseException(typedAnchor);
        }
    }

    private TypedTFGProperty getTypedAnchorProperty(
        TypedTFGAnchor typedAnchor
    ) {
        /*
        typedProperty =
            case typedAnchor of
                IR.ParentDeepSelectionAnchor ParentDeepSelection{..} ->
                    typedProperty

                IR.ChildrenDeepSelectionAnchor ChildrenDeepSelection{..} ->
                    typedProperty

                IR.PropertyAnchor typedProperty ->
                    typedProperty
         */
        if (typedAnchor instanceof TypedTFGParentDeepSelection) {
            TypedTFGParentDeepSelection typedParentDeepSelection =
                (TypedTFGParentDeepSelection) typedAnchor;
            return typedParentDeepSelection.getProperty();
        } else if (typedAnchor instanceof TypedTFGChildrenDeepSelection) {
            TypedTFGChildrenDeepSelection typedChildrenDeepSelection =
                (TypedTFGChildrenDeepSelection) typedAnchor;
            return typedChildrenDeepSelection.getProperty();
        } else if (typedAnchor instanceof TypedTFGPropertyAnchor) {
            TypedTFGPropertyAnchor typedPropertyAnchor =
                (TypedTFGPropertyAnchor) typedAnchor;
            return typedPropertyAnchor.getProperty();
        } else {
            throw new UnhandledCaseException(typedAnchor);
        }
    }

    private String upperCaseName(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private Typecheck<TypedTFGArgumentApplication> typecheckPropertyArgumentApplication(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        TFGProperty property,
        FieldDefinition fieldDefinition,
        TFGArgumentApplication argumentApplication
    ) {

        return Typecheck.fromOption(
            List.ofAll(fieldDefinition.getInputValueDefinitions())
                .find(inputValueDefinition ->
                    inputValueDefinition.getName()
                        .equals(argumentApplication.getName())
                )
                .map(inputValueDefinition ->
                    buildCommonTypeFromSchema(
                        inputValueDefinition.getType()
                    )
                ),
            () -> List.of(
                new UndefinedArgumentError(
                    query.getName(),
                    property.getName(),
                    argumentApplication.getName()
                )
            )
        )
            .flatMap(declaredType ->
                typecheckExpression(
                    typeDefinitionRegistry,
                    query,
                    property.getName(),
                    argumentApplication,
                    declaredType,
                    argumentApplication.getExpression()
                )
                    .map(typedExpression ->
                        new TypedTFGArgumentApplication(
                            argumentApplication.getName(),
                            typedExpression
                        )
                    )
            );
    }

    private CommonType buildCommonTypeFromSchema(
        Type type
    ) {
        return buildCommonTypeFromSchemaNullable(true, type);
    }

    private CommonType buildCommonTypeFromSchemaNullable(
        boolean nullable,
        Type type
    ) {
        if (type instanceof TypeName) {
            final var typeName = (TypeName) type;
            return new CommonSimpleType(
                NominalTypeIdentification.identify(typeName.getName()),
                nullable
            );
        } else if (type instanceof NonNullType) {
            if (!nullable) {
                throw new RuntimeException("Duplicate nested NonNullType");
            } else {
                return buildCommonTypeFromSchemaNullable(
                    false,
                    ((NonNullType) type).getType()
                );
            }
        } else if (type instanceof ListType) {
            final var appliedType = ((ListType) type).getType();
            return new CommonListType(
                buildCommonNominalTypeFromSchemaNullable(true, appliedType),
                nullable
            );
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private CommonSimpleType buildCommonNominalTypeFromSchemaNullable(
        boolean nullable,
        Type type
    ) {
        if (type instanceof TypeName) {
            final var typeName = (TypeName) type;
            return new CommonSimpleType(
                NominalTypeIdentification.identify(typeName.getName()),
                nullable
            );
        } else if (type instanceof NonNullType) {
            if (!nullable) {
                throw new RuntimeException("Duplicate nested NonNullType");
            } else {
                return buildCommonNominalTypeFromSchemaNullable(
                    false,
                    ((NonNullType) type).getType()
                );
            }
        } else if (type instanceof ListType) {
            throw new RuntimeException("Nested lists are not supported.");
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private Typecheck<TypedTFGSubselection> typecheckSubselection(
        TypeDefinitionRegistry typeDefinitionRegistry,
        List<TFGQuery> queries,
        TFGQuery query,
        Option<AnchorTarget> anchorTargetOption,
        TFGSubselection subselection
    ) {
        return Typecheck.right(Curry.curry2(TypedTFGSubselection::new))
            .combine(
                Function::apply,
                Typecheck.right(subselection.getName())
            )
            .combine(
                Function::apply,
                Typecheck.fromOption(
                    anchorTargetOption,
                    () -> List.of(
                        // TODO subselection on children or anchor really broken
                        // TODO remove check in typecheckFields
                    )
                )
                    .flatMap(anchorTarget ->
                        Typecheck.fromOption(
                            queries
                                .find(q ->
                                    q.getName().equals(subselection.getName())
                                ),
                            () ->
                                List.of(
                                    new UndefinedQueryError(
                                        query.getName(),
                                        anchorTarget.getLastPropertyName(),
                                        subselection.getName()
                                    )
                                )
                        )
                            .flatMap(targetQuery -> {
                                final var nominalType =
                                    anchorTarget.getTargetedType();

                                List<TypeError> typeMismatchErrors;
                                if (nominalType instanceof CommonPrimitiveType) {
                                    typeMismatchErrors = List.of(
                                        // TODO subselection on primitive type
                                    );
                                } else if (nominalType instanceof CommonCompoundType) {
                                    final var compoundType =
                                        (CommonCompoundType) nominalType;

                                    typeMismatchErrors =
                                        targetQuery.getType()
                                            .equals(compoundType.getName()) ?
                                            List.empty() :
                                            List.of(
                                                new SubselectionTypeMismatchError(
                                                    query.getName(),
                                                    anchorTarget.getLastPropertyName(),
                                                    compoundType.getName(),
                                                    targetQuery.getName(),
                                                    targetQuery.getType()
                                                )
                                            );
                                } else {
                                    throw new UnhandledCaseException(nominalType);
                                }

                                return Typecheck
                                    .combineTraversal(
                                        argumentApplication ->
                                            typecheckSubselectionArgumentApplication(
                                                typeDefinitionRegistry,
                                                query,
                                                anchorTarget,
                                                targetQuery,
                                                argumentApplication
                                            ),
                                        subselection.getArgumentApplications()
                                    )
                                    .considerAdditionalChecks(typeMismatchErrors);
                            })
                    )
            );
    }

    private String getTypeName(Type type) {
        if (type instanceof TypeName) {
            return ((TypeName) type).getName();
        } else if (type instanceof ListType) {
            return getTypeName(((ListType) type).getType());
        } else if (type instanceof NonNullType) {
            return getTypeName(((NonNullType) type).getType());
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private CommonNominalType getNominalOfSchemaType(CommonType type) {
        if (type instanceof CommonNullType) {
            throw new RuntimeException("Schema types are never a null type");
        } else if (type instanceof CommonSimpleType) {
            return ((CommonSimpleType) type).getNominalType();
        } else if (type instanceof CommonListType) {
            return ((CommonListType) type).getAppliedType().getNominalType();
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private boolean isTypeNullable(CommonType type) {
        if (type instanceof CommonNullType) {
            return true;
        } else if (type instanceof CommonSimpleType) {
            final var simpleType = (CommonSimpleType) type;
            return simpleType.getNullable();
        } else if (type instanceof CommonListType) {
            final var listType = (CommonListType) type;
            return listType.getNullable();
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private boolean isTypeAList(CommonType type) {
        if (type instanceof CommonNullType) {
            return false;
        } else if (type instanceof CommonSimpleType) {
            return false;
        } else if (type instanceof CommonListType) {
            return true;
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private boolean hasTypeNullableItems(CommonType type) {
        if (type instanceof CommonNullType) {
            return false;
        } else if (type instanceof CommonSimpleType) {
            return false;
        } else if (type instanceof CommonListType) {
            final var listType = (CommonListType) type;
            return listType.getAppliedType().getNullable();
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private Typecheck<TypedTFGArgumentApplication> typecheckSubselectionArgumentApplication(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        AnchorTarget anchorTarget,
        TFGQuery targetQuery,
        TFGArgumentApplication argumentApplication
    ) {
        return Typecheck
            .fromOption(
                targetQuery
                    .getArgumentDeclarations()
                    .find(argumentDeclaration ->
                        argumentDeclaration.getName()
                            .equals(argumentApplication.getName()))
                    .map(TFGArgumentDeclaration::getType),
                () -> List.of(
                    new UndefinedArgumentError(
                        query.getName(),
                        anchorTarget.getLastPropertyName(),
                        argumentApplication.getName()
                    )
                )
            )
            .flatMap(
                declaredType ->
                    typecheckExpression(
                        typeDefinitionRegistry,
                        query,
                        anchorTarget.getLastPropertyName(),
                        argumentApplication,
                        declaredType,
                        argumentApplication.getExpression()
                    )
                        .map(typedExpression ->
                            new TypedTFGArgumentApplication(
                                argumentApplication.getName(),
                                typedExpression
                            )
                        )
            );
    }

    private Typecheck<TypedTFGExpression> typecheckExpression(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        String propertyName,
        TFGArgumentApplication argumentApplication,
        CommonType expected,
        TFGExpression provided
    ) {
        if (provided instanceof TFGVariable) {
            final var variable = (TFGVariable) provided;
            return typecheckVariable(
                query,
                propertyName,
                argumentApplication,
                expected,
                variable
            )
                .map(x -> x);
        } else if (provided instanceof TFGLiteral) {
            final var literal = (TFGLiteral) provided;
            return typecheckLiteral(
                typeDefinitionRegistry,
                query,
                propertyName,
                argumentApplication,
                expected,
                literal
            )
                .map(x -> x);
        } else {
            throw new UnhandledCaseException(provided);
        }
    }

    private Typecheck<TypedTFGVariable> typecheckVariable(
        TFGQuery query,
        String propertyName,
        TFGArgumentApplication argumentApplication,
        CommonType expected,
        TFGVariable variable
    ) {
        return Typecheck.fromOption(
            query.getArgumentDeclarations()
                .find(
                    argumentDeclaration ->
                        argumentDeclaration.getName()
                            .equals(variable.getName())
                ),
            () -> List.of(
                new UndefinedVariableError(
                    query.getName(),
                    propertyName,
                    argumentApplication.getName(),
                    variable.getName()
                )
            )
        )
            .map(TFGArgumentDeclaration::getType)
            .flatMap(variableType ->
                typecheckTypes(expected, variableType) ?
                    Typecheck.right(
                        new TypedTFGVariable(variable.getName())
                    ) :
                    Typecheck.left(
                        List.of(
                            new WrongArgumentTypeError(
                                query.getName(),
                                propertyName,
                                argumentApplication.getName(),
                                CommonTypeRenderer.render(expected),
                                variable
                            )
                        )
                    )
            );
    }

    private Typecheck<TypedTFGLiteral> typecheckLiteral(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        String propertyName,
        TFGArgumentApplication argumentApplication,
        CommonType expected,
        TFGLiteral literal
    ) {
        if (literal instanceof TFGIntLiteral) {
            final var intLiteral = (TFGIntLiteral) literal;
            return typecheckPrimitiveLiteral(
                query,
                propertyName,
                argumentApplication,
                expected,
                intLiteral,
                List.of(CommonPrimitiveTypeEnum.Int),
                () -> new TypedTFGIntLiteral(intLiteral.getRawValue())
            );
        } else if (literal instanceof TFGFloatLiteral) {
            final var floatLiteral = (TFGFloatLiteral) literal;
            return typecheckPrimitiveLiteral(
                query,
                propertyName,
                argumentApplication,
                expected,
                floatLiteral,
                List.of(CommonPrimitiveTypeEnum.Float),
                () -> new TypedTFGFloatLiteral(floatLiteral.getRawValue())
            );
        } else if (literal instanceof TFGStringLiteral) {
            final var stringLiteral = (TFGStringLiteral) literal;
            return typecheckPrimitiveLiteral(
                query,
                propertyName,
                argumentApplication,
                expected,
                stringLiteral,
                List.of(
                    CommonPrimitiveTypeEnum.String,
                    CommonPrimitiveTypeEnum.ID
                ),
                () -> new TypedTFGStringLiteral(stringLiteral.getRawValue())
            );
        } else if (literal instanceof TFGBooleanLiteral) {
            final var booleanLiteral = (TFGBooleanLiteral) literal;
            return typecheckPrimitiveLiteral(
                query,
                propertyName,
                argumentApplication,
                expected,
                booleanLiteral,
                List.of(CommonPrimitiveTypeEnum.Boolean),
                () -> new TypedTFGBooleanLiteral(booleanLiteral.getValue())
            );
        } else if (literal instanceof TFGNullLiteral) {
            return isTypeNullable(expected) ?
                Typecheck.right(new TypedTFGNullLiteral()) :
                Typecheck.left(
                    List.of(
                        new WrongArgumentTypeError(
                            query.getName(),
                            propertyName,
                            argumentApplication.getName(),
                            CommonTypeRenderer.render(expected),
                            literal
                        )
                    )
                );
        } else if (literal instanceof TFGEnumLiteral) {
            final var enumLiteral = (TFGEnumLiteral) literal;
            return typecheckEnumLiteral(
                typeDefinitionRegistry,
                query,
                propertyName,
                argumentApplication,
                expected,
                enumLiteral
            )
                .map(x -> x);
        } else if (literal instanceof TFGListLiteral) {
            final var listLiteral = (TFGListLiteral) literal;
            return typecheckListLiteral(
                typeDefinitionRegistry,
                query,
                propertyName,
                argumentApplication,
                expected,
                listLiteral
            )
                .map(x -> x);
        } else if (literal instanceof TFGRecordLiteral) {
            final var recordLiteral = (TFGRecordLiteral) literal;
            return typecheckRecordLiteral(
                typeDefinitionRegistry,
                query,
                propertyName,
                argumentApplication,
                expected,
                recordLiteral
            )
                .map(x -> x);
        } else {
            throw new UnhandledCaseException(literal);
        }
    }

    private Typecheck<TypedTFGLiteral> typecheckPrimitiveLiteral(
        TFGQuery query,
        String propertyName,
        TFGArgumentApplication argumentApplication,
        CommonType expected,
        TFGLiteral literal,
        List<CommonPrimitiveTypeEnum> allowedTypes,
        Supplier<TypedTFGLiteral> typedTFGLiteralSupplier
    ) {
        if (expected instanceof CommonSimpleType) {
            final var simpleType = (CommonSimpleType) expected;
            if (simpleType.getNominalType() instanceof CommonPrimitiveType) {
                final var primitiveType =
                    (CommonPrimitiveType) simpleType.getNominalType();
                if (allowedTypes.contains(primitiveType.getName())) {
                    return Typecheck.right(typedTFGLiteralSupplier.get());
                }
            }
        }

        return Typecheck.left(
            List.of(
                new WrongArgumentTypeError(
                    query.getName(),
                    propertyName,
                    argumentApplication.getName(),
                    CommonTypeRenderer.render(expected),
                    literal
                )
            )
        );
    }

    private Typecheck<TypedTFGEnumLiteral> typecheckEnumLiteral(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        String propertyName,
        TFGArgumentApplication argumentApplication,
        CommonType expected,
        TFGEnumLiteral enumLiteral
    ) {

        if (expected instanceof CommonSimpleType) {
            final var simpleType = (CommonSimpleType) expected;
            if (simpleType.getNominalType() instanceof CommonCompoundType) {
                final var compoundType =
                    (CommonCompoundType) simpleType.getNominalType();

                return selectTypeDefinition(
                    typeDefinitionRegistry,
                    query,
                    compoundType.getName()
                )
                    .flatMap(
                        typeDefinition -> {
                            if (typeDefinition instanceof EnumTypeDefinition) {
                                final var enumTypeDefinition =
                                    (EnumTypeDefinition) typeDefinition;

                                final var validEnumValue = List.ofAll(
                                    enumTypeDefinition.getEnumValueDefinitions()
                                )
                                    .map(EnumValueDefinition::getName)
                                    .contains(enumLiteral.getRawValue());

                                return validEnumValue ?
                                    Typecheck.right(
                                        new TypedTFGEnumLiteral(
                                            enumLiteral.getRawValue()
                                        )
                                    ) :
                                    Typecheck.left(
                                        List.of(
                                            new WrongArgumentTypeError(
                                                query.getName(),
                                                propertyName,
                                                argumentApplication.getName(),
                                                CommonTypeRenderer.render(expected),
                                                enumLiteral
                                            )
                                        )
                                    );
                            } else {
                                return Typecheck.left(
                                    List.of(
                                        new WrongArgumentTypeError(
                                            query.getName(),
                                            propertyName,
                                            argumentApplication.getName(),
                                            CommonTypeRenderer.render(expected),
                                            enumLiteral
                                        )
                                    )
                                );
                            }
                        }
                    );
            }
        }

        return Typecheck.left(
            List.of(
                new WrongArgumentTypeError(
                    query.getName(),
                    propertyName,
                    argumentApplication.getName(),
                    CommonTypeRenderer.render(expected),
                    enumLiteral
                )
            )
        );
    }

    private Typecheck<TypedTFGListLiteral> typecheckListLiteral(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        String propertyName,
        TFGArgumentApplication argumentApplication,
        CommonType expected,
        TFGListLiteral listLiteral
    ) {
        if (expected instanceof CommonListType) {
            final var listType = (CommonListType) expected;
            return Typecheck.combineTraversal(
                item ->
                    typecheckExpression(
                        typeDefinitionRegistry,
                        query,
                        propertyName,
                        argumentApplication,
                        listType.getAppliedType(),
                        item
                    ),
                listLiteral.getItems()
            ).map(TypedTFGListLiteral::new);
        } else {
            return Typecheck.left(
                List.of(
                    new WrongArgumentTypeError(
                        query.getName(),
                        propertyName,
                        argumentApplication.getName(),
                        CommonTypeRenderer.render(expected),
                        listLiteral
                    )
                )
            );
        }
    }

    private Typecheck<TypedTFGRecordLiteral> typecheckRecordLiteral(
        TypeDefinitionRegistry typeDefinitionRegistry,
        TFGQuery query,
        String propertyName,
        TFGArgumentApplication argumentApplication,
        CommonType expected,
        TFGRecordLiteral recordLiteral
    ) {

        if (expected instanceof CommonSimpleType) {
            final var simpleType = (CommonSimpleType) expected;
            if (simpleType.getNominalType() instanceof CommonCompoundType) {
                final var compoundType =
                    (CommonCompoundType) simpleType.getNominalType();

                return selectTypeDefinition(
                    typeDefinitionRegistry,
                    query,
                    compoundType.getName()
                )
                    .flatMap(
                        typeDefinition -> {
                            if (typeDefinition instanceof InputObjectTypeDefinition) {
                                final var inputObjectTypeDefinition =
                                    (InputObjectTypeDefinition) typeDefinition;

                                return Typecheck.combineTraversal(
                                    property ->
                                        Typecheck.fromOption(
                                            List.ofAll(
                                                inputObjectTypeDefinition
                                                    .getInputValueDefinitions()
                                            )
                                                .find(inputValueDefinition ->
                                                    inputValueDefinition
                                                        .getName()
                                                        .equals(property.getName())
                                                ),
                                            () -> List.of(
                                                new UndefinedRecordPropertyError(
                                                    query.getName(),
                                                    propertyName,
                                                    argumentApplication.getName(),
                                                    CommonTypeRenderer.render(expected),
                                                    property.getName()
                                                )
                                            )
                                        )
                                            .map(InputValueDefinition::getType)
                                            .flatMap(propertyType ->
                                                typecheckExpression(
                                                    typeDefinitionRegistry,
                                                    query,
                                                    propertyName,
                                                    argumentApplication,
                                                    buildCommonTypeFromSchema(
                                                        propertyType
                                                    ),
                                                    property.getValue()
                                                )
                                                    .map(typedExpression ->
                                                        new TypedTFGRecordLiteralProperty(
                                                            property.getName(),
                                                            typedExpression
                                                        )
                                                    )
                                            ),
                                    recordLiteral.getProperties()
                                )
                                    .map(TypedTFGRecordLiteral::new)
                                    .considerAdditionalChecks(
                                        List.ofAll(
                                            inputObjectTypeDefinition
                                                .getInputValueDefinitions()
                                        )
                                            .filter(
                                                inputValueDefinition ->
                                                    recordLiteral
                                                        .getProperties()
                                                        .find(
                                                            property ->
                                                                property
                                                                    .getName()
                                                                    .equals(
                                                                        inputValueDefinition
                                                                            .getName()
                                                                    )
                                                        )
                                                        .isEmpty()
                                            )
                                            .map(
                                                inputValueDefinition ->
                                                    new MissingRecordPropertyError(
                                                        query.getName(),
                                                        propertyName,
                                                        argumentApplication.getName(),
                                                        CommonTypeRenderer.render(expected),
                                                        inputValueDefinition.getName()
                                                    )
                                            )
                                    );
                            } else {
                                return Typecheck.left(
                                    List.of(
                                        new WrongArgumentTypeError(
                                            query.getName(),
                                            propertyName,
                                            argumentApplication.getName(),
                                            CommonTypeRenderer.render(expected),
                                            recordLiteral
                                        )
                                    )
                                );
                            }
                        }
                    );
            }
        }

        return Typecheck.left(
            List.of(
                new WrongArgumentTypeError(
                    query.getName(),
                    propertyName,
                    argumentApplication.getName(),
                    CommonTypeRenderer.render(expected),
                    recordLiteral
                )
            )
        );
    }

    private boolean typecheckTypes(
        CommonType expected,
        CommonType provided
    ) {
        if (expected instanceof CommonListType) {
            final var requestedList = (CommonListType) expected;
            if (provided instanceof CommonListType) {
                final var providedList = (CommonListType) provided;
                return typecheckNominalTypes(
                    requestedList.getAppliedType(),
                    providedList.getAppliedType()
                );
            } else if (provided instanceof CommonSimpleType) {
                return false;
            } else if (provided instanceof CommonNullType) {
                return requestedList.getNullable();
            } else {
                throw new UnhandledCaseException(expected);
            }
        } else if (expected instanceof CommonSimpleType) {
            final var requestedNominal = (CommonSimpleType) expected;
            if (provided instanceof CommonSimpleType) {
                final var providedNominal = (CommonSimpleType) provided;
                return typecheckNominalTypes(requestedNominal, providedNominal);
            } else if (provided instanceof CommonListType) {
                return false;
            } else if (provided instanceof CommonNullType) {
                return requestedNominal.getNullable();
            } else {
                throw new UnhandledCaseException(provided);
            }
        } else if (expected instanceof CommonNullType) {
            if (provided instanceof CommonNullType) {
                return true;
            } else if (provided instanceof CommonListType) {
                return false;
            } else if (provided instanceof CommonSimpleType) {
                return false;
            } else {
                throw new UnhandledCaseException(provided);
            }
        } else {
            throw new UnhandledCaseException(expected);
        }
    }

    private boolean typecheckNominalTypes(
        CommonSimpleType declared,
        CommonSimpleType applied
    ) {
        return declared.getNominalType().equals(applied.getNominalType()) &&
            (!applied.getNullable() || declared.getNullable());
    }
}
