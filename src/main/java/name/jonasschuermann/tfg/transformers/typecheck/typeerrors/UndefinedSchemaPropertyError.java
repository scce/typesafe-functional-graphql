package name.jonasschuermann.tfg.transformers.typecheck.typeerrors;

import java.util.Objects;

public final class UndefinedSchemaPropertyError extends TypeError {
    private String queryName;
    private String propertyName;
    private String targetSchemaType;

    public UndefinedSchemaPropertyError(
        String queryName,
        String propertyName,
        String targetSchemaType
    ) {
        this.queryName = queryName;
        this.propertyName = propertyName;
        this.targetSchemaType = targetSchemaType;
    }

    public String getQueryName() {
        return queryName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public String getTargetSchemaType() {
        return targetSchemaType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UndefinedSchemaPropertyError that = (UndefinedSchemaPropertyError) o;
        return queryName.equals(that.queryName) &&
            propertyName.equals(that.propertyName) &&
            targetSchemaType.equals(that.targetSchemaType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryName, propertyName, targetSchemaType);
    }

    @Override
    public String toString() {
        return "SelectionUndefinedSchemaProperty{" +
            "queryName='" + queryName + '\'' +
            ", propertyName='" + propertyName + '\'' +
            ", targetSchemaType='" + targetSchemaType + '\'' +
            '}';
    }
}
