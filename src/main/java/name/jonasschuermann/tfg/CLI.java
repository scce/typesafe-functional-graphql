package name.jonasschuermann.tfg;

import name.jonasschuermann.tfg.transformers.typecheck.typeerrors.TypeError;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CLI {
    public static void main(String[] args) throws IOException {
        final String schemaFilename = args[0];
        final String queryFilename = args[1];

        final var compiler = Compiler.make();

        final var result = compiler.compile(
            Files.readString(Path.of(schemaFilename)),
            Files.readString(Path.of(queryFilename))
        );

        if (result.isLeft()) {
            final var typeErrors = result.getLeft();
            for (TypeError typeError : typeErrors) {
                System.out.println(typeError);
            }
        }

        if (result.isRight()) {
            final var typeScriptFiles = result.get();
            final var outputDirectory = "generated-source-code";
            Files.createDirectory(Paths.get(outputDirectory));
            for (String filename : typeScriptFiles.keySet()) {
                Files.writeString(
                    Paths.get(outputDirectory, filename),
                    typeScriptFiles.get(filename).get()
                );
            }
        }
    }
}
