package name.jonasschuermann.tfg.languages.tfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TFGQuery {
    private final TFGQueryIntend intend;
    private final String name;
    private final List<TFGArgumentDeclaration> argumentDeclarations;
    private final String type;
    private final List<TFGSelection> selections;

    public TFGQuery(
        TFGQueryIntend intend,
        String name,
        List<TFGArgumentDeclaration> argumentDeclarations,
        String type,
        List<TFGSelection> selections
    ) {
        this.intend = intend;
        this.name = name;
        this.argumentDeclarations = argumentDeclarations;
        this.type = type;
        this.selections = selections;
    }

    public TFGQueryIntend getIntend() {
        return intend;
    }

    public String getName() {
        return name;
    }

    public List<TFGArgumentDeclaration> getArgumentDeclarations() {
        return argumentDeclarations;
    }

    public String getType() {
        return type;
    }

    public List<TFGSelection> getSelections() {
        return selections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGQuery tfgQuery = (TFGQuery) o;
        return intend == tfgQuery.intend && name.equals(tfgQuery.name) && argumentDeclarations.equals(tfgQuery.argumentDeclarations) && type.equals(tfgQuery.type) && selections.equals(tfgQuery.selections);
    }

    @Override
    public int hashCode() {
        return Objects.hash(intend, name, argumentDeclarations, type, selections);
    }

    @Override
    public String toString() {
        return "TFGQuery{" +
            "intend=" + intend +
            ", name='" + name + '\'' +
            ", argumentDeclarations=" + argumentDeclarations +
            ", type='" + type + '\'' +
            ", selections=" + selections +
            '}';
    }
}
