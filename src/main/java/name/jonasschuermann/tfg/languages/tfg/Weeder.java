package name.jonasschuermann.tfg.languages.tfg;

import io.vavr.collection.List;
import io.vavr.control.Option;
import name.jonasschuermann.tfg.languages.common.NominalTypeIdentification;
import name.jonasschuermann.tfg.languages.common.ast.CommonListType;
import name.jonasschuermann.tfg.languages.common.ast.CommonSimpleType;
import name.jonasschuermann.tfg.languages.common.ast.CommonType;
import name.jonasschuermann.tfg.languages.tfg.ast.*;
import name.jonasschuermann.tfg.languages.tfg.parser.TFGLexer;
import name.jonasschuermann.tfg.languages.tfg.parser.TFGParser;
import org.antlr.v4.runtime.*;

public final class Weeder {

    public TFGFile process(String input) {
        CharStream stringStream = CharStreams.fromString(input);
        TFGLexer lexer = new TFGLexer(stringStream);
        TokenStream tokens = new CommonTokenStream(lexer);
        TFGParser parser = new TFGParser(tokens);
        parser.setErrorHandler(new BailErrorStrategy());
        return buildFile(parser.file());
    }

    private TFGFile buildFile(
        TFGParser.FileContext context
    ) {
        return new TFGFile(
            List.ofAll(context.queries)
                .map(this::buildQuery)
        );
    }

    private TFGQuery buildQuery(
        TFGParser.QueryContext context
    ) {
        return new TFGQuery(
            buildQueryIntend(context.intend.getText()),
            context.name.getText(),
            List.ofAll(context.arguments)
                .map(this::buildArgumentDeclaration),
            context.type.getText(),
            List.ofAll(context.selections)
                .map(this::buildSelection)
        );
    }

    private TFGQueryIntend buildQueryIntend(String input) {
        switch (input) {
            case "query":
                return TFGQueryIntend.Query;
            case "mutation":
                return TFGQueryIntend.Mutation;
            default:
                throw new RuntimeException(
                    "Unhandled case for query intend: " + input
                );
        }
    }

    private TFGArgumentDeclaration buildArgumentDeclaration(
        TFGParser.ArgumentDeclarationContext context
    ) {
        return new TFGArgumentDeclaration(
            context.name.getText().substring(1),
            buildTypeDeclaration(context.type)
        );
    }

    private CommonType buildTypeDeclaration(
        TFGParser.TypeDeclarationContext context
    ) {
        if (context.nominalTypeDeclaration() != null) {
            return new CommonSimpleType(
                NominalTypeIdentification.identify(
                    context.nominalTypeDeclaration().name.getText()
                ),
                context.nominalTypeDeclaration().nonNullable == null
            );
        } else if (context.listTypeDeclaration() != null) {
            final var appliedType = context.listTypeDeclaration().appliedType;
            return new CommonListType(
                new CommonSimpleType(
                    NominalTypeIdentification.identify(
                        appliedType.name.getText()
                    ),
                    appliedType.nonNullable == null
                ),
                context.listTypeDeclaration().nonNullable == null
            );
        } else {
            throw new RuntimeException("Unhandled case for type declaration.");
        }
    }

    private TFGSelection buildSelection(
        TFGParser.SelectionContext context
    ) {
        return new TFGSelection(
            buildAnchor(context.anchor()),
            Option.of(context.subselection()).map(this::buildSubselection)
        );
    }

    private TFGAnchor buildAnchor(
        TFGParser.AnchorContext context
    ) {
        if (context.propertyAnchor() != null) {
            return buildPropertyAnchor(context.propertyAnchor());
        } else if (context.parentDeepSelection() != null) {
            return buildParentDeepSelection(context.parentDeepSelection());
        } else if (context.childrenDeepSelection() != null) {
            return buildChildrenDeepSelection(context.childrenDeepSelection());
        } else {
            throw new RuntimeException("Unhandled case for anchor.");
        }
    }

    private TFGPropertyAnchor buildPropertyAnchor(
        TFGParser.PropertyAnchorContext context
    ) {
        return new TFGPropertyAnchor(
            buildProperty(context.property())
        );
    }

    private TFGParentDeepSelection buildParentDeepSelection(
        TFGParser.ParentDeepSelectionContext context
    ) {
        return new TFGParentDeepSelection(
            buildProperty(context.property()),
            buildAnchor(context.child)
        );
    }

    private TFGChildrenDeepSelection buildChildrenDeepSelection(
        TFGParser.ChildrenDeepSelectionContext context
    ) {
        return new TFGChildrenDeepSelection(
            buildProperty(context.property()),
            List.ofAll(context.children).map(this::buildSelection)
        );
    }

    private TFGProperty buildProperty(
        TFGParser.PropertyContext context
    ) {
        return new TFGProperty(
            context.name.getText(),
            Option.of(context.alias).map(Token::getText),
            List.ofAll(context.arguments).map(this::buildArgumentApplication)
        );
    }

    private TFGArgumentApplication buildArgumentApplication(
        TFGParser.ArgumentApplicationContext context
    ) {
        return new TFGArgumentApplication(
            context.name.getText(),
            buildExpression(context.expression())
        );
    }

    private TFGExpression buildExpression(
        TFGParser.ExpressionContext context
    ) {
        if (context.variable() != null) {
            return buildVariable(context.variable());
        } else if (context.literal() != null) {
            return buildLiteral(context.literal());
        } else {
            throw new RuntimeException("Unhandled case for expression.");
        }
    }

    private TFGVariable buildVariable(
        TFGParser.VariableContext context
    ) {
        return new TFGVariable(
            context.name.getText().substring(1)
        );
    }

    private TFGLiteral buildLiteral(
        TFGParser.LiteralContext context
    ) {
        if (context.intLiteral() != null) {
            return buildIntLiteral(context.intLiteral());
        } else if (context.floatLiteral() != null) {
            return buildFloatLiteral(context.floatLiteral());
        } else if (context.stringLiteral() != null) {
            return buildStringLiteral(context.stringLiteral());
        } else if (context.booleanLiteral() != null) {
            return buildBooleanLiteral(context.booleanLiteral());
        } else if (context.nullLiteral() != null) {
            return new TFGNullLiteral();
        } else if (context.enumLiteral() != null) {
            return buildEnumLiteral(context.enumLiteral());
        } else if (context.listLiteral() != null) {
            return buildListLiteral(context.listLiteral());
        } else if (context.recordLiteral() != null) {
            return buildRecordLiteral(context.recordLiteral());
        } else {
            throw new RuntimeException("Unhandled case for literal.");
        }
    }

    private TFGIntLiteral buildIntLiteral(
        TFGParser.IntLiteralContext context
    ) {
        return new TFGIntLiteral(context.value.getText());
    }

    private TFGFloatLiteral buildFloatLiteral(
        TFGParser.FloatLiteralContext context
    ) {
        return new TFGFloatLiteral(context.value.getText());
    }

    private TFGStringLiteral buildStringLiteral(
        TFGParser.StringLiteralContext context
    ) {
        return new TFGStringLiteral(context.value.getText());
    }

    private TFGBooleanLiteral buildBooleanLiteral(
        TFGParser.BooleanLiteralContext context
    ) {
        final var text = context.value.getText();
        if (text.equals("false")) {
            return new TFGBooleanLiteral(false);
        } else if (text.equals("true")) {
            return new TFGBooleanLiteral(true);
        } else {
            throw new RuntimeException(
                "Unhandled case for boolean literal value: " + text
            );
        }
    }

    private TFGEnumLiteral buildEnumLiteral(
        TFGParser.EnumLiteralContext context
    ) {
        return new TFGEnumLiteral(context.value.getText());
    }

    private TFGListLiteral buildListLiteral(
        TFGParser.ListLiteralContext context
    ) {
        return new TFGListLiteral(
            List.ofAll(context.items).map(this::buildExpression)
        );
    }

    private TFGRecordLiteral buildRecordLiteral(
        TFGParser.RecordLiteralContext context
    ) {
        return new TFGRecordLiteral(
            List.ofAll(context.properties).map(this::buildRecordLiteralProperty)
        );
    }

    private TFGRecordLiteralProperty buildRecordLiteralProperty(
        TFGParser.RecordLiteralPropertyContext context
    ) {
        return new TFGRecordLiteralProperty(
            context.name.getText(),
            buildExpression(context.value)
        );
    }

    private TFGSubselection buildSubselection(
        TFGParser.SubselectionContext context
    ) {
        return new TFGSubselection(
            context.name.getText(),
            List.ofAll(context.arguments)
                .map(this::buildArgumentApplication)
        );
    }
}
