package name.jonasschuermann.tfg.languages.tfg.ast;

import java.util.Objects;

public final class TFGNullLiteral extends TFGLiteral {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return o != null && getClass() == o.getClass();
    }

    @Override
    public int hashCode() {
        return Objects.hash();
    }

    @Override
    public String toString() {
        return "TFGNullLiteral{}";
    }
}
