package name.jonasschuermann.tfg.languages.tfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TFGListLiteral extends TFGLiteral {
    private final List<TFGExpression> items;

    public TFGListLiteral(List<TFGExpression> items) {
        this.items = items;
    }

    public List<TFGExpression> getItems() {
        return items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGListLiteral that = (TFGListLiteral) o;
        return items.equals(that.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(items);
    }

    @Override
    public String toString() {
        return "TFGListLiteral{" +
            "items=" + items +
            '}';
    }
}
