package name.jonasschuermann.tfg.languages.tfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TFGSubselection {
    private final String name;
    private final List<TFGArgumentApplication> argumentApplications;

    public TFGSubselection(String name, List<TFGArgumentApplication> argumentApplications) {
        this.name = name;
        this.argumentApplications = argumentApplications;
    }

    public String getName() {
        return name;
    }

    public List<TFGArgumentApplication> getArgumentApplications() {
        return argumentApplications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGSubselection that = (TFGSubselection) o;
        return name.equals(that.name) &&
            argumentApplications.equals(that.argumentApplications);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, argumentApplications);
    }

    @Override
    public String toString() {
        return "TFGSubselection{" +
            "name='" + name + '\'' +
            ", argumentApplications=" + argumentApplications +
            '}';
    }
}
