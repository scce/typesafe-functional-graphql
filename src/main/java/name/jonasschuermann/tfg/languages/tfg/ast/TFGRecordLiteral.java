package name.jonasschuermann.tfg.languages.tfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TFGRecordLiteral extends TFGLiteral {
    private final List<TFGRecordLiteralProperty> properties;

    public TFGRecordLiteral(List<TFGRecordLiteralProperty> properties) {
        this.properties = properties;
    }

    public List<TFGRecordLiteralProperty> getProperties() {
        return properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGRecordLiteral that = (TFGRecordLiteral) o;
        return properties.equals(that.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(properties);
    }

    @Override
    public String toString() {
        return "TFGRecordLiteral{" +
            "properties=" + properties +
            '}';
    }
}
