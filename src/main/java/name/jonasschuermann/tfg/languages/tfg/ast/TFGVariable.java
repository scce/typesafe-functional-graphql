package name.jonasschuermann.tfg.languages.tfg.ast;

import java.util.Objects;

public final class TFGVariable extends TFGExpression {
    private final String name;

    public TFGVariable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGVariable selectionVariable = (TFGVariable) o;
        return name.equals(selectionVariable.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "TFGVariable{" +
            "name='" + name + '\'' +
            '}';
    }
}
