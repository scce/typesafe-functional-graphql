package name.jonasschuermann.tfg.languages.tfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public class TFGChildrenDeepSelection extends TFGAnchor {
    private final TFGProperty property;
    private final List<TFGSelection> children;

    public TFGChildrenDeepSelection(
        TFGProperty property,
        List<TFGSelection> children
    ) {
        this.property = property;
        this.children = children;
    }

    public TFGProperty getProperty() {
        return property;
    }

    public List<TFGSelection> getChildren() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGChildrenDeepSelection that = (TFGChildrenDeepSelection) o;
        return property.equals(that.property) &&
            children.equals(that.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(property, children);
    }

    @Override
    public String toString() {
        return "TFGChildrenDeepSelection{" +
            "property=" + property +
            ", children=" + children +
            '}';
    }
}

