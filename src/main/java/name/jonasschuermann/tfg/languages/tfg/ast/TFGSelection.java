package name.jonasschuermann.tfg.languages.tfg.ast;

import io.vavr.control.Option;

import java.util.Objects;

public class TFGSelection {
    private final TFGAnchor anchor;
    private final Option<TFGSubselection> subselection;

    public TFGSelection(
        TFGAnchor anchor,
        Option<TFGSubselection> subselection
    ) {
        this.anchor = anchor;
        this.subselection = subselection;
    }

    public TFGAnchor getAnchor() {
        return anchor;
    }

    public Option<TFGSubselection> getSubselection() {
        return subselection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGSelection that = (TFGSelection) o;
        return anchor.equals(that.anchor) &&
            subselection.equals(that.subselection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(anchor, subselection);
    }

    @Override
    public String toString() {
        return "TFGSelection{" +
            "anchor=" + anchor +
            ", subselection=" + subselection +
            '}';
    }
}
