package name.jonasschuermann.tfg.languages.tfg.ast;

import java.util.Objects;

public class TFGPropertyAnchor extends TFGAnchor {
    private final TFGProperty property;

    public TFGPropertyAnchor(TFGProperty property) {
        this.property = property;
    }

    public TFGProperty getProperty() {
        return property;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGPropertyAnchor that = (TFGPropertyAnchor) o;
        return property.equals(that.property);
    }

    @Override
    public int hashCode() {
        return Objects.hash(property);
    }

    @Override
    public String toString() {
        return "TFGPropertyAnchor{" +
            "property=" + property +
            '}';
    }
}
