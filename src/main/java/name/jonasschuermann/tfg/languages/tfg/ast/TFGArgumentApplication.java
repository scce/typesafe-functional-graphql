package name.jonasschuermann.tfg.languages.tfg.ast;

import java.util.Objects;

public final class TFGArgumentApplication {
    private final String name;
    private final TFGExpression expression;

    public TFGArgumentApplication(String name, TFGExpression expression) {
        this.name = name;
        this.expression = expression;
    }

    public String getName() {
        return name;
    }

    public TFGExpression getExpression() {
        return expression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGArgumentApplication that = (TFGArgumentApplication) o;
        return name.equals(that.name) &&
            expression.equals(that.expression);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, expression);
    }

    @Override
    public String toString() {
        return "TFGArgumentApplication{" +
            "name='" + name + '\'' +
            ", expression=" + expression +
            '}';
    }
}
