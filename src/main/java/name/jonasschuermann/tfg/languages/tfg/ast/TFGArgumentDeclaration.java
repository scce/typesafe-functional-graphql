package name.jonasschuermann.tfg.languages.tfg.ast;

import name.jonasschuermann.tfg.languages.common.ast.CommonType;

import java.util.Objects;

public final class TFGArgumentDeclaration {
    private final String name;
    private final CommonType type;

    public TFGArgumentDeclaration(
        String name,
        CommonType type
    ) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public CommonType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGArgumentDeclaration that = (TFGArgumentDeclaration) o;
        return name.equals(that.name) &&
            type.equals(that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type);
    }

    @Override
    public String toString() {
        return "TFGArgumentDeclaration{" +
            "name='" + name + '\'' +
            ", type=" + type +
            '}';
    }
}
