package name.jonasschuermann.tfg.languages.tfg.ast;

import java.util.Objects;

public class TFGParentDeepSelection extends TFGAnchor {
    private final TFGProperty property;
    private final TFGAnchor child;

    public TFGParentDeepSelection(
        TFGProperty property,
        TFGAnchor child
    ) {
        this.property = property;
        this.child = child;
    }

    public TFGProperty getProperty() {
        return property;
    }

    public TFGAnchor getChild() {
        return child;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGParentDeepSelection that = (TFGParentDeepSelection) o;
        return property.equals(that.property) &&
            child.equals(that.child);
    }

    @Override
    public int hashCode() {
        return Objects.hash(property, child);
    }

    @Override
    public String toString() {
        return "TFGParentDeepSelection{" +
            "property=" + property +
            ", child=" + child +
            '}';
    }
}
