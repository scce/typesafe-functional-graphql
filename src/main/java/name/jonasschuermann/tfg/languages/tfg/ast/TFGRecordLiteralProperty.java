package name.jonasschuermann.tfg.languages.tfg.ast;

import java.util.Objects;

public final class TFGRecordLiteralProperty {
    private final String name;
    private final TFGExpression value;

    public TFGRecordLiteralProperty(String name, TFGExpression value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public TFGExpression getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGRecordLiteralProperty that = (TFGRecordLiteralProperty) o;
        return name.equals(that.name) && value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }

    @Override
    public String toString() {
        return "TFGRecordLiteralProperty{" +
            "name='" + name + '\'' +
            ", value=" + value +
            '}';
    }
}
