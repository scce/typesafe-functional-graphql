package name.jonasschuermann.tfg.languages.tfg.ast;

import java.util.Objects;

public final class TFGStringLiteral extends TFGLiteral {
    private final String rawValue;

    public TFGStringLiteral(String rawValue) {
        this.rawValue = rawValue;
    }

    public String getRawValue() {
        return rawValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGStringLiteral that = (TFGStringLiteral) o;
        return rawValue.equals(that.rawValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rawValue);
    }

    @Override
    public String toString() {
        return "TFGStringLiteral{" +
            "rawValue=" + rawValue +
            '}';
    }
}
