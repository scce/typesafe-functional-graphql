package name.jonasschuermann.tfg.languages.tfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TFGFile {
    private final List<TFGQuery> queries;

    public TFGFile(List<TFGQuery> queries) {
        this.queries = queries;
    }

    public List<TFGQuery> getQueries() {
        return queries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGFile that = (TFGFile) o;
        return queries.equals(that.queries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queries);
    }

    @Override
    public String toString() {
        return "TFGFile{" +
            "queries=" + queries +
            '}';
    }
}
