package name.jonasschuermann.tfg.languages.tfg.ast;

import io.vavr.collection.List;
import io.vavr.control.Option;

import java.util.Objects;

public final class TFGProperty {
    private final String name;
    private final Option<String> alias;
    private final List<TFGArgumentApplication> arguments;

    public TFGProperty(
        String name,
        Option<String> alias,
        List<TFGArgumentApplication> arguments
    ) {
        this.name = name;
        this.alias = alias;
        this.arguments = arguments;
    }

    public String getName() {
        return name;
    }

    public Option<String> getAlias() {
        return alias;
    }

    public List<TFGArgumentApplication> getArguments() {
        return arguments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGProperty that = (TFGProperty) o;
        return name.equals(that.name) &&
            alias.equals(that.alias) &&
            arguments.equals(that.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, alias, arguments);
    }

    @Override
    public String toString() {
        return "TFGProperty{" +
            "name='" + name + '\'' +
            ", alias=" + alias +
            ", arguments=" + arguments +
            '}';
    }
}
