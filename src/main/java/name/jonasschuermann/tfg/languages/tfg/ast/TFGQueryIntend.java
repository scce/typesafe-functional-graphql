package name.jonasschuermann.tfg.languages.tfg.ast;

public enum TFGQueryIntend {
    Query, Mutation
}
