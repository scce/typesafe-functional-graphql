package name.jonasschuermann.tfg.languages.tfg.ast;

import java.util.Objects;

public final class TFGBooleanLiteral extends TFGLiteral {
    private final boolean value;

    public TFGBooleanLiteral(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TFGBooleanLiteral that = (TFGBooleanLiteral) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "TFGBooleanLiteral{" +
            "value=" + value +
            '}';
    }
}
