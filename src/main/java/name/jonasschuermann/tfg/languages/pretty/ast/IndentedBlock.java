package name.jonasschuermann.tfg.languages.pretty.ast;

import io.vavr.collection.List;
import name.jonasschuermann.tfg.SequenceMonoid;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;

public final class IndentedBlock extends Part {
    private SequenceMonoid<Part> elements;

    public static IndentedBlock of(Part... parts) {
        return new IndentedBlock(SequenceMonoid.ofAll(Arrays.asList(parts)));
    }

    public static <T> IndentedBlock over(
        Function<T, SequenceMonoid<Part>> f,
        List<T> elements
    ) {
        return new IndentedBlock(SequenceMonoid.sum(elements.map(f)));
    }

    public IndentedBlock(SequenceMonoid<Part> elements) {
        this.elements = elements;
    }

    public SequenceMonoid<Part> getElements() {
        return elements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndentedBlock indentedBlock = (IndentedBlock) o;
        return elements.equals(indentedBlock.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(elements);
    }

    @Override
    public String toString() {
        return "IndentedBlock{" +
            "elements=" + elements +
            '}';
    }
}
