package name.jonasschuermann.tfg.languages.pretty.ast;

import name.jonasschuermann.tfg.SequenceMonoid;

import java.util.Arrays;
import java.util.Objects;

public final class Line extends Part {
    private SequenceMonoid<String> words;

    public static Line of(String... words) {
        return new Line(SequenceMonoid.ofAll(Arrays.asList(words)));
    }

    public Line(SequenceMonoid<String> words) {
        this.words = words;
    }

    public SequenceMonoid<String> getWords() {
        return words;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Line line = (Line) o;
        return words.equals(line.words);
    }

    @Override
    public int hashCode() {
        return Objects.hash(words);
    }

    @Override
    public String toString() {
        return "Line{" +
            "words=" + words +
            '}';
    }
}
