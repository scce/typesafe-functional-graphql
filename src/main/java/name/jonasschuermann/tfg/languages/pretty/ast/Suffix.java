package name.jonasschuermann.tfg.languages.pretty.ast;

import name.jonasschuermann.tfg.SequenceMonoid;

import java.util.Objects;

public final class Suffix extends Part {
    private SequenceMonoid<String> content;
    private SequenceMonoid<Part> inner;

    public Suffix(SequenceMonoid<String> content, SequenceMonoid<Part> inner) {
        this.content = content;
        this.inner = inner;
    }

    public SequenceMonoid<String> getContent() {
        return content;
    }

    public SequenceMonoid<Part> getInner() {
        return inner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Suffix suffix = (Suffix) o;
        return content.equals(suffix.content) &&
            inner.equals(suffix.inner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content, inner);
    }

    @Override
    public String toString() {
        return "Suffix{" +
            "content=" + content +
            ", inner=" + inner +
            '}';
    }
}
