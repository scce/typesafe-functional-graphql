package name.jonasschuermann.tfg.languages.pretty;

import io.vavr.collection.List;
import name.jonasschuermann.tfg.SequenceMonoid;
import name.jonasschuermann.tfg.UnhandledCaseException;
import name.jonasschuermann.tfg.languages.pretty.ast.*;

import java.util.function.Function;

public final class PrettyPrinter {
    public String prettyPrint(SequenceMonoid<Part> parts) {
        return renderIndentedLines(
            renderDocuments(0, List.empty(), List.empty(), parts.toList())
        );
    }

    private List<IndentedLine> renderDocument(
        int indentation,
        List<String> prefixes,
        List<String> suffixes,
        Part part
    ) {
        if (part instanceof Line) {
            final var line = (Line) part;
            return List.of(
                new IndentedLine(
                    indentation,
                    String.join(
                        "",
                        prefixes
                            .appendAll(line.getWords().toList())
                            .appendAll(suffixes)
                    )
                )
            );
        } else if (part instanceof IndentedBlock) {
            final var indentedBlock = (IndentedBlock) part;
            return renderDocuments(
                indentation + 1,
                prefixes,
                suffixes,
                indentedBlock.getElements().toList()
            );
        } else if (part instanceof Prefix) {
            final var prefix = (Prefix) part;
            return renderDocuments(
                indentation,
                prefixes.appendAll(prefix.getContent().toList()),
                suffixes,
                prefix.getInner().toList()
            );
        } else if (part instanceof Suffix) {
            final var suffix = (Suffix) part;
            return renderDocuments(
                indentation,
                prefixes,
                suffixes.prependAll(suffix.getContent().toList()),
                suffix.getInner().toList()
            );
        } else {
            throw new UnhandledCaseException(part);
        }
    }

    private List<IndentedLine> renderDocuments(
        int indentation,
        List<String> prefixes,
        List<String> suffixes,
        List<Part> documents
    ) {
        return documents
            .zipWithIndex(
                (element, index) ->
                    renderDocument(
                        indentation,
                        index == 0 ? prefixes : List.empty(),
                        index == documents.length() - 1 ?
                            suffixes :
                            List.empty(),
                        element
                    )
            )
            .flatMap(Function.identity());
    }

    private String renderIndentedLines(List<IndentedLine> lines) {
        return String.join("",
            lines.map(
                line -> String.join("",
                    List.of(
                        "    ".repeat(line.getIndentation()),
                        line.getContent(),
                        "\n"
                    )
                )
            )
        );
    }
}
