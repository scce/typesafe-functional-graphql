package name.jonasschuermann.tfg.languages.pretty;

import java.util.Objects;

public final class IndentedLine {
    private int indentation;
    private String content;

    public IndentedLine(int indentation, String content) {
        this.indentation = indentation;
        this.content = content;
    }

    public int getIndentation() {
        return indentation;
    }

    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndentedLine that = (IndentedLine) o;
        return indentation == that.indentation &&
            content.equals(that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(indentation, content);
    }

    @Override
    public String toString() {
        return "IndentedLine{" +
            "indentation=" + indentation +
            ", content='" + content + '\'' +
            '}';
    }
}
