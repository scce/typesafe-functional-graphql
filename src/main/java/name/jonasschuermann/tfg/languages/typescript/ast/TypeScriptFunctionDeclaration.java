package name.jonasschuermann.tfg.languages.typescript.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypeScriptFunctionDeclaration extends TypeScriptTopLevelStatement {
    private String name;
    private List<TypeScriptArgumentDeclaration> argumentDeclarations;
    private TypeScriptType returnType;
    private List<TypeScriptStatement> statements;

    public TypeScriptFunctionDeclaration(
        String name,
        List<TypeScriptArgumentDeclaration> argumentDeclarations,
        TypeScriptType returnType,
        List<TypeScriptStatement> statements
    ) {
        this.name = name;
        this.argumentDeclarations = argumentDeclarations;
        this.returnType = returnType;
        this.statements = statements;
    }

    public String getName() {
        return name;
    }

    public List<TypeScriptArgumentDeclaration> getArgumentDeclarations() {
        return argumentDeclarations;
    }

    public TypeScriptType getReturnType() {
        return returnType;
    }

    public List<TypeScriptStatement> getStatements() {
        return statements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptFunctionDeclaration that = (TypeScriptFunctionDeclaration) o;
        return name.equals(that.name) &&
            argumentDeclarations.equals(that.argumentDeclarations) &&
            returnType.equals(that.returnType) &&
            statements.equals(that.statements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, argumentDeclarations, returnType, statements);
    }

    @Override
    public String toString() {
        return "TypeScriptFunctionDeclaration{" +
            "name='" + name + '\'' +
            ", argumentDeclarations=" + argumentDeclarations +
            ", returnType=" + returnType +
            ", statements=" + statements +
            '}';
    }
}
