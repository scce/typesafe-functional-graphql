package name.jonasschuermann.tfg.languages.typescript.ast;

import java.util.Objects;

public final class TypeScriptObjectLiteralProperty extends TypeScriptObjectLiteralPart {
    private String name;
    private TypeScriptExpression value;

    public TypeScriptObjectLiteralProperty(
        String name,
        TypeScriptExpression value
    ) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public TypeScriptExpression getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptObjectLiteralProperty that = (TypeScriptObjectLiteralProperty) o;
        return name.equals(that.name) &&
            value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }

    @Override
    public String toString() {
        return "TypeScriptObjectLiteralProperty{" +
            "name='" + name + '\'' +
            ", value=" + value +
            '}';
    }
}
