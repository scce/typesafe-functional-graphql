package name.jonasschuermann.tfg.languages.typescript.ast;

import java.util.Objects;

public final class TypeScriptReturnStatement extends TypeScriptStatement {
    private TypeScriptExpression value;

    public TypeScriptReturnStatement(TypeScriptExpression value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptReturnStatement that = (TypeScriptReturnStatement) o;
        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public TypeScriptExpression getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "TypeScriptReturnStatement{" +
            "value=" + value +
            '}';
    }
}
