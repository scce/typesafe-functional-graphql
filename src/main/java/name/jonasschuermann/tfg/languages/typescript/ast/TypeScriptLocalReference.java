package name.jonasschuermann.tfg.languages.typescript.ast;

import java.util.Objects;

public final class TypeScriptLocalReference extends TypeScriptReference {
    private String name;

    public TypeScriptLocalReference(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptLocalReference that = (TypeScriptLocalReference) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "TypeScriptLocalReference{" +
            "name='" + name + '\'' +
            '}';
    }

    @Override
    public String getLocalName() {
        return name;
    }
}
