package name.jonasschuermann.tfg.languages.typescript.ast;

abstract public class TypeScriptReference implements Comparable<TypeScriptReference> {
    abstract public String getLocalName();

    @Override
    public int compareTo(TypeScriptReference that) {
        if (
            this instanceof TypeScriptLocalReference &&
                that instanceof TypeScriptLocalReference
        ) {
            return ((TypeScriptLocalReference) this)
                .getName()
                .compareTo(
                    ((TypeScriptLocalReference) that).getName()
                );
        } else if (
            this instanceof TypeScriptImportedReference &&
                that instanceof TypeScriptImportedReference
        ) {
            var moduleCompared =
                ((TypeScriptImportedReference) this)
                    .getModule()
                    .compareTo(
                        ((TypeScriptImportedReference) that).getModule()
                    );
            if (moduleCompared != 0) {
                return moduleCompared;
            }
            return ((TypeScriptImportedReference) this)
                .getName()
                .compareTo(
                    ((TypeScriptImportedReference) that).getName()
                );
        } else if (this instanceof TypeScriptLocalReference) {
            return -1;
        } else {
            return 1;
        }
    }
}
