package name.jonasschuermann.tfg.languages.typescript.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypeScriptUnionType extends TypeScriptType {
    private final List<TypeScriptType> types;

    public TypeScriptUnionType(List<TypeScriptType> types) {
        this.types = types;
    }

    public List<TypeScriptType> getTypes() {
        return types;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptUnionType that = (TypeScriptUnionType) o;
        return types.equals(that.types);
    }

    @Override
    public int hashCode() {
        return Objects.hash(types);
    }

    @Override
    public String toString() {
        return "TypeScriptUnionType{" +
            "types=" + types +
            '}';
    }
}
