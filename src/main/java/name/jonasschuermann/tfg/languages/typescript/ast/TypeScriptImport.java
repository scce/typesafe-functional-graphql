package name.jonasschuermann.tfg.languages.typescript.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypeScriptImport implements Comparable<TypeScriptImport> {
    private String from;
    private List<String> definitions;

    public TypeScriptImport(String from, List<String> definitions) {
        this.from = from;
        this.definitions = definitions;
    }

    public String getFrom() {
        return from;
    }

    public List<String> getDefinitions() {
        return definitions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptImport that = (TypeScriptImport) o;
        return from.equals(that.from) &&
            definitions.equals(that.definitions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, definitions);
    }

    @Override
    public String toString() {
        return "TypeScriptImport{" +
            "from='" + from + '\'' +
            ", definitions=" + definitions +
            '}';
    }

    @Override
    public int compareTo(TypeScriptImport that) {
        var fromTypeComparison =
            Boolean.compare(
                this.from.charAt(0) == '.',
                that.from.charAt(0) == '.'
            );
        if (fromTypeComparison != 0) {
            return fromTypeComparison;
        }
        var fromComparison = this.from.compareTo(that.from);
        if (fromComparison != 0) {
            return fromComparison;
        }
        final var maxLength =
            Math.max(this.definitions.length(), that.definitions.length());
        for (int i = 0; i < maxLength - 1; i++) {
            if (i == this.definitions.length()) {
                return -1;
            }
            if (i == that.definitions.length()) {
                return 1;
            }
            final var elementComparison =
                this.definitions.get(i).compareTo(that.definitions.get(i));
            if (elementComparison != 0) {
                return elementComparison;
            }
        }
        return 0;
    }
}

