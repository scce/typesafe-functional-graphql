package name.jonasschuermann.tfg.languages.typescript.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypeScriptTypeApplication extends TypeScriptType {
    private TypeScriptReference constructor;
    private List<TypeScriptType> arguments;

    public TypeScriptTypeApplication(
        TypeScriptReference constructor,
        List<TypeScriptType> arguments
    ) {
        this.constructor = constructor;
        this.arguments = arguments;
    }

    public TypeScriptReference getConstructor() {
        return constructor;
    }

    public List<TypeScriptType> getArguments() {
        return arguments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptTypeApplication that = (TypeScriptTypeApplication) o;
        return constructor.equals(that.constructor) &&
            arguments.equals(that.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(constructor, arguments);
    }

    @Override
    public String toString() {
        return "TypeScriptTypeApplication{" +
            "constructor=" + constructor +
            ", arguments=" + arguments +
            '}';
    }
}
