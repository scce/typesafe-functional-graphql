package name.jonasschuermann.tfg.languages.typescript.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypeScriptFile {
    private final String filename;
    private final List<TypeScriptImport> imports;
    private final List<TypeScriptTopLevelStatement> topLevelStatements;

    public TypeScriptFile(
        String filename,
        List<TypeScriptImport> imports,
        List<TypeScriptTopLevelStatement> topLevelStatements
    ) {
        this.filename = filename;
        this.imports = imports;
        this.topLevelStatements = topLevelStatements;
    }

    public String getFilename() {
        return filename;
    }

    public List<TypeScriptImport> getImports() {
        return imports;
    }

    public List<TypeScriptTopLevelStatement> getTopLevelStatements() {
        return topLevelStatements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptFile that = (TypeScriptFile) o;
        return filename.equals(that.filename) &&
            imports.equals(that.imports) &&
            topLevelStatements.equals(that.topLevelStatements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(filename, imports, topLevelStatements);
    }

    @Override
    public String toString() {
        return "TypeScriptFile{" +
            "filename='" + filename + '\'' +
            ", imports=" + imports +
            ", topLevelStatements=" + topLevelStatements +
            '}';
    }
}
