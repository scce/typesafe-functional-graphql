package name.jonasschuermann.tfg.languages.typescript.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypeScriptObjectLiteral extends TypeScriptExpression {
    private List<TypeScriptObjectLiteralPart> parts;

    public TypeScriptObjectLiteral(
        List<TypeScriptObjectLiteralPart> parts
    ) {
        this.parts = parts;
    }

    public List<TypeScriptObjectLiteralPart> getParts() {
        return parts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptObjectLiteral that = (TypeScriptObjectLiteral) o;
        return parts.equals(that.parts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parts);
    }

    @Override
    public String toString() {
        return "TypeScriptObjectLiteral{" +
            "parts=" + parts +
            '}';
    }
}
