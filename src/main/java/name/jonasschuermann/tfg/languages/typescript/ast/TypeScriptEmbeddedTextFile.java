package name.jonasschuermann.tfg.languages.typescript.ast;

import java.util.Objects;

public class TypeScriptEmbeddedTextFile extends TypeScriptTopLevelStatement {
    final String variableName;
    final String content;

    public TypeScriptEmbeddedTextFile(String variableName, String content) {
        this.variableName = variableName;
        this.content = content;
    }

    public String getVariableName() {
        return variableName;
    }

    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptEmbeddedTextFile that = (TypeScriptEmbeddedTextFile) o;
        return variableName.equals(that.variableName) &&
            content.equals(that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(variableName, content);
    }

    @Override
    public String toString() {
        return "TypeScriptEmbeddedTextFile{" +
            "variableName='" + variableName + '\'' +
            ", content='" + content + '\'' +
            '}';
    }
}
