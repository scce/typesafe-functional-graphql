package name.jonasschuermann.tfg.languages.typescript.ast;

import java.util.Objects;

public final class TypeScriptStringLiteral extends TypeScriptExpression {
    private String value;

    public TypeScriptStringLiteral(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptStringLiteral that = (TypeScriptStringLiteral) o;
        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "TypeScriptStringLiteral{" +
            "value='" + value + '\'' +
            '}';
    }
}
