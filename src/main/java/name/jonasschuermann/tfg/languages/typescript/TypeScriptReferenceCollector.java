package name.jonasschuermann.tfg.languages.typescript;

import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.collection.TreeSet;
import name.jonasschuermann.tfg.UnhandledCaseException;
import name.jonasschuermann.tfg.languages.typescript.ast.*;

public final class TypeScriptReferenceCollector {
    public Set<TypeScriptReference> collectReferences(
        List<TypeScriptTopLevelStatement> topLevelStatements
    ) {
        return topLevelStatements
            .map(this::collectInTopLevelStatement)
            .fold(TreeSet.empty(), Set::union);
    }

    private Set<TypeScriptReference> collectInTopLevelStatement(
        TypeScriptTopLevelStatement topLevelStatement
    ) {
        if (topLevelStatement instanceof TypeScriptInterfaceDeclaration) {
            var interfaceDeclaration =
                (TypeScriptInterfaceDeclaration) topLevelStatement;
            return interfaceDeclaration
                .getPropertyDeclarations()
                .map(this::collectInPropertyDeclaration)
                .fold(TreeSet.empty(), Set::union);
        } else if (topLevelStatement instanceof TypeScriptFunctionDeclaration) {
            var functionDeclaration =
                (TypeScriptFunctionDeclaration) topLevelStatement;
            return functionDeclaration
                .getArgumentDeclarations()
                .flatMap(this::collectInArgumentDeclaration)
                .toSet()
                .union(
                    collectInType(
                        functionDeclaration
                            .getReturnType()
                    )
                )
                .union(
                    functionDeclaration
                        .getStatements()
                        .map(this::collectInStatement)
                        .fold(TreeSet.empty(), Set::union)
                );
        } else if (topLevelStatement instanceof TypeScriptEmbeddedTextFile) {
            return TreeSet.empty();
        } else {
            throw new UnhandledCaseException(topLevelStatement);
        }
    }

    private Set<TypeScriptReference> collectInPropertyDeclaration(
        TypeScriptPropertyDeclaration propertyDeclaration
    ) {
        return collectInType(propertyDeclaration.getType());
    }

    private Set<TypeScriptReference> collectInType(
        TypeScriptType type
    ) {
        if (type instanceof TypeScriptNominalType) {
            var basicType = (TypeScriptNominalType) type;
            return TreeSet.of(basicType.getName());
        } else if (type instanceof TypeScriptTypeApplication) {
            var typeApplication = (TypeScriptTypeApplication) type;
            return TreeSet.of(typeApplication.getConstructor())
                .union(
                    typeApplication
                        .getArguments()
                        .map(this::collectInType)
                        .fold(TreeSet.empty(), Set::union)
                );
        } else if (type instanceof TypeScriptUnionType) {
            var unionType = (TypeScriptUnionType) type;
            return unionType
                .getTypes()
                .map(this::collectInType)
                .fold(TreeSet.empty(), Set::union);
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private Set<TypeScriptReference> collectInArgumentDeclaration(
        TypeScriptArgumentDeclaration argumentDeclaration
    ) {
        return collectInType(argumentDeclaration.getType());
    }

    private Set<TypeScriptReference> collectInStatement(
        TypeScriptStatement statement
    ) {
        if (statement instanceof TypeScriptReturnStatement) {
            var returnStatement = (TypeScriptReturnStatement) statement;
            return collectInExpression(returnStatement.getValue());
        } else {
            throw new UnhandledCaseException(statement);
        }
    }

    private Set<TypeScriptReference> collectInExpression(
        TypeScriptExpression expression
    ) {
        if (expression instanceof TypeScriptStringLiteral) {
            return TreeSet.empty();
        } else if (expression instanceof TypeScriptObjectLiteral) {
            var objectLiteral = (TypeScriptObjectLiteral) expression;
            return objectLiteral
                .getParts()
                .map(this::collectInObjectLiteralPart)
                .fold(TreeSet.empty(), Set::union);
        } else if (expression instanceof TypeScriptFunctionApplication) {
            var functionApplication = (TypeScriptFunctionApplication) expression;
            return TreeSet.of(functionApplication.getName())
                .union(
                    functionApplication
                        .getExpressions()
                        .map(this::collectInExpression)
                        .fold(TreeSet.empty(), Set::union)
                );
        } else {
            throw new UnhandledCaseException(expression);
        }
    }

    private Set<TypeScriptReference> collectInObjectLiteralPart(
        TypeScriptObjectLiteralPart part
    ) {
        if (part instanceof TypeScriptObjectLiteralProperty) {
            TypeScriptObjectLiteralProperty property =
                (TypeScriptObjectLiteralProperty) part;
            return collectInObjectLiteralProperty(property);
        } else if (part instanceof TypeScriptObjectLiteralVariableProperty) {
            TypeScriptObjectLiteralVariableProperty variableProperty =
                (TypeScriptObjectLiteralVariableProperty) part;
            return collectInObjectLiteralVariableProperty(variableProperty);
        } else {
            throw new UnhandledCaseException(part);
        }
    }

    private Set<TypeScriptReference> collectInObjectLiteralProperty(
        TypeScriptObjectLiteralProperty Property
    ) {
        return collectInExpression(Property.getValue());
    }

    private Set<TypeScriptReference> collectInObjectLiteralVariableProperty(
        TypeScriptObjectLiteralVariableProperty variableProperty
    ) {
        return TreeSet.of(variableProperty.getName());
    }
}
