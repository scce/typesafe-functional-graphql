package name.jonasschuermann.tfg.languages.typescript.ast;

import java.util.Objects;

public final class TypeScriptArgumentDeclaration {
    private String name;
    private TypeScriptType type;

    public TypeScriptArgumentDeclaration(String name, TypeScriptType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public TypeScriptType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptArgumentDeclaration that = (TypeScriptArgumentDeclaration) o;
        return name.equals(that.name) &&
            type.equals(that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type);
    }

    @Override
    public String toString() {
        return "TypeScriptArgumentDeclaration{" +
            "name='" + name + '\'' +
            ", type=" + type +
            '}';
    }
}
