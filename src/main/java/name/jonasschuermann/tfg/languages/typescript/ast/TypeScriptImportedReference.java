package name.jonasschuermann.tfg.languages.typescript.ast;

import java.util.Objects;

public final class TypeScriptImportedReference extends TypeScriptReference {
    private String module;
    private String name;

    public TypeScriptImportedReference(String module, String name) {
        this.module = module;
        this.name = name;
    }

    public String getModule() {
        return module;
    }

    public String getName() {
        return name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptImportedReference that = (TypeScriptImportedReference) o;
        return module.equals(that.module) &&
            name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(module, name);
    }

    @Override
    public String toString() {
        return "TypeScriptImportedReference{" +
            "module='" + module + '\'' +
            ", name='" + name + '\'' +
            '}';
    }

    @Override
    public String getLocalName() {
        return name;
    }
}
