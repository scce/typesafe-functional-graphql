package name.jonasschuermann.tfg.languages.typescript.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypeScriptInterfaceDeclaration extends TypeScriptTopLevelStatement {
    private final String name;
    private final List<TypeScriptPropertyDeclaration> propertyDeclarations;

    public TypeScriptInterfaceDeclaration(
        String name,
        List<TypeScriptPropertyDeclaration> propertyDeclarations
    ) {
        this.name = name;
        this.propertyDeclarations = propertyDeclarations;
    }

    public String getName() {
        return name;
    }

    public List<TypeScriptPropertyDeclaration> getPropertyDeclarations() {
        return propertyDeclarations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptInterfaceDeclaration that = (TypeScriptInterfaceDeclaration) o;
        return name.equals(that.name) &&
            propertyDeclarations.equals(that.propertyDeclarations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, propertyDeclarations);
    }

    @Override
    public String toString() {
        return "TypeScriptInterfaceDeclaration{" +
            "name='" + name + '\'' +
            ", propertyDeclarations=" + propertyDeclarations +
            '}';
    }
}
