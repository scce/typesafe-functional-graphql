package name.jonasschuermann.tfg.languages.typescript.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypeScriptFunctionApplication extends TypeScriptExpression {
    private TypeScriptReference name;
    private List<TypeScriptExpression> expressions;

    public TypeScriptFunctionApplication(
        TypeScriptReference name,
        List<TypeScriptExpression> expressions
    ) {
        this.name = name;
        this.expressions = expressions;
    }

    public TypeScriptReference getName() {
        return name;
    }

    public List<TypeScriptExpression> getExpressions() {
        return expressions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptFunctionApplication that = (TypeScriptFunctionApplication) o;
        return name.equals(that.name) &&
            expressions.equals(that.expressions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, expressions);
    }

    @Override
    public String toString() {
        return "TypeScriptFunctionApplication{" +
            "name=" + name +
            ", expressions=" + expressions +
            '}';
    }
}
