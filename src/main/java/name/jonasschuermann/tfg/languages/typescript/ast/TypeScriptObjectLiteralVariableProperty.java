package name.jonasschuermann.tfg.languages.typescript.ast;

import java.util.Objects;

public final class TypeScriptObjectLiteralVariableProperty extends TypeScriptObjectLiteralPart {
    private TypeScriptReference name;

    public TypeScriptObjectLiteralVariableProperty(TypeScriptReference name) {
        this.name = name;
    }

    public TypeScriptReference getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeScriptObjectLiteralVariableProperty that = (TypeScriptObjectLiteralVariableProperty) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "TypeScriptObjectLiteralVariableProperty{" +
            "name=" + name +
            '}';
    }
}
