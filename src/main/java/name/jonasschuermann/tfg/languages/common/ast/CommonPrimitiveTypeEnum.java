package name.jonasschuermann.tfg.languages.common.ast;

public enum CommonPrimitiveTypeEnum {
    Boolean, Int, Float, String, ID
}
