package name.jonasschuermann.tfg.languages.common.ast;

import java.util.Objects;

public final class CommonSimpleType extends CommonType {
    private CommonNominalType nominalType;
    private boolean nullable;

    public CommonSimpleType(CommonNominalType nominalType, boolean nullable) {
        this.nominalType = nominalType;
        this.nullable = nullable;
    }

    public CommonNominalType getNominalType() {
        return nominalType;
    }

    public boolean getNullable() {
        return nullable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommonSimpleType that =
            (CommonSimpleType) o;
        return nullable == that.nullable &&
            nominalType.equals(that.nominalType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nominalType, nullable);
    }

    @Override
    public String toString() {
        return "CommonSimpleType{" +
            "name=" + nominalType +
            ", nullable=" + nullable +
            '}';
    }
}
