package name.jonasschuermann.tfg.languages.common.ast;

import java.util.Objects;

public final class CommonPrimitiveType extends CommonNominalType {
    private CommonPrimitiveTypeEnum name;

    public CommonPrimitiveType(CommonPrimitiveTypeEnum name) {
        this.name = name;
    }

    public CommonPrimitiveTypeEnum getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommonPrimitiveType that = (CommonPrimitiveType) o;
        return name == that.name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "CommonPrimitiveType{" +
            "name=" + name +
            '}';
    }
}
