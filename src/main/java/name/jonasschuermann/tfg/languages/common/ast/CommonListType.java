package name.jonasschuermann.tfg.languages.common.ast;

import java.util.Objects;

public final class CommonListType extends CommonType {
    private CommonSimpleType appliedType;
    private boolean nullable;

    public CommonListType(
        CommonSimpleType appliedType,
        boolean nullable
    ) {
        this.nullable = nullable;
        this.appliedType = appliedType;
    }

    public CommonSimpleType getAppliedType() {
        return appliedType;
    }

    public boolean getNullable() {
        return nullable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommonListType that = (CommonListType) o;
        return appliedType.equals(that.appliedType) &&
            nullable == that.nullable;
    }

    @Override
    public int hashCode() {
        return Objects.hash(appliedType, nullable);
    }

    @Override
    public String toString() {
        return "CommonListType{" +
            "appliedType=" + appliedType +
            ", nullable=" + nullable +
            '}';
    }
}
