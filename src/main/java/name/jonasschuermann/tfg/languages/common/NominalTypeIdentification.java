package name.jonasschuermann.tfg.languages.common;

import name.jonasschuermann.tfg.languages.common.ast.CommonCompoundType;
import name.jonasschuermann.tfg.languages.common.ast.CommonNominalType;
import name.jonasschuermann.tfg.languages.common.ast.CommonPrimitiveType;
import name.jonasschuermann.tfg.languages.common.ast.CommonPrimitiveTypeEnum;

public final class NominalTypeIdentification {
    public static CommonNominalType identify(String type) {
        switch (type) {
            case "Boolean":
                return new CommonPrimitiveType(CommonPrimitiveTypeEnum.Boolean);
            case "Int":
                return new CommonPrimitiveType(CommonPrimitiveTypeEnum.Int);
            case "Float":
                return new CommonPrimitiveType(CommonPrimitiveTypeEnum.Float);
            case "String":
                return new CommonPrimitiveType(CommonPrimitiveTypeEnum.String);
            case "ID":
                return new CommonPrimitiveType(CommonPrimitiveTypeEnum.ID);
            default:
                return new CommonCompoundType(type);
        }
    }
}
