package name.jonasschuermann.tfg.languages.common.ast;

import java.util.Objects;

public final class CommonCompoundType extends CommonNominalType {
    private String name;

    public CommonCompoundType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommonCompoundType that = (CommonCompoundType) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "CommonCompoundType{" +
            "name='" + name + '\'' +
            '}';
    }
}
