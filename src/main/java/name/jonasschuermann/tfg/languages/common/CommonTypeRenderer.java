package name.jonasschuermann.tfg.languages.common;

import name.jonasschuermann.tfg.UnhandledCaseException;
import name.jonasschuermann.tfg.languages.common.ast.*;

public final class CommonTypeRenderer {
    public static String render(CommonType type) {
        if (type instanceof CommonListType) {
            final var listType = (CommonListType) type;
            return "[" + renderNominalType(listType.getAppliedType()) + "]" +
                renderNullable(listType.getNullable());
        } else if (type instanceof CommonSimpleType) {
            return renderNominalType((CommonSimpleType) type);
        } else if (type instanceof CommonNullType) {
            return "null";
        } else {
            throw new UnhandledCaseException(type);
        }
    }

    private static String renderNominalType(CommonSimpleType simpleType) {
        final var nominalType = simpleType.getNominalType();
        String inner;
        if (nominalType instanceof CommonPrimitiveType) {
            final var primitiveType = (CommonPrimitiveType) nominalType;
            inner = renderPrimitiveType(primitiveType.getName());
        } else if (nominalType instanceof CommonCompoundType) {
            final var compoundType = (CommonCompoundType) nominalType;
            inner = compoundType.getName();
        } else {
            throw new UnhandledCaseException(nominalType);
        }
        return inner + renderNullable(simpleType.getNullable());
    }

    private static String renderPrimitiveType(
        CommonPrimitiveTypeEnum primitiveType
    ) {
        switch (primitiveType) {
            case Boolean:
                return "Boolean";
            case Int:
                return "Int";
            case Float:
                return "Float";
            case String:
                return "String";
            case ID:
                return "ID";
            default:
                throw new UnhandledCaseException(primitiveType);
        }
    }

    private static String renderNullable(boolean nullable) {
        return nullable ? "" : "!";
    }
}
