package name.jonasschuermann.tfg.languages.graphql.ast;

public final class GraphQLNullLiteral extends GraphQLLiteral {
    @Override
    public String toString() {
        return "GraphQLNullLiteral{}";
    }
}
