package name.jonasschuermann.tfg.languages.graphql.ast;

import io.vavr.collection.List;
import io.vavr.control.Option;

import java.util.Objects;

public final class GraphQLProperty {
    private final String name;
    private final Option<String> alias;
    private final List<GraphQLArgumentApplication> arguments;
    private final Option<GraphQLInterface> subquery;

    public GraphQLProperty(
        String name,
        Option<String> alias,
        List<GraphQLArgumentApplication> arguments,
        Option<GraphQLInterface> subquery
    ) {
        this.name = name;
        this.alias = alias;
        this.arguments = arguments;
        this.subquery = subquery;
    }

    public String getName() {
        return name;
    }

    public Option<String> getAlias() {
        return alias;
    }

    public List<GraphQLArgumentApplication> getArguments() {
        return arguments;
    }

    public Option<GraphQLInterface> getSubquery() {
        return subquery;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphQLProperty that = (GraphQLProperty) o;
        return name.equals(that.name) &&
            alias.equals(that.alias) &&
            arguments.equals(that.arguments) &&
            subquery.equals(that.subquery);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, alias, arguments, subquery);
    }

    @Override
    public String toString() {
        return "GraphQLProperty{" +
            "name='" + name + '\'' +
            ", alias=" + alias +
            ", arguments=" + arguments +
            ", subquery=" + subquery +
            '}';
    }
}
