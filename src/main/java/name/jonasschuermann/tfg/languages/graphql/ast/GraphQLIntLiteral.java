package name.jonasschuermann.tfg.languages.graphql.ast;

import java.util.Objects;

public final class GraphQLIntLiteral extends GraphQLLiteral {
    private final String rawValue;

    public GraphQLIntLiteral(String rawValue) {
        this.rawValue = rawValue;
    }

    public String getRawValue() {
        return rawValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphQLIntLiteral that = (GraphQLIntLiteral) o;
        return rawValue.equals(that.rawValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rawValue);
    }

    @Override
    public String toString() {
        return "GraphQLIntLiteral{" +
            "rawValue=" + rawValue +
            '}';
    }
}
