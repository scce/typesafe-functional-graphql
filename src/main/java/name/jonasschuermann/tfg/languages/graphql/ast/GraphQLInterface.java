package name.jonasschuermann.tfg.languages.graphql.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class GraphQLInterface {
    private final List<GraphQLProperty> properties;

    public GraphQLInterface(List<GraphQLProperty> properties) {
        this.properties = properties;
    }

    public List<GraphQLProperty> getProperties() {
        return properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphQLInterface that = (GraphQLInterface) o;
        return properties.equals(that.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(properties);
    }

    @Override
    public String toString() {
        return "GraphQLInterface{" +
            "properties=" + properties +
            '}';
    }
}
