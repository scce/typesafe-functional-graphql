package name.jonasschuermann.tfg.languages.graphql.ast;

import java.util.Objects;

public final class GraphQLArgumentApplication {
    private final String name;
    private final GraphQLExpression expression;

    public GraphQLArgumentApplication(String name, GraphQLExpression expression) {
        this.name = name;
        this.expression = expression;
    }

    public String getName() {
        return name;
    }

    public GraphQLExpression getExpression() {
        return expression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphQLArgumentApplication that = (GraphQLArgumentApplication) o;
        return name.equals(that.name) &&
            expression.equals(that.expression);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, expression);
    }

    @Override
    public String toString() {
        return "GraphQLArgumentApplication{" +
            "name='" + name + '\'' +
            ", expression=" + expression +
            '}';
    }
}
