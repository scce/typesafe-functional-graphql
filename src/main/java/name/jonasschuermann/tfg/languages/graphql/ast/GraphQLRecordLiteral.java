package name.jonasschuermann.tfg.languages.graphql.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class GraphQLRecordLiteral extends GraphQLLiteral {
    private final List<GraphQLRecordLiteralProperty> properties;

    public GraphQLRecordLiteral(List<GraphQLRecordLiteralProperty> properties) {
        this.properties = properties;
    }

    public List<GraphQLRecordLiteralProperty> getProperties() {
        return properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphQLRecordLiteral that = (GraphQLRecordLiteral) o;
        return properties.equals(that.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(properties);
    }

    @Override
    public String toString() {
        return "GraphQLRecordLiteral{" +
            "properties=" + properties +
            '}';
    }
}
