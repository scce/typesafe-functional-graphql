package name.jonasschuermann.tfg.languages.graphql.ast;

public enum GraphQLQueryIntend {
    Query, Mutation
}
