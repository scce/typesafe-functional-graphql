package name.jonasschuermann.tfg.languages.graphql.ast;

import java.util.Objects;

public final class GraphQLBooleanLiteral extends GraphQLLiteral {
    private final boolean value;

    public GraphQLBooleanLiteral(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphQLBooleanLiteral that = (GraphQLBooleanLiteral) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "GraphQLBooleanLiteral{" +
            "value=" + value +
            '}';
    }
}
