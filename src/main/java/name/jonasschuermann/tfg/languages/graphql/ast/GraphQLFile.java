package name.jonasschuermann.tfg.languages.graphql.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class GraphQLFile {
    private final GraphQLQueryIntend intend;
    private final String name;
    private final List<GraphQLArgumentDeclaration> arguments;
    private final GraphQLInterface rootInterface;

    public GraphQLFile(
        GraphQLQueryIntend intend,
        String name,
        List<GraphQLArgumentDeclaration> arguments,
        GraphQLInterface rootInterface
    ) {
        this.intend = intend;
        this.name = name;
        this.arguments = arguments;
        this.rootInterface = rootInterface;
    }

    public GraphQLQueryIntend getIntend() {
        return intend;
    }

    public String getName() {
        return name;
    }

    public List<GraphQLArgumentDeclaration> getArguments() {
        return arguments;
    }

    public GraphQLInterface getRootInterface() {
        return rootInterface;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphQLFile that = (GraphQLFile) o;
        return intend == that.intend && name.equals(that.name) && arguments.equals(that.arguments) && rootInterface.equals(that.rootInterface);
    }

    @Override
    public int hashCode() {
        return Objects.hash(intend, name, arguments, rootInterface);
    }

    @Override
    public String toString() {
        return "GraphQLFile{" +
            "intend=" + intend +
            ", name='" + name + '\'' +
            ", arguments=" + arguments +
            ", rootInterface=" + rootInterface +
            '}';
    }
}
