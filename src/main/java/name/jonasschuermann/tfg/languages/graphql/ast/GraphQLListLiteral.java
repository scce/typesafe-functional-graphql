package name.jonasschuermann.tfg.languages.graphql.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class GraphQLListLiteral extends GraphQLLiteral {
    private final List<GraphQLExpression> items;

    public GraphQLListLiteral(List<GraphQLExpression> items) {
        this.items = items;
    }

    public List<GraphQLExpression> getItems() {
        return items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphQLListLiteral that = (GraphQLListLiteral) o;
        return items.equals(that.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(items);
    }

    @Override
    public String toString() {
        return "GraphQLListLiteral{" +
            "items=" + items +
            '}';
    }
}
