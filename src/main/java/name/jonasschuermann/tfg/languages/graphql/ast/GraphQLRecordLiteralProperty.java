package name.jonasschuermann.tfg.languages.graphql.ast;

import java.util.Objects;

public final class GraphQLRecordLiteralProperty {
    private final String name;
    private final GraphQLExpression value;

    public GraphQLRecordLiteralProperty(String name, GraphQLExpression value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public GraphQLExpression getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphQLRecordLiteralProperty that = (GraphQLRecordLiteralProperty) o;
        return name.equals(that.name) && value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }

    @Override
    public String toString() {
        return "GraphQLRecordLiteralProperty{" +
            "name='" + name + '\'' +
            ", value=" + value +
            '}';
    }
}
