package name.jonasschuermann.tfg.languages.graphql.ast;

import name.jonasschuermann.tfg.languages.common.ast.CommonType;

import java.util.Objects;

public final class GraphQLArgumentDeclaration {
    private final String name;
    private final CommonType type;

    public GraphQLArgumentDeclaration(String name, CommonType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public CommonType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphQLArgumentDeclaration that = (GraphQLArgumentDeclaration) o;
        return name.equals(that.name) &&
            type.equals(that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type);
    }

    @Override
    public String toString() {
        return "GraphQLArgumentDeclaration{" +
            "name='" + name + '\'' +
            ", type=" + type +
            '}';
    }
}
