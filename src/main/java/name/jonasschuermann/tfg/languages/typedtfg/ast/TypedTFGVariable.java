package name.jonasschuermann.tfg.languages.typedtfg.ast;

import java.util.Objects;

public final class TypedTFGVariable extends TypedTFGExpression {
    private final String name;

    public TypedTFGVariable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGVariable typedSelectionVariable = (TypedTFGVariable) o;
        return name.equals(typedSelectionVariable.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "TypedTFGVariable{" +
            "name='" + name + '\'' +
            '}';
    }
}
