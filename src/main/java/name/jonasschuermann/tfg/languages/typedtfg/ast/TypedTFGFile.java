package name.jonasschuermann.tfg.languages.typedtfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypedTFGFile {
    private final List<TypedTFGQuery> queries;

    public TypedTFGFile(List<TypedTFGQuery> queries) {
        this.queries = queries;
    }

    public List<TypedTFGQuery> getQueries() {
        return queries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGFile that = (TypedTFGFile) o;
        return queries.equals(that.queries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queries);
    }

    @Override
    public String toString() {
        return "TypedTFGFile{" +
            "queries=" + queries +
            '}';
    }
}
