package name.jonasschuermann.tfg.languages.typedtfg.ast;

import java.util.Objects;

public final class TypedTFGPathElement {
    private final String name;
    private final boolean nullable;
    private final boolean list;
    private final boolean listItemsNullable;

    public TypedTFGPathElement(
        String name,
        boolean nullable,
        boolean list,
        boolean listItemsNullable
    ) {
        this.name = name;
        this.nullable = nullable;
        this.list = list;
        this.listItemsNullable = listItemsNullable;
    }

    public String getName() {
        return name;
    }

    public boolean getNullable() {
        return nullable;
    }

    public boolean getList() {
        return list;
    }

    public boolean getListItemsNullable() {
        return listItemsNullable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGPathElement that = (TypedTFGPathElement) o;
        return nullable == that.nullable && list == that.list && listItemsNullable == that.listItemsNullable && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, nullable, list, listItemsNullable);
    }

    @Override
    public String toString() {
        return "TypedTFGPathElement{" +
            "name='" + name + '\'' +
            ", nullable=" + nullable +
            ", list=" + list +
            ", listItemsNullable=" + listItemsNullable +
            '}';
    }
}
