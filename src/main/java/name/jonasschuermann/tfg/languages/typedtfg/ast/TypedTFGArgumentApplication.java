package name.jonasschuermann.tfg.languages.typedtfg.ast;

import java.util.Objects;

public final class TypedTFGArgumentApplication {
    private final String name;
    private final TypedTFGExpression expression;

    public TypedTFGArgumentApplication(String name, TypedTFGExpression expression) {
        this.name = name;
        this.expression = expression;
    }

    public String getName() {
        return name;
    }

    public TypedTFGExpression getExpression() {
        return expression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGArgumentApplication that = (TypedTFGArgumentApplication) o;
        return name.equals(that.name) &&
            expression.equals(that.expression);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, expression);
    }

    @Override
    public String toString() {
        return "TypedTFGArgumentApplication{" +
            "name='" + name + '\'' +
            ", expression=" + expression +
            '}';
    }
}
