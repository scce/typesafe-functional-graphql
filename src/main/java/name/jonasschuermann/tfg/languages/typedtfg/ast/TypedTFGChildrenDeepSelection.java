package name.jonasschuermann.tfg.languages.typedtfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypedTFGChildrenDeepSelection extends TypedTFGAnchor {
    private final TypedTFGProperty property;
    private final List<TypedTFGSelection> children;

    public TypedTFGChildrenDeepSelection(
        TypedTFGProperty property,
        List<TypedTFGSelection> children
    ) {
        this.property = property;
        this.children = children;
    }

    public TypedTFGProperty getProperty() {
        return property;
    }

    public List<TypedTFGSelection> getChildren() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGChildrenDeepSelection that = (TypedTFGChildrenDeepSelection) o;
        return property.equals(that.property) &&
            children.equals(that.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(property, children);
    }

    @Override
    public String toString() {
        return "TypedTFGChildrenDeepSelection{" +
            "property=" + property +
            ", children=" + children +
            '}';
    }
}
