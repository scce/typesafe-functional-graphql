package name.jonasschuermann.tfg.languages.typedtfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypedTFGSubselection {
    private final String name;
    private final List<TypedTFGArgumentApplication> argumentApplications;

    public TypedTFGSubselection(String name, List<TypedTFGArgumentApplication> argumentApplications) {
        this.name = name;
        this.argumentApplications = argumentApplications;
    }

    public String getName() {
        return name;
    }

    public List<TypedTFGArgumentApplication> getArgumentApplications() {
        return argumentApplications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGSubselection that = (TypedTFGSubselection) o;
        return name.equals(that.name) &&
            argumentApplications.equals(that.argumentApplications);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, argumentApplications);
    }

    @Override
    public String toString() {
        return "TypedTFGSubselection{" +
            "name='" + name + '\'' +
            ", argumentApplications=" + argumentApplications +
            '}';
    }
}
