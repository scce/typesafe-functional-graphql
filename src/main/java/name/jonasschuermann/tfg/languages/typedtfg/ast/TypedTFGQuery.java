package name.jonasschuermann.tfg.languages.typedtfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypedTFGQuery {
    private final TypedTFGQueryIntend intend;
    private final String name;
    private final List<TypedTFGArgumentDeclaration> argumentDeclarations;
    private final String type;
    private final List<TypedTFGSelection> selections;
    private final List<TypedTFGField> fields;

    public TypedTFGQuery(
        TypedTFGQueryIntend intend,
        String name,
        List<TypedTFGArgumentDeclaration> argumentDeclarations,
        String type,
        List<TypedTFGSelection> selections,
        List<TypedTFGField> fields
    ) {
        this.intend = intend;
        this.name = name;
        this.argumentDeclarations = argumentDeclarations;
        this.type = type;
        this.selections = selections;
        this.fields = fields;
    }

    public TypedTFGQueryIntend getIntend() {
        return intend;
    }

    public String getName() {
        return name;
    }

    public List<TypedTFGArgumentDeclaration> getArgumentDeclarations() {
        return argumentDeclarations;
    }

    public String getType() {
        return type;
    }

    public List<TypedTFGSelection> getSelections() {
        return selections;
    }

    public List<TypedTFGField> getFields() {
        return fields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGQuery that = (TypedTFGQuery) o;
        return intend == that.intend && name.equals(that.name) && argumentDeclarations.equals(that.argumentDeclarations) && type.equals(that.type) && selections.equals(that.selections) && fields.equals(that.fields);
    }

    @Override
    public int hashCode() {
        return Objects.hash(intend, name, argumentDeclarations, type, selections, fields);
    }

    @Override
    public String toString() {
        return "TypedTFGQuery{" +
            "intend=" + intend +
            ", name='" + name + '\'' +
            ", argumentDeclarations=" + argumentDeclarations +
            ", type='" + type + '\'' +
            ", selections=" + selections +
            ", fields=" + fields +
            '}';
    }
}
