package name.jonasschuermann.tfg.languages.typedtfg.ast;

import io.vavr.collection.List;
import name.jonasschuermann.tfg.languages.common.ast.CommonType;

import java.util.Objects;

public final class TypedTFGField {
    private final String name;
    private final CommonType type;
    private final List<TypedTFGPathElement> path;

    public TypedTFGField(
        String name,
        CommonType type,
        List<TypedTFGPathElement> path
    ) {
        this.name = name;
        this.type = type;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public CommonType getType() {
        return type;
    }

    public List<TypedTFGPathElement> getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGField that = (TypedTFGField) o;
        return name.equals(that.name) &&
            type.equals(that.type) &&
            path.equals(that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, path);
    }

    @Override
    public String toString() {
        return "TypedTFGField{" +
            "name='" + name + '\'' +
            ", type=" + type +
            ", path=" + path +
            '}';
    }
}
