package name.jonasschuermann.tfg.languages.typedtfg.ast;

import java.util.Objects;

public final class TypedTFGStringLiteral extends TypedTFGLiteral {
    private final String rawValue;

    public TypedTFGStringLiteral(String rawValue) {
        this.rawValue = rawValue;
    }

    public String getRawValue() {
        return rawValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGStringLiteral that = (TypedTFGStringLiteral) o;
        return rawValue.equals(that.rawValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rawValue);
    }

    @Override
    public String toString() {
        return "TypedTFGStringLiteral{" +
            "rawValue=" + rawValue +
            '}';
    }
}
