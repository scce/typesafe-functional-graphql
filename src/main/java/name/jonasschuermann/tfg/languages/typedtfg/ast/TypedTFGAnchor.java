package name.jonasschuermann.tfg.languages.typedtfg.ast;

abstract public class TypedTFGAnchor {
    public abstract TypedTFGProperty getProperty();
}
