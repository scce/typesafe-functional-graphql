package name.jonasschuermann.tfg.languages.typedtfg.ast;

import java.util.Objects;

public final class TypedTFGRecordLiteralProperty {
    private final String name;
    private final TypedTFGExpression value;

    public TypedTFGRecordLiteralProperty(
        String name,
        TypedTFGExpression value
    ) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public TypedTFGExpression getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGRecordLiteralProperty that = (TypedTFGRecordLiteralProperty) o;
        return name.equals(that.name) && value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }

    @Override
    public String toString() {
        return "TypedTFGRecordLiteralProperty{" +
            "name='" + name + '\'' +
            ", value=" + value +
            '}';
    }
}
