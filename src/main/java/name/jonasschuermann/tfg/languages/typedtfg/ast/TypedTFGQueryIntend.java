package name.jonasschuermann.tfg.languages.typedtfg.ast;

public enum TypedTFGQueryIntend {
    Query, Mutation
}
