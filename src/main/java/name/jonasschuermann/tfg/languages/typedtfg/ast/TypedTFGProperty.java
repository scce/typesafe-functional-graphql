package name.jonasschuermann.tfg.languages.typedtfg.ast;

import io.vavr.collection.List;
import io.vavr.control.Option;
import name.jonasschuermann.tfg.languages.common.ast.CommonType;

import java.util.Objects;

public final class TypedTFGProperty {
    private final String name;
    private final Option<String> alias;
    private final List<TypedTFGArgumentApplication> arguments;
    private final CommonType targetType;

    public TypedTFGProperty(
        String name,
        Option<String> alias,
        List<TypedTFGArgumentApplication> arguments,
        CommonType targetType
    ) {
        this.name = name;
        this.alias = alias;
        this.arguments = arguments;
        this.targetType = targetType;
    }

    public String getName() {
        return name;
    }

    public Option<String> getAlias() {
        return alias;
    }

    public List<TypedTFGArgumentApplication> getArguments() {
        return arguments;
    }

    public CommonType getTargetType() {
        return targetType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGProperty that = (TypedTFGProperty) o;
        return name.equals(that.name) &&
            alias.equals(that.alias) &&
            arguments.equals(that.arguments) &&
            targetType.equals(that.targetType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, alias, arguments, targetType);
    }

    @Override
    public String toString() {
        return "TypedTFGProperty{" +
            "name='" + name + '\'' +
            ", alias=" + alias +
            ", arguments=" + arguments +
            ", targetType=" + targetType +
            '}';
    }
}
