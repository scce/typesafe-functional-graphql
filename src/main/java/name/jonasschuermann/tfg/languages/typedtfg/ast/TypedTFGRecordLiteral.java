package name.jonasschuermann.tfg.languages.typedtfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypedTFGRecordLiteral extends TypedTFGLiteral {
    private final List<TypedTFGRecordLiteralProperty> properties;

    public TypedTFGRecordLiteral(
        List<TypedTFGRecordLiteralProperty> properties
    ) {
        this.properties = properties;
    }

    public List<TypedTFGRecordLiteralProperty> getProperties() {
        return properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGRecordLiteral that = (TypedTFGRecordLiteral) o;
        return properties.equals(that.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(properties);
    }

    @Override
    public String toString() {
        return "TypedTFGRecordLiteral{" +
            "properties=" + properties +
            '}';
    }
}
