package name.jonasschuermann.tfg.languages.typedtfg.ast;

import io.vavr.control.Option;

import java.util.Objects;

public final class TypedTFGSelection {
    private final TypedTFGAnchor anchor;
    private final Option<TypedTFGSubselection> subselection;

    public TypedTFGSelection(
        TypedTFGAnchor anchor,
        Option<TypedTFGSubselection> subselection
    ) {
        this.anchor = anchor;
        this.subselection = subselection;
    }

    public TypedTFGAnchor getAnchor() {
        return anchor;
    }

    public Option<TypedTFGSubselection> getSubselection() {
        return subselection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGSelection that = (TypedTFGSelection) o;
        return anchor.equals(that.anchor) &&
            subselection.equals(that.subselection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(anchor, subselection);
    }

    @Override
    public String toString() {
        return "TypedTFGSelection{" +
            "anchor=" + anchor +
            ", subselection=" + subselection +
            '}';
    }
}
