package name.jonasschuermann.tfg.languages.typedtfg.ast;

import java.util.Objects;

public final class TypedTFGParentDeepSelection extends TypedTFGAnchor {
    private final TypedTFGProperty property;
    private final TypedTFGAnchor child;

    public TypedTFGParentDeepSelection(
        TypedTFGProperty property,
        TypedTFGAnchor child
    ) {
        this.property = property;
        this.child = child;
    }

    public TypedTFGProperty getProperty() {
        return property;
    }

    public TypedTFGAnchor getChild() {
        return child;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGParentDeepSelection that = (TypedTFGParentDeepSelection) o;
        return property.equals(that.property) &&
            child.equals(that.child);
    }

    @Override
    public int hashCode() {
        return Objects.hash(property, child);
    }
}
