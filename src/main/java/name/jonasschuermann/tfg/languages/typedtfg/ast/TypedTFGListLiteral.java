package name.jonasschuermann.tfg.languages.typedtfg.ast;

import io.vavr.collection.List;

import java.util.Objects;

public final class TypedTFGListLiteral extends TypedTFGLiteral {
    private final List<TypedTFGExpression> items;

    public TypedTFGListLiteral(List<TypedTFGExpression> items) {
        this.items = items;
    }

    public List<TypedTFGExpression> getItems() {
        return items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGListLiteral that = (TypedTFGListLiteral) o;
        return items.equals(that.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(items);
    }

    @Override
    public String toString() {
        return "TypedTFGListLiteral{" +
            "items=" + items +
            '}';
    }
}
