package name.jonasschuermann.tfg.languages.typedtfg.ast;

public final class TypedTFGNullLiteral extends TypedTFGLiteral {
    @Override
    public boolean equals(Object obj) {
        return getClass() == obj.getClass();
    }

    @Override
    public String toString() {
        return "TypedTFGNullLiteral{}";
    }
}
