package name.jonasschuermann.tfg.languages.typedtfg.ast;

import java.util.Objects;

public final class TypedTFGPropertyAnchor extends TypedTFGAnchor {
    private final TypedTFGProperty property;

    public TypedTFGPropertyAnchor(TypedTFGProperty property) {
        this.property = property;
    }

    public TypedTFGProperty getProperty() {
        return property;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedTFGPropertyAnchor that = (TypedTFGPropertyAnchor) o;
        return property.equals(that.property);
    }

    @Override
    public int hashCode() {
        return Objects.hash(property);
    }

    @Override
    public String toString() {
        return "TypedTFGPropertyAnchor{" +
            "property=" + property +
            '}';
    }
}
