package name.jonasschuermann.tfg;

import io.vavr.Function2;
import io.vavr.Function4;
import io.vavr.Function6;

import java.util.function.Function;

public final class Curry {
    public static <A, B, R> Function<A, Function<B, R>> curry2(
        Function2<A, B, R> uncurried
    ) {
        return a -> b -> uncurried.apply(a, b);
    }

    public static <A, B, C, D, R> Function<A, Function<B, Function<C, Function<D, R>>>> curry4(
        Function4<A, B, C, D, R> uncurried
    ) {
        return a -> b -> c -> d -> uncurried.apply(a, b, c, d);
    }

    public static <A, B, C, D, E, F, R> Function<A, Function<B, Function<C, Function<D, Function<E, Function<F, R>>>>>> curry6(
        Function6<A, B, C, D, E, F, R> uncurried
    ) {
        return a -> b -> c -> d -> e -> f -> uncurried.apply(a, b, c, d, e, f);
    }
}
