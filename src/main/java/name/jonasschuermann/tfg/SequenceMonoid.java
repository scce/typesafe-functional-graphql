package name.jonasschuermann.tfg;

import io.vavr.collection.List;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;

/**
 * This monoid implementation is not very performant (probably O(n²))
 */
public final class SequenceMonoid<T> {
    private List<T> elements;

    private SequenceMonoid(List<T> elements) {
        this.elements = elements;
    }

    public static <T> SequenceMonoid<T> of(T... elements) {
        return new SequenceMonoid<>(List.ofAll(Arrays.asList(elements)));
    }

    public static <T> SequenceMonoid<T> ofAll(Iterable<T> elements) {
        return new SequenceMonoid<>(List.ofAll(elements));
    }

    public List<T> toList() {
        return elements;
    }

    public static <T> SequenceMonoid<T> zero() {
        return new SequenceMonoid<>(List.empty());
    }

    public SequenceMonoid<T> plus(SequenceMonoid<T> other) {
        return new SequenceMonoid<>(elements.appendAll(other.elements));
    }

    public static <T> SequenceMonoid<T> sum(
        List<SequenceMonoid<T>> monoids
    ) {
        return new SequenceMonoid<>(
            monoids
                .map(m -> m.elements)
                .flatMap(Function.identity())
        );
    }

    public SequenceMonoid<T> append(T element) {
        return new SequenceMonoid<>(elements.append(element));
    }

    public int length() {
        return elements.length();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SequenceMonoid<?> that = (SequenceMonoid<?>) o;
        return elements.equals(that.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(elements);
    }

    @Override
    public String toString() {
        return "SequenceMonoid{" +
            "elements=" + elements +
            '}';
    }
}
