package name.jonasschuermann.tfg;

import io.vavr.collection.List;
import name.jonasschuermann.tfg.languages.pretty.ast.IndentedBlock;
import name.jonasschuermann.tfg.languages.pretty.ast.Line;
import name.jonasschuermann.tfg.languages.pretty.ast.Part;

import java.util.function.Function;

public final class SerializerHelper {
    public static <T> IndentedBlock serializeMultilineList(
        Function<T, SequenceMonoid<String>> f,
        String connector,
        List<T> items
    ) {
        var generatedItems = items.map(f);
        return new IndentedBlock(
            SequenceMonoid.<Part>ofAll(
                generatedItems
                    .take(generatedItems.length() - 1)
                    .map(item -> new Line(item.append(connector)))
            )
                .append(new Line(generatedItems.last()))
        );
    }

    public static <T> SequenceMonoid<String> serializeInlineList(
        Function<T, SequenceMonoid<String>> f,
        String connector,
        List<T> items
    ) {
        var generatedItems = items.map(f);
        return SequenceMonoid.sum(
            generatedItems
                .take(generatedItems.length() - 1)
                .map(generatesProperty -> generatesProperty.append(connector))
        )
            .plus(generatedItems.last());
    }
}
