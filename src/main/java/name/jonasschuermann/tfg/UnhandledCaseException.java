package name.jonasschuermann.tfg;

public final class UnhandledCaseException extends RuntimeException {
    public UnhandledCaseException(Object object) {
        super(object.getClass().getName());
    }
}
