package name.jonasschuermann.tfg;

import graphql.schema.idl.SchemaParser;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.TreeMap;
import io.vavr.control.Either;
import name.jonasschuermann.tfg.languages.tfg.Weeder;
import name.jonasschuermann.tfg.languages.pretty.PrettyPrinter;
import name.jonasschuermann.tfg.languages.typescript.TypeScriptReferenceCollector;
import name.jonasschuermann.tfg.transformers.GraphQLQuerySerializer;
import name.jonasschuermann.tfg.transformers.GraphQLQueryGenerator;
import name.jonasschuermann.tfg.transformers.TypeScriptSerializer;
import name.jonasschuermann.tfg.transformers.typescriptgenerator.TypeScriptGenerator;
import name.jonasschuermann.tfg.transformers.typecheck.Typechecker;
import name.jonasschuermann.tfg.transformers.typecheck.typeerrors.TypeError;

public final class Compiler {
    private SchemaParser schemaParser;
    private Weeder weeder;
    private Typechecker typeChecker;
    private GraphQLQueryGenerator graphQLQueryGenerator;
    private GraphQLQuerySerializer graphQLQuerySerializer;
    private PrettyPrinter prettyPrinter;
    private TypeScriptGenerator typeScriptGenerator;
    private TypeScriptSerializer typeScriptSerializer;

    public Compiler(
        SchemaParser schemaParser,
        Weeder weeder,
        Typechecker typeChecker,
        GraphQLQueryGenerator graphQLQueryGenerator,
        GraphQLQuerySerializer graphQLQuerySerializer,
        PrettyPrinter prettyPrinter,
        TypeScriptGenerator typeScriptGenerator,
        TypeScriptSerializer typeScriptSerializer
    ) {
        this.schemaParser = schemaParser;
        this.weeder = weeder;
        this.typeChecker = typeChecker;
        this.graphQLQueryGenerator = graphQLQueryGenerator;
        this.graphQLQuerySerializer = graphQLQuerySerializer;
        this.prettyPrinter = prettyPrinter;
        this.typeScriptGenerator = typeScriptGenerator;
        this.typeScriptSerializer = typeScriptSerializer;
    }

    public static Compiler make() {
        return new Compiler(
            new SchemaParser(),
            new Weeder(),
            new Typechecker(),
            new GraphQLQueryGenerator(),
            new GraphQLQuerySerializer(),
            new PrettyPrinter(),
            new TypeScriptGenerator(new TypeScriptReferenceCollector()),
            new TypeScriptSerializer()
        );
    }

    public Either<List<TypeError>, Map<String, String>> compile(
        String schemaString,
        String tfgString
    ) {
        final var typeDefinitionRegistry = schemaParser.parse(schemaString);
        final var tfgAST = weeder.process(tfgString);
        return typeChecker.typecheck(typeDefinitionRegistry, tfgAST)
            .map(typedSelectionFile -> {
                    final var graphQLQueries =
                        graphQLQueryGenerator.generate(typedSelectionFile);
                    return TreeMap.ofEntries(
                        typeScriptGenerator.generate(
                            graphQLQueries
                                .mapValues(graphQLQuerySerializer::serialize)
                                .mapValues(prettyPrinter::prettyPrint),
                            typedSelectionFile
                        ).map(typeScriptFile ->
                            new Tuple2<>(
                                typeScriptFile.getFilename(),
                                prettyPrinter.prettyPrint(
                                    typeScriptSerializer.serialize(typeScriptFile)
                                )
                            )
                        )
                    );
                }
            );
    }
}