import Text.Read (readMaybe)

combine :: Semigroup e
        => (a -> b -> c)
        -> Either e a
        -> Either e b
        -> Either e c
combine f (Right a) (Right b) = Right $ f a b
combine f (Right _) (Left e2) = Left e2
combine f (Left e1) (Right _) = Left e1
combine f (Left e1) (Left e2) = Left $ e1 <> e2
-- combine f a b = f <$> a <#> b

infixl 4 <#>
(<#>) :: Semigroup e => Either e (a -> b) -> Either e a -> Either e b
(<#>) = combine ($)
-- Right f <#> Right a = Right (f a)
-- Right _ <#> Left e2 = Left e2
-- Left e1 <#> Right _ = Left e1
-- Left e1 <#> Left e2 = Left $ e1 <> e2

example1 :: Either [String] (Int, Int, Int)
example1 = (,,) <$> Right 1 <#> Right 2 <#> Right 3

example2 :: Either [String] (Int, Int, Int)
example2 = (,,) <$> Left ["Error 1"] <#> Right 2 <#> Left ["Error 2"]

traverseCombine :: Semigroup e => (a -> Either e b) -> [a] -> Either e [b]
traverseCombine f = foldr (combine (:) . f) (Right [])

parseInts :: [String] -> Either [String] [Int]
parseInts = traverseCombine f where
    f input = (maybeToEither ["Invalid input: " <> input]) . readMaybe $ input

example3 :: Either [String] [Int]
example3 = parseInts ["1", "2", "3"]

example4 :: Either [String] [Int]
example4 = parseInts ["a", "2", "b"]

maybeToEither :: e -> Maybe a -> Either e a
maybeToEither _ (Just a) = Right a
maybeToEither e Nothing  = Left e
