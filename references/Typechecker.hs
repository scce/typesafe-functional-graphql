type Typecheck a = Either [String] a

typecheckQuery :: Selection.Query -> Typecheck Typed.Query
typecheckQuery query =
    Typed.Query
        <$> pure (name query)
        <#> traverseCombine
                typecheckArgumentDeclaration
                (argumentDeclarations property)
        <#> pure (type_ query)
        <#> traverseCombine (typecheckSubselection ...) (subselection property)
