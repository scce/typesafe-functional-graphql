let pkgs = import ./nixpkgs.nix {};
in pkgs.stdenv.mkDerivation {
    name = "haskell-shell";
    buildInputs = with pkgs; [
        (haskellPackages.ghcWithPackages (ps: with ps; [
            haskell-src-meta
            megaparsec
            HUnit
        ]))
    ];
}