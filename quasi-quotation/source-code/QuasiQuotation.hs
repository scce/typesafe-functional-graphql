module QuasiQuotation
    ( Parser
    , unquoteParser
    , makeQuoter
    ) where

import Data.Void ( Void )
import Control.Monad ( join, void )
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as Lexer
import Language.Haskell.TH.Syntax ( Q, Exp )
import Language.Haskell.TH.Quote ( QuasiQuoter(..) )
import qualified Language.Haskell.Meta as Haskell

type Parser = Parsec Void String

makeQuoter :: Parser () -> Parser (Q Exp) -> QuasiQuoter
makeQuoter spaceConsumer parser = QuasiQuoter
    { quoteExp = expressionQuoter spaceConsumer parser
    , quotePat = undefined
    , quoteType = undefined
    , quoteDec = undefined
    }

expressionQuoter :: Parser () -> Parser (Q Exp) -> String -> Q Exp
expressionQuoter spaceConsumer parser source = ast
    where (Just ast) = parseMaybe (topLevelParser spaceConsumer parser) source

topLevelParser :: Parser () -> Parser a -> Parser a
topLevelParser spaceConsumer =  (<* eof) . (spaceConsumer >>)

unquoteParser :: Parser () -> Parser (Q Exp)
unquoteParser spaceConsumer = Lexer.lexeme spaceConsumer $ do
    void $ string "$("
    Right ast <- Haskell.parseExp <$> unquoteContentParser
    void $ string ")"
    pure $ pure ast

unquoteContentParser :: Parser String
unquoteContentParser = repeatedChoice
    [ do
        open <- string "("
        between <- unquoteContentParser
        close <- string ")"
        pure $ open <> between <> close
    , try $ do
        open <- string "'"
        between <- choice
            [ (<>) <$> string "\\" <*> (pure <$> anySingle)
            , pure <$> anySingleBut '\''
            ]
        close <- string "'"
        pure $ open <> between <> close
    , do
        open <- string "\""
        between <- repeatedChoice
            [ string "\\\\"
            , string "\\\""
            , pure <$> anySingleBut '"'
            ]
        close <- string "\""
        pure $ open <> between <> close
    , pure <$> anySingleBut ')'
    ]

repeatedChoice :: [ Parser String ] -> Parser String
repeatedChoice = (join <$>) . many . choice
