module Tests where

import Test.HUnit

import TypeScript

tests :: Test
tests = test
    [ "proper unquote parsing" ~:
        Function "\')\"oo)\\bar" ~=?
            let x' = "bar" in
            [tsDeclaration|function $('\'' : ')' : "\"oo)\\" ++ (x'))(){}|]
    ]
