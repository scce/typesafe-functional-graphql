{-# LANGUAGE QuasiQuotes #-}
module Main where

import TypeScript

main :: IO ()
main = pure ()

fooFunction :: String -> Function
fooFunction x = [tsDeclaration|
    function $("foo" ++ x)(

    ) {

    }
|]
