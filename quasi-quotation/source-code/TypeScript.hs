{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE TemplateHaskell #-}
module TypeScript where

import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import Language.Haskell.TH.Quote ( QuasiQuoter )
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as Lexer
import Control.Applicative ( empty )
import Control.Monad ( void )
import QuasiQuotation

tsDeclaration :: QuasiQuoter
tsDeclaration = makeQuoter spaceConsumer functionParser

data Function = Function
    { name :: String
    } deriving ( Show, Eq, Lift )

spaceConsumer :: Parser ()
spaceConsumer = Lexer.space space1 empty empty

lexeme :: Parser a -> Parser a
lexeme = Lexer.lexeme spaceConsumer

symbol :: String -> Parser String
symbol = Lexer.symbol spaceConsumer

functionParser :: Parser (Q Exp)
functionParser = do
    void $ symbol "function"
    name <- choice
        [ dslUnquoteParser
        , valueIdentifierParser
        ]
    void $ symbol "("
    void $ symbol ")"
    void $ symbol "{"
    void $ symbol "}"
    pure $ [| Function $(name) |]

valueIdentifierParser :: Parser (Q Exp)
valueIdentifierParser =
    lexeme $ lift <$> ((:) <$> lowerChar <*> many alphaNumChar)

dslUnquoteParser :: Parser (Q Exp)
dslUnquoteParser = unquoteParser spaceConsumer
