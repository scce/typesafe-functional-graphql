import (builtins.fetchTarball {
    name = "nixos-20.09-647cc06986c1ae4a2bb05298e0cf598723e42970";
    url = "https://github.com/nixos/nixpkgs/archive/647cc06986c1ae4a2bb05298e0cf598723e42970.tar.gz";
    sha256 = "1n1sd5lbds08vxy8x9l94w0z8bbq39fh2rrr6mnq0rmhf4xb2mj1";
})