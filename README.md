# Type-Safe Functional GraphQL

## Example

The following example shows how complex data can be retrieved from the GitHub
GraphQL API. The query fetches repositories from a given organization and
includes all used languages with their name and byte size for each repository.

### Source Files

Two source files are needed for the compilation: The TFG query definitions and
the GraphQL schema definition of the API.

#### TFG

```tfg
query repositoryListing(
    $organization: String!,
    $repositoryLimit: Int!,
    $languageLimit: Int!
) on Query {
    organization(login: $organization).{
        repositories(
            first: $repositoryLimit,
            orderBy: { field: STARGAZERS, direction: DESC }
        ).nodes <- repositoryForListing(languageLimit: $languageLimit)
    }
}

query repositoryForListing($languageLimit: Int!) on Repository {
    name
    languages(
        first: $languageLimit,
        orderBy: { field: SIZE, direction: DESC }
    ).edges <- languageWithSize
}

query languageWithSize on LanguageEdge {
    node.{
        name
    }
    size
}
```

#### GraphQL Schema

```graphql
type Query {
    organization(login: String!): Organization
}

type Organization {
    repositories(first: Int, orderBy: RepositoryOrder): RepositoryConnection!
}

input RepositoryOrder {
    field: RepositoryOrderField!
    direction: OrderDirection!
}

enum RepositoryOrderField {
    CREATED_AT
    UPDATED_AT
    PUSHED_AT
    NAME
    STARGAZERS
}

enum OrderDirection {
    ASC
    DESC
}

type RepositoryConnection {
    nodes: [Repository]
}

type Repository {
    name: String!
    languages(first: Int, orderBy: LanguageOrder): LanguageConnection
}

input LanguageOrder {
    field: LanguageOrderField!
    direction: OrderDirection!
}

enum LanguageOrderField {
    SIZE
}

type LanguageConnection {
    edges: [LanguageEdge]
}

type LanguageEdge {
    size: Int!
    node: Language!
}

type Language {
    name: String!
}
```

### Generated Artifacts

Based on these inputs plain GraphQL queries, and a type-safe TypeScript
communication interfaces are generated. The communication interface includes
data type definitions and JSON decoders to validate incoming server responses.
The decoders are to be used with
the [json-bouncer](https://gitlab.com/MazeChaZer/json-bouncer) library.

#### GraphQL Query

```graphql
query repositoryListing(
    $organization: String!,
    $repositoryLimit: Int!,
    $languageLimit: Int!
) {
    organization(
        login: $organization
    ) {
        repositories(
            first: $repositoryLimit,
            orderBy: { field: STARGAZERS, direction: DESC }
        ) {
            nodes {
                name
                languages(
                    first: $languageLimit,
                    orderBy: { field: SIZE, direction: DESC }
                ) {
                    edges {
                        node {
                            name
                        }
                        size
                    }
                }
            }
        }
    }
}
```

#### TypeScript Modules

**RepositoryListing.ts**
```typescript
import {
    Decoder,
    array,
    field,
    nullable,
    object,
} from "json-bouncer"

import {
    RepositoryForListing,
    repositoryForListingDecoder,
} from "./RepositoryForListing"

export function requestRepositoryListing(
    organization: string,
    repositoryLimit: number,
    languageLimit: number,
): RepositoryListingRequest {
    return {
        query,
        arguments: {
            organization,
            repositoryLimit,
            languageLimit,
        },
    };
}

export const query =
    "query repositoryListing(\n" +
    "    $organization: String!,\n" +
    "    $repositoryLimit: Int!,\n" +
    "    $languageLimit: Int!\n" +
    ") {\n" +
    "    organization(\n" +
    "        login: $organization\n" +
    "    ) {\n" +
    "        repositories(\n" +
    "            first: $repositoryLimit,\n" +
    "            orderBy: { field: STARGAZERS, direction: DESC }\n" +
    "        ) {\n" +
    "            nodes {\n" +
    "                name\n" +
    "                languages(\n" +
    "                    first: $languageLimit,\n" +
    "                    orderBy: { field: SIZE, direction: DESC }\n" +
    "                ) {\n" +
    "                    edges {\n" +
    "                        node {\n" +
    "                            name\n" +
    "                        }\n" +
    "                        size\n" +
    "                    }\n" +
    "                }\n" +
    "            }\n" +
    "        }\n" +
    "    }\n" +
    "}\n"

export interface RepositoryListingRequest {
    readonly query: string
    readonly arguments: RepositoryListingArguments
}

export interface RepositoryListingArguments {
    readonly organization: string
    readonly repositoryLimit: number
    readonly languageLimit: number
}

export interface RepositoryListing {
    readonly repositories: ReadonlyArray<RepositoryForListing | null> | null
}

export function repositoryListingDecoder(): Decoder<RepositoryListing> {
    return object(
        {
            repositories: field(
                "organization",
                nullable(
                    field(
                        "repositories",
                        field(
                            "nodes",
                            nullable(
                                array(
                                    nullable(
                                        repositoryForListingDecoder(),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        },
    );
}
```

**RepositoryForListing.ts**
```typescript
import {
    Decoder,
    array,
    field,
    nullable,
    object,
    string,
} from "json-bouncer"

import {
    LanguageWithSize,
    languageWithSizeDecoder,
} from "./LanguageWithSize"

export interface RepositoryForListing {
    readonly name: string
    readonly languages: ReadonlyArray<LanguageWithSize | null> | null
}

export function repositoryForListingDecoder(): Decoder<RepositoryForListing> {
    return object(
        {
            name: field(
                "name",
                string(),
            ),
            languages: field(
                "languages",
                nullable(
                    field(
                        "edges",
                        nullable(
                            array(
                                nullable(
                                    languageWithSizeDecoder(),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        },
    );
}
```

**LanguageWithSize.ts**
```typescript
import {
    Decoder,
    field,
    number,
    object,
    string,
} from "json-bouncer"

export interface LanguageWithSize {
    readonly name: string
    readonly size: number
}

export function languageWithSizeDecoder(): Decoder<LanguageWithSize> {
    return object(
        {
            name: field(
                "node",
                field(
                    "name",
                    string(),
                ),
            ),
            size: field(
                "size",
                number(),
            ),
        },
    );
}
```
