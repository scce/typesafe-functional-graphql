# Typechecker

The typechecker does not only check the types of a given TFG AST but it also
produces an new TFG AST that includes type annotations detected in the
process. In that sense, the typechecker is not only a validator, but a parser
that that takes the untyped AST and then either returns a type-annotated AST
on success or a list of type errors on failure. The idealized data types for
the typechecker illustrate this point:

```haskell
data Either e a = Left e | Right a

type Typecheck a = Either [TypeError] a

typecheck :: AST -> Typecheck TypedAST
```

In general, the typechecker recursively descents into the AST, combining type
checks of child nodes on the way back. If all child nodes of a given node
were typechecked successfully and the node itself was ok, all the results
from the children will be put together to return a sucessful typecheck in the
`Right` constructor. If the typechecking failed for any child node or the
node itself, all the errors are collected in a list and returned as a failed
typecheck in the `Left` constructor. This is implemented using
applicative-style lifts and currying.

## Implementation

If we want to find all visible type errors at once, using exceptions is not
possible, because then the first error causes the whole typecheck to
terminate and only the first error is returned. That is why the typechecker
uses sum types for the error handling, which is a much more flexible
approach. The sum type used, `Either`, only has two constructors: `Left` for
failure, `Right` for success. The `Right` and `Left` constructors carry
values of different types: While the `Right`-constructor contains a node of
the typed AST, the `Left` constructor contains a list of type errors. With
sum types, we are free to combine different results any way we want,
including collecting all those type errors, but a naive implementation looks
very cumbersome:

```java
Either<List<TypeError>, TypedParentNode> typecheckParentNode(
    ParentNode node
) {
    final var child1Typecheck = typecheckChild1(node.getChild1());
    final var child2Typecheck = typecheckChild2(node.getChild2());
    final var child3Typecheck = typecheckChild3(node.getChild3());

    if (child1Typecheck.isRight() &&
        child2Typecheck.isRight() &&
        child3Typecheck.isRight()
    ) {
        return Either.right(
            new TypedParentNode(
                child1Typecheck.unwrapRight(),
                child2Typecheck.unwrapRight(),
                child3Typecheck.unwrapRight()
            )
        );
    }

    return Either.left(
        List.empty()
            .appendAll(
                child1Typecheck.isLeft ?
                    childe1Typecheck.unwrapLeft() :
                    List.empty()
            )
            .appendAll(
                child2Typecheck.isLeft ?
                    childe2Typecheck.unwrapLeft() :
                    List.empty()
            )
            .appendAll(
                child3Typecheck.isLeft ?
                    childe3Typecheck.unwrapLeft() :
                    List.empty()
            )
    );
}
```

This code fulfills the specification, but the redundancy is very visible and
seems impossible to abstract away. Another problem with this code is the
heavy usage of unwrap-functions. `unwrapRight`, when called on an
`Either`-value, returns the value given to the right constructor when the
value was created. But if the value was created with the left constructor,
`unwrapRight` throws an exception. Same goes for the `unwrapLeft` function.
Here it's fine because we check the constructor of the `Either`-value using
`isRight` and `isLeft`, but this verification is only informal. If there is a
typo in any of the if-statements, the unwrap-functions will throw exceptions
at runtime and there is no way to make sure this doesn't happen. So ideally
we would like to avoid using unwrap-functions altogether. The solution to
these problems lies in the usage of applicative functors and will be
explained in the following section.

## Applicative Functors to the Rescue!

We show how the same behavior can be implemented much more succinctly using
applicative functors. To ease the explanation of this abstraction the
examples use Haskell source code. Afterwards the solution will be translated
back to Java. The previous example looks like this in Haskell:

```haskell
typecheckParentNode :: ParentNode -> Typecheck TypedParentNode
typecheckParentNode node =
    let
        child1Typecheck = typecheckChild1 (child1 node)
        child2Typecheck = typecheckChild2 (child2 node)
        child3Typecheck = typecheckChild3 (child3 node)
    in
        case (child1Typecheck, child2Typecheck, child3Typecheck) of
            (Right c1, Right c2, Right c3) ->
                Right $ TypedParentNode c1 c2 c3
            
            _ ->
                Left $ concat
                    [ getErrors child1Typecheck
                    , getErrors child2Typecheck
                    , getErrors child3Typecheck
                    ]

getErrors :: Either [e] a -> [e]
getErrors (Right _)     = []
getErrors (Left errors) = errors
```

Using proper pattern matching we can get rid of all unwrap-functions which is
good, but the code still looks pretty cumbersome and redundant. We now need
the `Functor` and `Applicative` type classes, which are roughly defined like
this:

```haskell
class Functor f where
    fmap :: (a -> b) -> f a -> f b

class Functor f => Applicative f where
    pure :: a -> f a
    (<*>) :: f (a -> b) -> f a -> f b
```

In Haskell `Either` has default implementations of Functor and Applicative
Instances:

```haskell
instance Functor (Either e) where
    fmap f (Right a) = Right $ f a
    fmap _ (Left e)  = Left e

instance Applicative (Either e) where
    pure = Right

    Right f <*> Right a = Right $ f a
    Right _ <*> Left e  = Left e
    Left e  <*> _       = Left e
```

Another important detail is the fact that functions are automatically curried
in Haskell. Instead of applying n arguments to a function with arity n, one
can only supply m arguments to get back a function that accepts the remaining
n - m arguments before fully evaluating. E.g. the type of the
`TypedParentNode` constructor ist:

```haskell
TypedParentNode :: ChildNode1 -> (ChildeNode2 -> (ChildNode3 -> TypedParentNode))
```

By putting the `TypedParentNode` constructor into an `Either` using `pure`,
we can step by step apply the typecheck results to the constructor using the
`<*>`-operator:

```haskell
typecheckParentNode :: ParentNode -> Typecheck TypedParentNode
typecheckParentNode node =
    pure TypedParentNode <*> typecheckChild1 (child1 node)
                         <*> typecheckChild2 (child2 node)
                         <*> typecheckChild3 (child3 node)
```

That looks much better and does nearly the same thing as before. If all child
typechecks succeed, the `TypedParentNode` will be constructed. The only
problem is that not all errors, but only the first will be returned if any of
the child typechecks fail. That's why we create a slightly different
implementation of the `<*>`-operator for the `Either` data type:

```haskell
infixl 4 <#>
(<#>) :: Semigroup e => Either e (a -> b) -> Either e a -> Either e b
Right f <#> Right a = Right (f a)
Right _ <#> Left e2 = Left e2
Left e1 <#> Right _ = Left e1
Left e1 <#> Left e2 = Left $ e1 <> e2
```

This operator requies a `Semigroup` instance on the error type so that errors
can be concatenated using the `<>`-operator. As we keep errors in a simple
list, there is a `Semigroup` instance for our error type:

```haskell
typecheckParentNode :: ParentNode -> Typecheck TypedParentNode
typecheckParentNode node =
    pure TypedParentNode <#> typecheckChild1 (child1 node)
                         <#> typecheckChild2 (child2 node)
                         <#> typecheckChild3 (child3 node)
```

The code is still as simple as before but now completely conforms to our
specifications: If all child typechecks succeed, a `TypedParentNode` is
constructed in the `Right` constructor, otherwise the accumulated list of all
child typecheck errors is returned in the `Left` constructor.

## Back to Java

We now port the structure that we previously developed in Haskell back to
Java. Of course the translation will not be nearly as concise, but we will
realize the same semantics at the end.

The first problem is the fact that Java doesn't allow the definition of
custom operators. Defining the `<#>`-operator as a static method would lead
to deeply nested code when multiple typechecks are applied. The best solution
would be applying the operator via method chaining. So we create a new class
that wraps an `Either` object and has a member method to apply other
instances with the semantics of the `<#>`-operator:

```java
public final class Typecheck<T> {
    final Either<List<Typecheck>, T> either;

    public Typecheck(Either<List<Typecheck>, T> either) {
        this.either = either;
    }

    public Either<List<Typecheck>, T> unwrap() {
        return either;
    }

    // Define member method for <#>-operator here
}
```

But looking back at the type definition of `<#>`, we see that the first
argument must be of type `Semigroup e => Either e (a -> b)`. When doing
method chaining, the first argument to a function is the object itself, but
in Java it's not possible to constrain the type of `T` to be a function from
`a` to `b` just in this method, so there is no way to define this operator as
a member method.

Fortunately there is another, completely equivalent way of defining the
`Applicative` type class that replaces the `<*>`-operator with the `liftA2`
function:

```haskell
class Functor f => Applicative f where
    pure :: a -> f a
    liftA2 :: (a -> b -> c) -> f a -> f b -> f c
```

By defining `<*>` and `liftA2` in terms of each other we show that these
definitions are in fact equivalent:

```haskell
(<*>) = liftA2 ($)
liftA2 f x y = pure f <*> x <*> y
```

The default implementation of this alternative `Applicative` defintion for
`Either` looks like this:

```haskell
instance Applicative (Either e) where
    pure = Right

    liftA2 f (Right a) (Right b) = Right $ f a b
    liftA2 _ (Right _) (Left e2) = Left e2
    liftA2 _ (Left e1)  _        = Left e1
```

Again, we develop a derivation of the `liftA2` for `Either` that collects the
errors, called `combine`:

```haskell
combine :: Semigroup e => (a -> b -> c) -> Either e a -> Either e b -> Either e c
combine f (Right a) (Right b) = Right $ f a b
combine _ (Right _) (Left e2) = Left e2
combine _ (Left e1) (Right _) = Left e1
combine _ (Left e1) (Left e2) = Left $ e1 <> e2
```

After switching the first two arguments `combine` can implemented as a
chainable Java method:

```java
public final class Typecheck<T> {
    final Either<List<Typecheck>, T> either;

    public Typecheck(Either<List<Typecheck>, T> either) {
        this.either = either;
    }

    public Either<List<Typecheck>, T> unwrap() {
        return either;
    }

    public <U, R> Typecheck<R> combine(
        Function2<T, U, R> f,
        Typecheck<U> other
    ) {
        // ...
    }
}
```

When chaining `combine` we just pass in `Function::apply` for `f` and thus
get the `<#>`-operator back within Java's constrained type system.

Before we can put this into action, there is one missing piece: currying. In
Java, all functions a not curried by default so we have to define a curry
function for each arity:

```java
public final class Curry {
    // ...

    public static <A, B, C, R> Function<A, Function<B, Function<C, R>>> curry3(
        Function3<A, B, C, R> uncurried
    ) {
        return a -> b -> -> c -> uncurried.apply(a, b, c);
    }

    // ...
}
```

Now we can finally put it all together in the typechecker:

```java
Typecheck<TypedParentNode> typecheckParentNode(
    ParentNode node
) {
    return Typecheck.right(Curry.curry3(TypedParentNode::new))
        .combine(Function::apply, typecheckChild1(node.getChild1()))
        .combine(Function::apply, typecheckChild2(node.getChild2()))
        .combine(Function::apply, typecheckChild3(node.getChild3()));
}
```

## Handling Lists of Typechecks

At some places, the AST contains lists of elements, e.g. the properties of a
query. This gives us a list of typechecks from the child elements. If all
typechecks in the list are successful, we would like to get a list of typed
AST elements in the `Right` constructor. If any of the typechecks failed,
a list of all errors in the `Left` constructor should be produced. So given a
list of `Typecheck` values, we want a `Typecheck` of a list. Luckily there is
a general concept that captures this requirement called `Traversable`:

```haskell
class (Functor t, Foldable t) => Traversable t where
    traverse :: Applicative f => (a -> f b) -> t a -> f (t b)
    -- ...
```

Instantiating this general defition with our concrete types makes this
clearer:

```haskell
traverse :: (a -> Typecheck b) -> [a] -> Typecheck [b]
```

In Haskell, `traverse` is already defined for `Either`, but like before this
default implementation only captures the first error that occures. So again
we define a variant of `traverse` that collects all the errors:

```haskell
combineTraversal :: (Semigroup e) => (a -> Either e b) -> [a] -> Either e [b]
combineTraversal f (x:xs) = pure (:) <#> (f x) <#> (combineTraversal f xs)
combineTraversal f []     = pure []
```

Typechecking a list of children would look like this using
`combineTraversal`:

```
typecheckParentNode :: ParentNode -> Typecheck TypedParentNode
typecheckParentNode node =
    pure TypedParentNode <#> combineTraversal typecheckChild1 (children1 node)
                         <#> typecheckChild2 (child2 node)
                         <#> typecheckChild3 (child3 node)
```

In Java, `combineTraversal` can be implemented as a static method in the
`Typecheck` class:

```java
public static <A, B> Typecheck<List<B>> combineTraversal(
    Function<A, Typecheck<B>> f,
    List<A> list
) {
    // ...
}
```

And thus we can easily typecheck lists of children:

```java
Typecheck<TypedParentNode> typecheckParentNode(
    ParentNode node
) {
    return Typecheck.right(Curry.curry3(TypedParentNode::new))
        .combine(
            Function::apply,
            Typecheck.combineTraversal(
                this::typecheckChild1,
                node.getChildren1()
            )
        )
        .combine(Function::apply, typecheckChild2(node.getChild2()))
        .combine(Function::apply, typecheckChild3(node.getChild3()));
}
```
