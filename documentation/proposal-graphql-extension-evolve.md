# Generating Entity Interfaces and Decodes from GraphQL Queries

A GraphQL query describes the complete structure of the data that will be
received from the server while the GraphQL schema defines the types of every
possible endpoint. This makes a model transformation from query + schema to
entity possible, which is then used to generate the matching interface and
decoder for the request.

## Problem: Handling Nested Queries

```graphql
{
    heroes {
        name
        friends {
            name
        }
    }
    humans {
        name
    }
}
```

The objects queried with GraphQL can be arbitrarily nested. This is a problem
because Evolve entities only allow for flat objects.

## Restricting GraphQL Queries

```graphql
{
    humans {
        name
        age
    }
}
```

A simple solution to the problem would be to restrict what kind of GraphQL
queries are allowed. If every GraphQL query would have only one root object
with no further nested attributes, a mapping to Evolve entities is possible.
Needless to say that this restriction is not feasible in a somewhat bigger
project, because the power of GraphQL lies in the possibility to fetch nested
data in one request.

## Nested Evolve Entities

```typescript
interface HeroQuery {
    readonly heroes: {
        readonly name: string
        readonly friends: Array<{
            readonly name: string
        }>
    }[]
    readonly humans: Array<{
        name: string
    }>
}
```

One could extend the entity model to allow for nested objects. While this would
give proper static types for the whole datastructure, the generated interface
is complicated and hard to work with. It is conceivable that one would like to
work on specific subtrees of the data structure (e.g. friend-objects), but the
generated interface can only describe the whole thing. So additional interfaces
have to be written by hand, which are redundant because they already exist as
part of the generated interface. The complications in the entity model would
not be worth the end result.

## Splitting Up the Tree

```typescript
interface CharacterQuery {
    readonly herores: HeroWithFriends[]
    readonly humans: Human[]
}

interface HeroWithFriends {
    readonly name: string
    readonly friends: Friend[]
}

interface Friend {
    readonly name: string
}

interface Human {
    readonly name: string
}
```

Instead of one big interface it would be better to use one interface for each
object in the tree. This way, every sub-component of the tree can be addressed
separately and the entity model doesn't have to be extended. Furthermore re-use
of sub-components that appear in different queries becomes possible. The
hardest in this approach is giving meaningful names to the separate interfaces.
Three ideas for this problem are presented in the following.

### Auto-Generated Interface Names

Names could be auto-generated from the tree structure, e.g. `CharacterQuery`,
`CharacterQueryHero`, `CharacterQueryHeroFriend`, and so on. This would be easy
to do but has several downsides. For once, names become unwieldedly large,
especially with objects that are nested 3 or more layers deep. Singular/plural
conversion becomes a problem. Also, re-using interfaces of identical
sub-components becomes impossible because they get assigned different generated
names.

### Named Fragments

```graphql
{
    heroes {...heroWithFriends}
    humans {...human}
}

fragment heroWithFriends on Hero {
    name
    friends {...friend}
}

fragment friend on Character {
    name
}

fragment human on Human {
    name
}
```

GraphQL natively supports the notion of fragments, named subqueries that can be
inserted into another query. If one requires the user to put every nested
object into a fragment, the names of these framents can be used for interface
names. The implementation of the code generator can become a little more
complicated, though. If two fragments from different queries have the same
name, they have to be checked to be structurally equal, then re-use of
sub-query interfaces is possible. Moreover, fragments can't be re-used from
different GraqhQL queries, because the language doesn't have support for that.

### Custom DSL

`CharacterQuery.json`
```json
{
    "primitiveFields": [],
    "nestedFields": {
        "heroes": "HeroWithFriends",
        "humans": "Human"
    }
}
```

`HeroWithFriends.json`
```json
{
    "primitiveFields": ["name"],
    "nestedFields": {
        "friends": "Friend"
    }
}
```

`Friend.json`
```json
{
    "primitiveFields": ["name"],
    "nestedFields": {}
}
```

`Human.json`
```json
{
    "primitiveFields": ["name"],
    "nestedFields": {}
}
```

The possibly best solution of those presented would be creating a custom DSL,
as shown above. Every JSON file is identified by their file name and describes
exactly one object with its primitive fields and nested fields. Generating
interfaces and decodes from this model becomes trivial and the final GraphQL
query can also be easily assembled by the code generator. Every interface is
named properly and re-use of sub-components is even possible when build the
query itself.
