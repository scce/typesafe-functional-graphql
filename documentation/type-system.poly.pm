#lang pollen
◊figure[#:label "Type Inference Rules"]{
  ◊inference[
    #:name "QI"
    ◊premise{
      ◊context-on-s{v_1 : \sigma_1, \dots, v_m : \sigma_m}
      ◊term{$\text{p}_1$}
      ◊type{\tau_1}}
    ◊premise-elision
    ◊premise{
      ◊context-on-s{v_1 : \sigma_1, \dots, v_m : \sigma_m}
      ◊term{$\text{p}_n$}
      ◊type{\tau_n}}
    ◊conclusion{
      ◊term{query q($\text{\$v}_1$ : $\sigma_1$, $\dots$, $\text{\$v}_m$ : $\sigma_m$) on S { $\text{p}_1$, $\dots$, $\text{p}_n$}}
      \\
      ◊query-type["\\sigma" "m" "\\{\\, p_1 : \\tau_1, \\dots, p_n : \\tau_n \\,\\}" "S"]}]

  ◊inference[
    #:name "PE"
    ◊premise{
      ◊context-on-s{\Gamma}
      ◊term{p}
      ◊property-type["\\sigma" "n" "\\tau" "S"]}
    ◊premise{
      ◊context-on-s{\Gamma}
      ◊term{$\text{e}_1$}
      ◊type{\sigma_1}}
    ◊premise-elision
    ◊premise{
      ◊context-on-s{\Gamma}
      ◊term{$\text{e}_n$}
      ◊type{\sigma_n}}
    ◊conclusion{
      ◊context-on-s{\Gamma}
      ◊term{p($\text{e}_1$, $\dots$, $\text{e}_n$)}
      ◊type{\tau}}]

  ◊inference[
    #:name "SE"
    ◊premise{
      ◊context-on-s{\Gamma}
      ◊term{p}
      ◊property-type["\\rho" "m" "T" "S"]}
    ◊premise{
      ◊context-on-s{\Gamma}
      ◊term{$\text{a}_1$}
      ◊type{\rho_1}}
    ◊premise-elision
    ◊premise{
      ◊context-on-s{\Gamma}
      ◊term{$\text{a}_m$}
      ◊type{\rho_m}}
    ◊premise{
      ◊term{q}
      ◊query-type["\\sigma" "n" "\\tau" "T"]}
    ◊premise{
      ◊context-on-s{\Gamma}
      ◊term{$\text{b}_1$}
      ◊type{\sigma_1}}
    ◊premise-elision
    ◊premise{
      ◊context-on-s{\Gamma}
      ◊term{$\text{b}_n$}
      ◊type{\sigma_n}}
    ◊conclusion{
      ◊context-on-s{\Gamma}
      ◊term{p($\text{a}_1$, $\dots$, $\text{a}_m$) $\leftarrow$ q($\text{b}_1$, $\dots$, $\text{b}_n$)}
      ◊type{\tau}}]

  ◊inference[
    #:name "PI"
    ◊premise{
      ◊term{Schema[S]}
      ◊type{\{ \, p : (\sigma_1, \dots, \sigma_n) \rightarrow \tau, \dots \, \}}}
    ◊conclusion{
      ◊term{p}
      ◊property-type["\\sigma" "n" "\\tau" "S"]}]

  ◊inference[
    #:name "Var"
    ◊conclusion{
      ◊context-on-s{\Gamma, v : \tau}
      ◊term{\$v}
      ◊type{\tau}}]}
