# Language

TFG is a language that is very similar to GraphQL. One big difference, however,
is how nested data structures are requested. In GraphQL, one can simply write
nested queries like so:

```graphql
query usersWithGroup {
    users {
        email
        username
        group {
            name
        }
    }
}
```

The keys `users` and `group` reference other collections in the graph database.
By providing subqueries right after the reference the graph model can be
traversed.

In TFG, every query operates on one collection only. To request nested
datastructures other queries can be inserted by referencing then with the `<-`
operator:

```tfg
query usersWithGroup on Query {
    users <- userWithGroup
}

query userWithGroup on User {
    email
    username
    group <- group
}

query group on Group {
    name
}
```

Additionally, the collection that a query operates on must be explicitly named
after the `on` keyword. In GraphQL this is not necessary because every query
operates on the root object, but in TFG a query can start at any collection.
Naturally only those TFG queries that start on the `Query` collection will be
synthesized to GraphQL queries later on.

While this seems tedious compared to the way GraphQL queries work, this gives
crucial information to the compiler because every subquery is equipped with its
own name that can be used in the code generation stage. This is how a TypeScript
interface with a readable name can be generated for every layer of the nested
data structure:

```typescript
interface UsersWithGroup {
    user: [UserWithGroup]
}

interface UserWithGroup {
    email: string
    username: string
    group: Group
}

interface Group {
    name: string
}
```

After a complex object is requested from the server, it gets processed and thous
taken apart in the client. With these interface definitions every part of it can
be typed easily. If TFG had used the same nested query structure as GraphQL
queries, there would been either one big TypeScript interface that can't be used
to describe the parts it contains or the interfaces for subquery results would
have unreadable names.

Apart from providing the compiler with additional information, this way of
composing nested queries facilitates reuse, when writing the TFG queries and
when the TypeScript definitions are generated. For every TFG query there is only
one pair of TypeScript interface/decoder definitions, and that is used
everywhere this TFG query is referenced.