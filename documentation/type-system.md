# Type System

## Inference Rules

### QI

```math
\frac{
    \forall i \in [n]. \: \text{target} : \sigma,
    \{ v_j : \rho_j \}_{j \in [m]} \:
    \vdash \text{$\text{s}_i$} :
    \text{selection $\text{x}_i$ $\tau_i$ on $\sigma$}\\
}{
    \vdash \text{
        query q\(
            \text{
                (\( \text{\$v}_j \) : \( \rho_j \))
            }_{j \in [m]}
        \)
        on $\sigma$ \(\text{\{\,\(\text{s}_i\)\,\}}_{i \in [n]}\)} :
    \text{query} \: (\rho_j)_{j \in [m]} \rightarrow
    \{\, x_i : \tau_i \,\}_{i \in [n]}\: \text{on} \: \sigma
}
```

### QE

```math
\frac{
    \begin{array}{c}
        \Gamma,  \: \text{target} : \sigma \vdash \text{a} :
        \text{anchor x $T$ on $\sigma$}
        \\
        \vdash \text{q} : \text{query} \: (\rho_i)_{i \in [n]}
        \rightarrow \tau\: \text{on} \: T
        \\
        \forall i \in [n]. \: \Gamma, \: \text{target} : \sigma \vdash
        \text{$\text{b}_i$} : \rho_i
    \end{array}
}{
    \Gamma, \: \text{target} : \sigma \vdash
    \text{a} \leftarrow \text{q$\text{($\text{b}_i$)}_{i \in [n]}$} : 
    \text{selection x $\tau$ on $\sigma$}
}
```

### BE

```math
\frac{
    \begin{array}{c}
        \Gamma, \: \text{target} : \sigma \vdash \text{a} :
        \text{anchor x $\tau$ on $\sigma$}
        \\
        \text{basic type($\tau$)}
    \end{array}
}{
    \Gamma, \: \text{target} : \sigma \vdash \text{a} :
    \text{selection x $\tau$ on $\sigma$}
}
```

### PI

```math
\frac{
    \vdash \text{Schema[$\sigma$]} : \{ \, p : (\rho_i)_{i \in [n]}
    \rightarrow \tau, \dots \, \}
}{
    \vdash \text{p} : \text{property} \: (\rho_i)_{i \in [n]}
    \rightarrow \tau\: \text{on} \: \sigma
}
```

### PE1

```math
\frac{
    \begin{array}{c}
        \vdash \text{p} : \text{property}
        \: (\rho_i)_{i \in [n]} \rightarrow \tau\:
        \text{on} \: \sigma
        \\
        \forall i \in [n]. \:
        \Gamma \vdash \text{$\text{e}_i$} : \rho_i
    \end{array}
}{
    \Gamma \vdash \text{p($\text{e}_i$)}_{i \in [n]} :
    \text{anchor p $\tau$ on $\sigma$}
}
```

### PE2

```math
\frac{
    \begin{array}{c}
        \vdash \text{p} : \text{property}
        \: (\rho_i)_{i \in [n]} \rightarrow \tau\:
        \text{on} \: \sigma
        \\
        \forall i \in [n]. \:
        \Gamma \vdash \text{$\text{e}_i$} : \rho_i\\
    \end{array}
}{
    \Gamma \vdash \text{x: p($\text{e}_i$)}_{i \in [n]} :
    \text{anchor x $\tau$ on $\sigma$}
}
```

### DE1

```math
\frac{
    \begin{array}{c}
        \Gamma \vdash \text{a}_1 : \text{anchor x $\sigma$ on $\rho$}
        \\
        \Gamma \vdash \text{a}_2 : \text{anchor y $\tau$ on $\sigma$}
    \end{array}
}{
    \Gamma \vdash \text{$\text{a}_1$.$\text{a}_2$} :
    \text{anchor x $\tau$ on $\rho$}
}
```

### DE2

```math
\frac{
    \begin{array}{c}
        \Gamma \vdash \text{a}_1 : \text{anchor x $\sigma$ on $\rho$}
        \\
        \Gamma \vdash \text{a}_2 : \text{anchor y $\tau$ on $\sigma$}
    \end{array}
}{
    \Gamma \vdash \text{$\text{a}_1$.\{$\text{a}_2$, $\dots$\}} :
    \text{anchor y $\tau$ on $\rho$}
}
```

### Var

```math
\frac{}{\Gamma, v : \tau \vdash \text{\$v} : \tau}
```
