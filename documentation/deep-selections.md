# Deep Selections

## Motivation

Sometimes the response objects from GraphQL queries are unnecessarily nested
because the GraphQL schema is modelled in a very general way to allow for many
different queries. This leads to many small TFG queries with few properties that
have to be composed, because every TFG query can only describe one flat data
object. Deep selections solve this problem by flattening response objects and
thereby also improving the resulting data model. Let's take a look at a
simplified excerpt from the schema of the GitHub GraphQL API regarding
repository objects:

```graphql
type Repository {
    name: String!
    languages: LanguageConnection
}

type LanguageConnection {
    edges: [LanguageEdge]
    ...
}

type LanguageEdge {
    size: Int!
    node: Language!
}

type Language {
    name: String!
    color: String
}
```

Using a simple GraphQL query, we can retrieve a repository with all of its
languages:

```graphql
query repository($owner: String!, $name: String!) {
    repository(owner: $owner, name: $name) {
        owner
        name
        languages {
            edges {
                size
                node {
                    name
                    color
                }
            }
        }
    }
}
```

A possible JSON response to this query would look like this:

```json
{
    "owner": "antlr",
    "name": "antlr4",
    "languages": {
        "edges": [
            {
                "node": {
                    "name": "Java",
                    "color": "#b07219"
                },
                "size": 2948093
            },
            {
                "node": {
                    "name": "Python",
                    "color": "#3572a5"
                },
                "size": 1421237
            },
            ...
        ]
    }
}
```

But that is not a very good data model to work with, all the `node` and `edge`
keys are useless and lead to much more nesting than needed. What we would like
to work with instead would be this:

```json
{
    "owner": "antlr",
    "name": "antlr4",
    "languages": [
        {
            "name": "Java",
            "color": "#b07219",
            "size": 2948093
        },
        {
            "name": "Python",
            "color": "#3572a5",
            "size": 1421237
        },
        ...
    ]
}
```

## Usage

With TFG, query responses can be flattened by removing unnecessary layers of
nesting (like the `node` and `edge` keys above). These are the TFG queries that
produce the same GraphQL query as before but generate decoders that emit the
flat data model:

```tfg
query repositoryWithLanguages {
    owner
    name
    languages.edges <- languageWithSize
}

query languageWithSize  {
    node.{
        name
        color
    }
    size
}
```

The `.`-operator represents deep selections and “pulls properties up” in the
data model. There are two variants of deep selections which are explained in the
following.

The `repositoryWithLanguages` query uses the `.`-operator without following
curly braces. This means that the *child* layer `edges` should be eliminated.
The resulting TypeScript interface and decoder looks like this:

```ts
interface RepositoryWithLanguages {
    readonly name: string
    readonly languages: ReadonlyArray<LanguageWithSize>
}

function repositoryWithLanguagesDecoder(): Decoder<RepositoryWithLanguages> {
    return object({
        owner: field("owner", string()),
        name: field("name", string()),
        languages: field("languages", field("edges", languageWithSizeDecoder()),
    })
}
```

The interface only has the parent property `languages` and the decoder goes one
layer deeper into the response object and grabs the data from the nested `edges`
key.

The second query `languageWithSize` uses the `.`-operator with following curly
braces, which means that the *parent* layer `node` should be eliminated and all
children be pulled up in the data model. The interface and decoder looks like
this:

```ts
interface LanguageWithSize {
    readonly name: string
    readonly size: number
    readonly color: string | null
}

function languageWithSizeDecoder(): Decoder<LanguageWithSize> {
    return object({
        name: field("node", field("name", string())),
        color: field("node", field("color", nullable(string()))),
        size: field("size", number()),       
    })
}
```

Again, the decoder goes one layer deeper to grab the child properties of `node`,
thereby eliminating that layer in the resulting data model.

## Composability

Deep selections can be mixed and nested and are fully composable with aliases
and argument applications, as the following fabricated (and complex) example
shows:

### TFG

```tfg
query something {
    entity1: entity(id: 1).{
        fuu: foo
        bar.node
    }
    entity2: entity(id: 2).{
        baz
    }
    things.nodes
}
```

### GraphQL

```graphql
query something {
    entity1: entity(id: 1) {
        fuu: foo
        bar {
            node
        }
    }
    entity2: entity(id: 2) {
        baz
    }
    things {
        nodes
    }
}
```

### TypeScript

```ts
interface Something {
    readonly fuu: string
    readonly bar: string
    readonly baz: string
    readonly things: string
}

function somethingDecoder(): Decoder<Something> {
    return object({
        fuu: field("entity1", field("fuu", string())),
        bar: field("entity1", field("bar", field("node", string()))),
        baz: field("entity2", field("baz", string())),
        things: field("things", field("nodes", string())),
    })
}
```